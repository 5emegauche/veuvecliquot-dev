<div class="pagination pagination-centered">
  <ul>
    <li><a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getPreviousPage() ?]"><span>«</span></a></li>
    [?php foreach ($pager->getLinks() as $page): ?]
      [?php if ($page == $pager->getPage()): ?]
    <li class="active"><span>[?php echo $page ?]</span></li>
      [?php else: ?]
    <li><a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $page ?]"><span>[?php echo $page ?]</span></a></li>
      [?php endif; ?]
    [?php endforeach; ?]
    <li><a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getNextPage() ?]"><span>»</span></a></li>
  </ul>
</div>