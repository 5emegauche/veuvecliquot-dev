[?php

/**
 * <?php echo $this->getModuleName() ?> module configuration.
 *
 * @package    ##PROJECT_NAME##
 * @subpackage <?php echo $this->getModuleName()."\n" ?>
 * @author     ##AUTHOR_NAME##
 * @version    SVN: $Id: helper.php 24171 2009-11-19 16:37:50Z Kris.Wallsmith $
 */
abstract class Base<?php echo ucfirst($this->getModuleName()) ?>GeneratorHelper extends sfModelGeneratorHelper
{
  public function getUrlForAction($action)
  {
    return 'list' == $action ? '<?php echo $this->params['route_prefix'] ?>' : '<?php echo $this->params['route_prefix'] ?>_'.$action;
  }

  public function linkToNew($params)
  {
    return '<li>'.link_to(__($params['label'], array()), '@'.$this->getUrlForAction('new'), array('class'=>'btn btn-primary')).'</li>';
  }

  public function linkToEdit($object, $params)
  {
    return '<li>'.link_to(__($params['label'], array()), $this->getUrlForAction('edit'), $object, array('class'=>'btn')).'</li>';
  }

  public function linkToDelete($object, $params)
  {
    if ($object->isNew())
    {
      return '';
    }

    return '<li>'.link_to(__($params['label'], array()), $this->getUrlForAction('delete'), $object, array('class'=>'btn btn-inverse','method' => 'delete', 'confirm' => !empty($params['confirm']) ? __($params['confirm'], array()) : $params['confirm'])).'</li>';
  }

  public function linkToList($params)
  {
    return '<li>'.link_to(__($params['label'], array()), '@'.$this->getUrlForAction('list'), array('class'=>'btn')).'</li>';
  }

  public function linkToSave($object, $params)
  {
    return '<li><input type="submit" value="'.__($params['label'], array()).'" class="btn btn-primary"/></li>';
  }

  public function linkToSaveAndAdd($object, $params)
  {
    if (!$object->isNew())
    {
      return '';
    }

    return '<li><input type="submit" value="'.__($params['label'], array()).'" name="_save_and_add" class="btn btn-primary" /></li>';
  }

}
