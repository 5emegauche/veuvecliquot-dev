jQuery(document).ready(function() {
  function adapteListeEtats() {
    switch(jQuery('#country_new').val()) {
      case 'USA':
        jQuery('#state_fr').parent().hide();
        jQuery('#state').parent().show();
        break;
      case 'France':
        jQuery('#state').parent().hide();
        jQuery('#state_fr').parent().show();
        break;
      default:
        jQuery('#state').parent().hide();
        jQuery('#state_fr').parent().hide();
        break;
    }
  }

  jQuery('#country_new').bind('change', function() {
    adapteListeEtats();
  });

  adapteListeEtats();
});