var _prehomeConfig =
{
  activeClass: 'ui-state-active'
}

jQuery(document).ready(function() {
  var paysChoisi = jQuery('#pays>option:selected');

  function updateLangues() {
    var langues = paysChoisi.data('langues').split(',');
    jQuery('#lang option').attr('disabled', 'disabled');
    jQuery('#lang option').hide();
    var iNbLangues = langues.length;
    for (i=0;i<iNbLangues;i++) {
      var selecteur = '#' + langues[i];
      jQuery(selecteur).removeAttr('disabled');
      jQuery(selecteur).show();
    }
    jQuery('#lang>option:enabled:first').attr('selected', 'selected');
  }

  function updateAge() {
    var dateJour = new Date();
    var annee = dateJour.getFullYear() - parseInt(paysChoisi.data('age'));
    jQuery('#annee_ph').html(annee);
    jQuery('#input_annee_naissance').val(annee);
  }

  function updateAutorisation() {
    var autorise = parseInt(paysChoisi.data('autorise'));
    if (autorise) {
      jQuery('#age_legal').show();
      jQuery('#btn-yes').show();
      jQuery('#btn-no').show();
    }
    else {
      jQuery('#age_legal').hide();
      jQuery('#btn-yes').hide();
      jQuery('#btn-no').hide();
    }
  }

  jQuery('#pays').on('change', function() {
    paysChoisi = jQuery('#pays>option:selected');
    updateLangues();
    updateAge();
    updateAutorisation();
  });

  updateLangues();
  updateAge();
  updateAutorisation();

  jQuery('#form_ph').on('submit', function(e) {
	var autorise = parseInt(paysChoisi.data('autorise'));
	if (!autorise) {
	  e.preventDefault();
	  alert('Navigation interdite pour ce pays');
	}

    var current = new Date();
    var year = current.getFullYear() - parseInt(paysChoisi.data('age'));

    var month = (current.getMonth() + 1).toString();
    if (month.length < 10) {
      month = '0' + month;
    }

    var day = current.getDate().toString();
    if (day.length == 1) {
      day = '0' + day;
    }

    var min_date = year + month + day;
    var user_date = $('#annee').val() + $('#mois').val() + $('#jours').val();

    if (user_date > min_date) {
       e.preventDefault();

jQuery('#trop_jeune').addClass(_prehomeConfig.activeClass);
       // message d'erreur à mettre
    }
  });

  /*
  jQuery('#btn-no').on('click', function(e) {
    e.preventDefault();
    jQuery('#age_legal').hide();
    jQuery('#btn-yes').hide();
    jQuery('#btn-no').hide();
    jQuery('#pays').hide();
    jQuery('#lang').hide();
    jQuery('#trop_jeune').addClass(_prehomeConfig.activeClass);
  });
  */
  jQuery('#btn-yes').on('click', function() {
	jQuery('#form_ph').trigger('submit');
  });

  var cookieBlock = jQuery('#cookie-usage');
  var closeButton = jQuery('<span class="close image-text">Close</span>').on('click',function()
  {
    cookieBlock.removeClass(_prehomeConfig.activeClass);
  })
  cookieBlock.prepend(closeButton);

});
