var _constant_obj = {};
//var _is_touch_device = 'ontouchstart' in document.documentElement;
//
function setConstant(){
  _constant_obj.window = $(window);
  _constant_obj.document = $(document);
  _constant_obj.body = $('body');
  _constant_obj.window_width = null;
  _constant_obj.window_height = null;
  _constant_obj.ratio_num = 1280/800;
  _constant_obj.isTablet = true;
  _constant_obj.slideArray = [];
  if($('.tablet').css('display')==='none')_constant_obj.isTablet=false;
}
//
/////////////////////////////////// MAIN NAVIGATION
//
var _history = [];
function setNav(){
  _constant_obj.nav = $('.main-nav');
  var menuBt = $('.menu'),
  mainUl = _constant_obj.nav.find('.main-ul');
  //
  menuBt.on('click',function(e){
    e.preventDefault();
    if($(this).hasClass('open')){
        _constant_obj.nav.slideUp("easeInOutCubic",function(){
        mainUl.css({'margin-left':0}).find('.select').removeClass('select');
        if(_constant_obj.isTablet ){
          setMenuHeight();
        }else{
          if(_history.length>0){
            _constant_obj.nav.html(_history[0]);
            _history = [];
          }
        }
      });
    }else{
      _constant_obj.nav.slideDown("easeInOutCubic");
    }
    $(this).toggleClass('open');
  });
  //
  _constant_obj.nav.on('click','.back',function(e){//phone only .back aren t displayed in tablet format
    e.preventDefault();
     _constant_obj.nav.css({'width':2*_constant_obj.window_width+'px','margin-left':-_constant_obj.window_width+'px'});
      _constant_obj.nav.prepend(_history.pop()).animate({'margin-left':'0px'},600,'easeInOutCubic',function(){
        _constant_obj.nav.children('ul:last').remove();
        _constant_obj.nav.css({'width':_constant_obj.window_width+'px'});
      });
  });
  //
  _constant_obj.nav.on('click','a.span',function(e){
    //
    e.preventDefault();
    var _t_ = $(this);
    if(_constant_obj.isTablet){
      if(_t_.hasClass('select')){
        removeSelect(t);
      }else{
        $('a.select').each(function(i,el){
          if($(el).parent().find(_t_).length !== 1){
            removeSelect($(el));
          }
        });
        _t_.addClass('select');
        _t_.next().addClass('select');
        setMenuHeight();
      }
    }else{
      _history.push(_t_.closest('ul'));
      _constant_obj.nav.width(2*_constant_obj.window_width);
      _constant_obj.nav.append(_t_.next().clone()).animate({'margin-left':-_constant_obj.window_width+'px'},600,'easeInOutCubic',function(){
        _t_.closest('ul').remove();
        _constant_obj.nav.css({'margin-left':0,'width':_constant_obj.window_width+'px'});
      });
    }
  });
  $('.top').on('click',function(e){
    e.preventDefault();
    $('html,body').animate({'scrollTop':0},1000,"easeInOutCubic");
  });
}
function removeSelect(el){
  el.removeClass('select');
  el.next().removeClass('select').find('.select').each(function(){
    $(this).removeClass('select');
  });
  setTimeout(setMenuHeight,600);
}
function setMenuHeight(){
  var height = _constant_obj.nav.find('.main-ul').height();
  _constant_obj.nav.find('ul.select').each(function(){
    height = Math.max(height,$(this).height());
  });
  _constant_obj.nav.animate({'height':height+'px'},500,'easeInOutCubic');
}
function resizeNav(){
  _constant_obj.nav.css({'height':'inherit'});
  $('.main-nav ul').each(function(i){
    if(_constant_obj.isTablet){
      if(i===0){
        $(this).css({'height':$(this).children('li:not(.phone)').size()*54+'px','width':'30%'});
      }else{
        $(this).css({'height':$(this).children('li:not(.phone)').size()*54+'px','width':'0%'});
      }
    }else{
      $(this).css({'height':'100%','width':_constant_obj.window_width+'px'});
    }
  })
}
//
//
//////////////////////////////////// SET OFFERS ARTICLE READ MORE BUTTON
//
//

var _textOverFlow;
function setTextOverFlow(){
  
  var overflow = $('.overflow');
  _textOverFlow = overflow.closest('.article');
  
  if(!_textOverFlow.get(0))return;
  
  _textOverFlow.each(function(){
    var t = $(this);
    t.append('<div class="bottom-panel"><a href="#" class="read-more">read more</a></div>').css('margin-bottom','30px');
    //
    var over = t.find('.overflow');
//_constant_obj.isTablet
    var overForm = jQuery('form',over);
    var bottomPanel = jQuery('.bottom-panel',t);
    
    if(overForm.length > 0)
    {
      var oF = overForm.clone();
      //overForm.remove();

      overForm.hide();

      bottomPanel.append(oF);
    }
    
    t.attr('data-overflow',over.height());

    //
  });

/*
  $('.overflow').each(function()
  {
    var t = $(this).closest('.article');
    t.append('<a href="#" class="read-more">read more</a>').css('margin-bottom','30px');
    //
    var over = t.find('.overflow');
    t.attr('data-overflow',over.height());
  });
  */
  //
  $('.read-more').on('click',function(e){
    e.preventDefault();
    var article = $(this).closest('.article');
    if(article.hasClass('art-open')){
      article.find('figure').animate({'height':'250px','opacity':1},600,'easeInOutCubic');
      article.find('.overflow').animate({'height':article.data('overflow')+'px'},600,'easeInOutCubic');
      if(article.find('.dots').get(0))article.find('.dots').slideDown(600,'easeInOutCubic');
article.find('.bottom-panel form').show();
article.find('.overflow form').hide();
      
    }else{
      article.find('figure').animate({'height':'1px','opacity':0},600,'easeInOutCubic');
      var goToHeight = article.data('finalheight')+10;
      if(!$('.slideShow.articles').get(0)){
        goToHeight = Math.max(goToHeight,article.data('height'));
      }
      article.find('.overflow').animate({'height':goToHeight+'px'},600,'easeInOutCubic');
      if(article.find('.dots').get(0))article.find('.dots').slideUp(600,'easeInOutCubic');

      article.find('.bottom-panel form').hide();
      article.find('.overflow form').show();
    }
    article.toggleClass('art-open');
    $('html,body').animate({'scrollTop':article.offset().top+'px'},500,'easeInOutCubic');
  });
}
function resizeTextOverflow(){
  _textOverFlow.each(function(){
    var t = $(this);
    if(!t.hasClass('art-open')){
      var over = t.find('.overflow');
      over.css('height','auto');
      //
      height = over.height();
      over.css('height',t.data('overflow'));
      //
      if(height>t.data('overflow')){
        t.find('.read-more').fadeIn();
        t.attr('data-finalheight',height);
      }else{
        t.find('.read-more').fadeOut();
      }
    t.attr('data-height',$(this).height()-31);
    }
  });
}
//
//
///////////////////////////////// SET VIDEO
//
//
var _fullScreenTimeout,_fullScreenInterval;
function videoEnd() {
  clearTimeout(_fullScreenTimeout);
  clearInterval(_fullScreenInterval);
  var video = document.getElementById("video");
  video.webkitExitFullscreen();
  video.pause();
  $(video).hide();
}
function goFs(){
  var video = document.getElementById("video");



    video.webkitEnterFullscreen();
  //video.addEventListener('webkitfullscreenchange', checkFS, false);  not fired on IOS
  if(!video.webkitDisplayingFullscreen){
    _fullScreenTimeout = setTimeout(goFs,1000);
  }else{
    _fullScreenInterval = setInterval(checkFS,500);
  }
}
//
function checkFS(){
  var video = document.getElementById("video");
  if(!video.webkitDisplayingFullscreen)videoEnd();
}

/*function playVideo(src) {
  var video = document.getElementById("video");
  video.src = src;
  //
  _fullScreenTimeout = setTimeout(goFs,1000);
  video.addEventListener('ended', videoEnd, false);
  video.play();
   var video = document.getElementById("video");
  video.webkitEnterFullscreen();
}*/
//
function canplaythrough(){
  var video = document.getElementById("video");
  var oTagging = { sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, videoAction: 'play' };
  videoLastEvent = 'play';
  video.play();
  $(video).show();
  goFs();

}

function timeUpdate() {
  var video = document.getElementById("video");
  var percentViewed = 100 * (video.currentTime / video.duration);
  if ((percentViewed == 100) && (videoLastEvent != '100pct')) {
    var oTagging = { sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, videoAction: '100%' };
    FiftyFiveTrack(oTagging);
    videoLastEvent = '100pct';
  }
  else if ((percentViewed >= 75) && (videoLastEvent != '75pct')) {
    var oTagging = { sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, videoAction: '75%' };
    FiftyFiveTrack(oTagging);
    videoLastEvent = '75pct';
  }
  else if ((percentViewed >= 50) && (videoLastEvent != '50pct')) {
    var oTagging = { sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, videoAction: '50%' };
    FiftyFiveTrack(oTagging);
    videoLastEvent = '50pct';
  }
  else if ((percentViewed >= 25) && (videoLastEvent != '25pct')) {
    var oTagging = { sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, videoAction: '25%' };
    FiftyFiveTrack(oTagging);
    videoLastEvent = '25pct';
  }
}

function setVideo(){
  var videoBt = $('.video');
  if(!videoBt.get(0))return;
  //
  videoBt.on('click',function(e){
    e.preventDefault();
    var video = document.getElementById("video");

    video.src = $(this).data('src');
    //

    //else aboslute position of video w100% h100%
    video.addEventListener('ended', videoEnd, false);
    video.addEventListener('canplaythrough', canplaythrough, false);
    video.addEventListener('timeupdate', timeUpdate, false);
    video.load();

  });
}
//
//
///////////////////////////////// SET SCHEMA PRODUCTION CYCLE
//
//
var _schemaPosLeft = 0;
function setSchema(){
  var schema = $('.schema');
  if(!schema.get(0))return;
  schema.find('img').hammer({ drag_lock_to_axis: true }).on("dragleft dragright release ", onSchemaDrag);
}
//
function onSchemaDrag(ev){
  ev.gesture.preventDefault();
  if(ev.type === 'release' ){
    var acc = -200;
    if(_schemaPosLeft < parseInt($(ev.currentTarget).css('margin-left'),10)){
      acc = 200;
    }
    _schemaPosLeft = parseInt($(ev.currentTarget).css('margin-left'),10) + acc;
    //
    if(_schemaPosLeft>0){
      _schemaPosLeft = 0;
    }
    if(_schemaPosLeft<-($(ev.currentTarget).width()-_constant_obj.window_width)){
      _schemaPosLeft = -($(ev.currentTarget).width()-_constant_obj.window_width);
    }
    $(ev.currentTarget).stop().animate({'margin-left': _schemaPosLeft +'px'},500,'easeOutCubic');
  }else{
    $(ev.currentTarget).css({'margin-left': _schemaPosLeft + ev.gesture.deltaX+'px'});
  }
}
//
//
////////////////////////////////////// SET MARGIN TO ARTICLE LIST
//
//
function setLarge(){
  var article = $('.articles-list');
  if(!article.get(0))return;
  var html = article.html();
  if(_constant_obj.isTablet){
    var count = 0;
    //
    if(html.indexOf('<li class="clearfix"></li>') === -1){
      article.children('li').each(function(i){
        if(!$(this).hasClass('large')){
          $(this).addClass('sml');
          if(count%2===1){
            $(this).css({'margin-left':'4%'})//.addClass('clearfix');

           $('<li class="clearfix"></li>').insertAfter(this);
          }
          count++;
        }
      });
    }
  }else{
    if(html.indexOf('<li class="clearfix"></li>') !== -1){
      article.children('li').each(function(i){
        $(this).css({'margin-left':'0%'});
      });
      $('li.clearfix').each(function(){
        if($(this).html() === '')$(this).remove();
      });
    }
  }
}
//
//
///////////////////////////////////// FORM JS
//
//
function setForm(){
  var form = $('form');
  if(!form.get(0))return;
  form.find('select').wrap('<div class="select">');
  //form.find('input[type="submit"]').wrap('<div class="submit">');
  var inputs = form.find('input[type="text"],input[type="email"],textarea');
  inputs.each(function(){
    $(this).attr('data-value',$(this).val());
  });
  form.on('focus','input[type="text"],input[type="email"],textarea',function(){
    if($(this).val() === $(this).data('value')){
        $(this).val('');
    }
  });
  form.on('blur','input[type="text"],input[type="email"],textarea',function(){
    if($(this).val() === ''){
        $(this).val($(this).data('value'));
    }
  });
}
//
//
//////////////////////////// GREAT EXPEDITION
//
//
var _dates
function setDatesExpedition(){
  _dates = $('.dates');
  if(!_dates.get(0))return;
  _dates.children('.year1700').addClass('show');
  $('.year1800').hide();
  //
  var images = _dates.find('img'),counter = 0;
  for (var i = 0; i < images.size(); i++) {
    var img = new Image();
    img.onload = function(){
      counter++;

      if(counter == images.size()){

      _dates.masonry('reload');
      }

    }
    img.src = images.eq(i).attr('src');
  };
  var yearMenu = $('.dates-menu');
  yearMenu.on('click','a',function(e){
    e.preventDefault();
    var t = $(this);
    if(t.hasClass('select'))return;
    var select = yearMenu.find('.select');
    select.removeClass('select');
    t.addClass('select');
    _dates.find('.show').removeClass('show');

    $('.year'+select.html()).hide();
    if(_constant_obj.isTablet)$('.year'+t.html()).addClass('show');
    $('.year'+t.html()).show();
    _dates.masonry('reload');

  });
}
//
//
/////////////////////////////////////    KEY DATES
//
//
var _timelinePosLeft,_timeline,_year,_isDragging = false,_keyDatesTimeout,_isTimelineArticleAnimating = false;
function setKeyDates(){
  _timeline = $('.timeline');
  if(!_timeline.get(0))return;



  _year = parseInt(_timeline.find('span').html(),10);
  var ul =  _timeline.find('ul');
  ul.css({'width':ul.find('li').size()*64+5+'px'});//5 magic number!!!
  _timeline.on('click','a',function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    if(_isTimelineArticleAnimating)return;
    goToYear(ul,$(this).parent());
    clearTimeout(_keyDatesTimeout);
  });
  _timelinePosLeft = _timeline.find('ul').css('margin-left');
  $('.timeline-article').hammer({ drag_lock_to_axis: true }).on("release dragleft dragright swipeleft swiperight",{t:$('.timeline-article')},onKeyDatesArticleSwipe);
  ul.hammer({ drag_lock_to_axis: true }).on("dragleft dragright release ", onTimelineDrag);
  _keyDatesTimeout = setTimeout(goNextDate,10000);

}
function onKeyDatesArticleSwipe(ev){

  if(_isTimelineArticleAnimating)return;
  ev.gesture.preventDefault();
  //
  clearTimeout(_keyDatesTimeout);
  //
  var ul = _timeline.find('ul'),next;
  clearTimeout(_keyDatesTimeout);
  //
  switch(ev.type){
    case 'dragright':
    case 'dragleft':
      //ev.data.t.css({'left': ev.gesture.deltaX+'px'})
      if(Math.abs(ev.gesture.deltaX) > _constant_obj.window_width/4) {
        if(ev.gesture.direction == 'right') {
          next = ul.find('.select').parent().prev();
        } else {
          next = ul.find('.select').parent().next();
        }
      }
    break;
    /*case 'swipeleft':
      next = ul.find('.select').parent().next();
    break;
    case 'dragright':
      next = ul.find('.select').parent().prev();

    break;*/



    case 'release':
    //alert(Math.abs(ev.gesture.deltaX) + ' '+ _constant_obj.window_width/4)

    break;
    }

  if(next.get(0))goToYear(ul,next);
}
//
function goNextDate(){
  var ul = _timeline.find('ul'),next = ul.find('.select').parent().next();
  if(!next.get(0))next = ul.find('li').eq(0);
  goToYear(ul,next);
  _keyDatesTimeout = setTimeout(goNextDate,10000);
}
//
function onTimelineDrag(ev){
  if(_isTimelineArticleAnimating)return;
  ev.gesture.preventDefault();
  clearTimeout(_keyDatesTimeout);
  if(ev.type === 'release' ){
    if(_isDragging)if(parseInt($(ev.currentTarget).css('margin-left'),10) !== _timelinePosLeft)getYearByPosition($(ev.currentTarget));
    _isDragging = false;
  }else{
    if(Math.abs(ev.gesture.deltaX)>3){
      _isDragging = true;
      $(ev.currentTarget).css({'margin-left': _timelinePosLeft + ev.gesture.deltaX+'px'});
    }
  }
}
//
function getYearByPosition(ul){
  var min_array = {},minValue = 1000,value;
  ul.find('li').each(function(i){
    value = Math.round(Math.abs($(this).offset().left-_constant_obj.window_width/2));
    min_array[value] = i;
    minValue = Math.min(minValue,value);
  });
  goToYear(ul,ul.find('li').eq(min_array[minValue]));
}
//

function setSpanYear(){

  if(_tempYear===_year)return;
  var step = 1;
  if(Math.abs(_tempYear-_year)>11)step =11;
  if(Math.abs(_tempYear-_year)>101)step =101;
  if(_tempYear>_year){
    _tempYear-=step;
  }else if(_tempYear<_year){
    _tempYear+=step;
  }
  if(_tempYear !== _year)setTimeout(setSpanYear,60);
  _timeline.find('span').text(_tempYear);
}
//
var _tempYear,_lastYear;
function goToYear(ul,li){
  _timelinePosLeft = _timeline.width() / 2 - li.position().left - 15;
  ul.stop().animate({'margin-left': _timelinePosLeft+'px'},300);
  //
  _timeline.find('.select').removeClass('select');
  var year = parseInt(li.find('a').addClass('select').data('year'),10);

  if(_year === year)return;
  _isTimelineArticleAnimating = true;

  _tempYear = _lastYear =_year;
  _year = year;
  setSpanYear();
  $.ajax({
    url: li.find('a').attr('href'),
    dataType: "html",
    beforeSend:function(){
      _timeline.find('img').css({'opacity':1});
    },
    success:function(html){
      _timeline.find('img').css({'opacity':0});
      if(_lastYear<_year){
        $('.timeline-article-content').css({'width':'200%'}).append(html).animate({'margin-left':'-100%'},500,'easeInOutCubic',function(){
          onYearAnimationEnded(0);
        }).find('article').css({'width':'50%'});
      }else{
        $('.timeline-article-content').css({'width':'200%','margin-left':'-100%'}).prepend(html).animate({'margin-left':'0%'},500,'easeInOutCubic',function(){
          onYearAnimationEnded(1);
        }).find('article').css({'width':'50%'});
      }
      setArticleParagraph();
    }
  });
}
function onYearAnimationEnded(id){
  $('.timeline-article-content').find('article').eq(id).remove();
  $('.timeline-article-content').css({'width':'100%','margin-left':'0%'}).find('article').css({'width':'100%'});
  setTextOverFlow();

  _isTimelineArticleAnimating = false;
}
//
function resizeTimeline(){
  var ul = _timeline.find('ul');
  goToYear(ul,ul.find('.select').parent());

}
//
//
//////////////////////////////////////  ANCHORS
//
//
var _anchors,_footer,_scrollTop,_isAnchorPosAbsolute=false;
function setAnchors(){
  var article =  $('.article:not(.news),.store');
  if(!article.get(0) && !$('.news'))return;
  if(article.size()<2)return;
  var html = '<div class="anchors"><a href="#" class="anchors-menu">anchors</a><ul>';
  var title,h;
  article.each(function(i){
    h = $(this).find('h3');
    if(h.get(0)){
      title = h.text();
      html += '<li><a href="#article'+i+'">'+title+'</a></li>';
      $(this).attr('data-anchor','article'+i);
    }
  });
  html += "</ul></div>";
  $('.content').append(html);
  _anchors = $('.anchors');
  var ulHeight = _anchors.find('ul').height();
  _anchors.attr('data-height',ulHeight).css({'margin-bottom':-ulHeight+'px'});
  _anchors.on('click','.anchors-menu',function(e){
    e.preventDefault();
    _anchors.toggleClass('open');
  });
  var hash;
  _anchors.on('click','ul  a',function(e){
    e.preventDefault();
    _anchors.removeClass('open');
    hash = $(this).attr('href').replace('#','');
    goToAnchor(hash);
  });
  $('.news').on('click','a',function(e){
    if (!$(this).hasClass('noanchor')) {
      e.preventDefault();
      hash = $(this).attr('href').replace('#','');
      goToAnchor(hash);
    }
  });
  _footer =$('.main-footer');
  $('.content').css({'padding-bottom':'50px'});
  _scrollTop = 0;
  checkAnchorsPosition();
}
function checkAnchorsPosition(){
  setTimeout(checkAnchorsPosition,100);
  if(_scrollTop == _constant_obj.window.scrollTop())return;
  _scrollTop = _constant_obj.window.scrollTop();
  if(_scrollTop+_constant_obj.window_height>_footer.offset().top){
    if(!_isAnchorPosAbsolute){
      _isAnchorPosAbsolute = true;
      _anchors.css({'position':'absolute'});
    }
  }else{
    if(_isAnchorPosAbsolute){
      _isAnchorPosAbsolute = false;
      _anchors.css({'position':'fixed'});
    }
  }
}
function goToAnchor(anchor){
  window.location.hash = anchor;
  $('html,body').animate({'scrollTop':$('[data-anchor="'+anchor+'"]').offset().top},1000,'easeInOutCubic');
}
//
//
/////////////////////////////////////// ARTICLE IMG LOADER
//
//
var _isImgLoaded = false;
function loadArticleImg(){
  var img = $('.article figure img,.store figure img');
  if(!img.get(0))return;
  var imgCounter = 0;
  img.each(function(){

    var fakeImg = new Image();
    fakeImg.onload = function(){
      imgCounter++;
      if(imgCounter == img.size()){

        _isImgLoaded = true;
        onResize();
        checkLocationHash();
      }
    };
    fakeImg.src = $(this).attr('src');
  });
}
function setImgSize(){
  $('.article figure img,.store figure img,.push figure img').each(function(){
    //alert($(this).width()/$(this).height())
    if($(this).width()/$(this).height()<2.5){//PANORAMA IMAGE

      if($(this).parent().width()<369){
        $(this).css({'height':'100%','width':'auto'});
        $(this).css({'position':'relative','left':'0','margin-left':$(this).parent().width()/2 - $(this).width()/2+'px','margin-top':'0px'});
      }else{
        $(this).css({'width':'100%','height':'auto'});
        $(this).css({'position':'relative','left':'0','margin-top':$(this).parent().height()/2 - $(this).height()/2+'px','margin-left':'0px'});
      }
    }

  });
}



//
//
////////////////////////////////////// CHECKLOACTIONHASH
//
//
function checkLocationHash(){
  var hash = window.location.hash.replace('#','');
  if(hash !== '')$('html,body').animate({'scrollTop':$('[data-anchor="'+hash+'"]').offset().top},10);
}
//
//
//////////////////////////////////////  ARTICLE PARAGRAPHE
//
//
function setArticleParagraph(){
  $('.article').each(function(){
    var t = $(this),p = t.find('p'),count = p.text().length
    if(count<220 && !t.parent().hasClass('sml'))p.addClass('nocolumn');
  })
}

//////////////////////////////////////  Slider top

var globalConfig = {
  clickEvent: 'click'
};
if(Modernizr.touch)
{
  //globalConfig.clickEvent = 'touchstart';
}

var bigSlides = [];
var smallSlides = [];
function setSlider()
{
  if(_constant_obj.isTablet)
  {
    var slideBlock = jQuery('.slideme');
    var slideConfig = {pager: false,adaptiveHeight: true,infiniteLoop: false,hideControlOnEnd: true,oneToOneTouch: false};

    /*if(Modernizr.touch)
    {
      slideConfig.controls = false;
    }*/

    slideBlock.each(function()
    {
      var slideB = jQuery(this);

      var slideCount = slideB.children('li').length;

      if(slideCount > 1)
      {
        if(!slideB.parent().hasClass('bx-viewport'))
        {
          slideB.wrap('<div id="slide-top"><div class="slide-block"></div></div>');
          var slider = slideB.bxSlider(slideConfig);
          var slideBlockHeight = slideB.outerHeight();
          slideB.find('li.pagecarnet').css({'height': slideBlockHeight});

          bigSlides.push(slider);
        }
      }
    });
  }
  else
  {
    var slideBlock = jQuery('.date-vendange');
    var slideConfig = {pager: false,infiniteLoop: false,hideControlOnEnd: true,oneToOneTouch: false};

    //
    
    //,controls: false
    slideBlock.each(function()
    {
      var slideB = jQuery(this);

      var slideCount = slideB.children('li').length;

      if(slideCount > 1)
      {
        if(!slideB.parent().hasClass('bx-viewport'))
        {
          var slider = slideB.bxSlider(slideConfig);
          smallSlides.push(slider);
        }
      }
    });
  }
}

function removeSlider(sColl)
{
  for(var i=0,count=sColl.length;i<count;i++)
  {
    sColl[i].destroySlider();
  }
}

//////////////////////////////////////  Images centrées verticalement

function verticalMiddle()
{
  jQuery('.middle figure').each(function()
  {
    var fig = jQuery(this);
    
    var maxHeight = fig.outerHeight();
    var maxWidth = fig.outerWidth();
    var img = fig.find('img');
    var height = img.outerHeight();
    var width = img.outerWidth();

    var top = parseInt((parseInt(maxHeight)-parseInt(height))/2);
    var left = parseInt((parseInt(maxWidth)-parseInt(width))/2);
    
    if(!img.parent().hasClass('middle-figure'))
    {
      img.wrap('<span class="middle-figure" style="position:relative;top:'+top+'px;"/>');
    }
    else
    {
      img.parent().css({'top': top});
    }
  });
}

//////////////////////////////////////

var overflowConfig = {
  maxHeight: 250,
  openClass: 'art-open'
};
function setBlockOverflow()
{
  jQuery('.block-overflow.odd,.block-overflow.even').each(function()
  {
    var nextId = '';
    var item = jQuery(this);
    var next = item.next();
    if(next.length != 0)
    {
      nextId = next.attr('id');
    }
    var more = jQuery('.more.cvend',item);

    var blockC = jQuery('.carnet-content',item);
    blockC.css({'height': 'auto'});
    item.attr('data-maxheight',blockC.outerHeight());
    
    if(more.length == 0)
    {
      var more = jQuery('<a href="#" class="more cvend">read more</a>').on(globalConfig.clickEvent,function(ev)
      {
        var block = jQuery(this).parent();
        var blockC = jQuery('.carnet-content',block);

        if(block.hasClass(overflowConfig.openClass))
        {
          blockC.animate({'height': overflowConfig.maxHeight+'px'},600,'easeInOutCubic',function()
          {
            if(nextId != '')
            {
              var nextItem = $('#'+nextId);
              $('html, body').animate({
                scrollTop: nextItem.offset().top
              }, 500);
            }
          });
        }
        else
        {
          blockC.animate({'height': parseInt(block.attr('data-maxheight'))+'px'},600,'easeInOutCubic');
          
        }
        
        block.toggleClass(overflowConfig.openClass);
        //jQuery('html,body').animate({'scrollTop':blockC.offset().top+'px'},500,'easeInOutCubic');

        ev.preventDefault();
        ev.stopPropagation();
        return false;
      });
      
      item.append(more);
    }

    /*setTimeout(function(){
        blockC.css({'overflow': 'hidden','height': overflowConfig.maxHeight});
    },3);*/

    blockC.css({'overflow': 'hidden','height': overflowConfig.maxHeight});
    
  });
}

var carouselObjectCollection = {};

function setCarousel()
{
  var carouselCollection = jQuery('.carousel');

  carouselCollection.each(function()
  {
    var element = jQuery(this);

    var items = jQuery('ul.date-vendange > li',element);
    if(items.length > 1)
    {
      var carousel = new Carousel(element);
      carousel.init();

      var key = element.parents('.carnet-content').attr('id');

      if(key !== undefined && key != '')
      {
        carouselObjectCollection[key] = carousel;
      }
    }
  });
}

function setDateScroll()
{
  var deltaH = jQuery(window).height()/4;
  var hash = window.location.hash;
  
  var hashExp = /#(([a-z]+-\d+)-([a-z]+-\d+))/;
  var hashRes = hash.match(hashExp);

  if(hashRes && hashRes.length >= 3)
  {
    var itemKey = hashRes[2];
    var item = jQuery('#'+itemKey);
    jQuery('html, body').animate({
      scrollTop: item.offset().top-deltaH
    }, 500,'swing',function()
    {
      var dateKey = hashRes[1];
      var dateItem = jQuery('[data-id='+dateKey+']');
      
      if(carouselObjectCollection[itemKey] !== undefined)
      {
        var carouselItem = carouselObjectCollection[itemKey];
        var dateIndex = null;
        jQuery('.date-vendange > li',item).each(function(ind)
        {
          var date = jQuery(this);
          if(date.data('id') == dateKey)
          {
            dateIndex = ind;
          }
        });

        if(dateIndex !== null)
        {
          carouselItem.showPane(dateIndex,true);
        }
      }
    });
  }
}

//
//
//////////////////////////////////////  MAIN
//
//
$(function(){
  setConstant();
  setNav();
  setVideo();
  setArticleParagraph();
  setSchema();
  setForm();
  setAnchors();
  setDatesExpedition();
  setKeyDates();
  loadArticleImg();

  //
  


  if(_constant_obj.isTablet)
  {
    //verticalMiddle();
    setSlider();
  }
  else
  {
    setCarousel();
    setBlockOverflow();

    setDateScroll();
  }

  setTextOverFlow();
  //
  if($(".page-weareclicquot").length === 0) {
    $('.slideShow').each(function(){
      var slide = new SlideShow($(this));
      _constant_obj.slideArray.push(slide);
    });
  } else {
      var slide = new SlideShow($('.slideShow'),0);
      _constant_obj.slideArray.push(slide);

      $("#episode_nextSlide").on('click', function(e) {
        var slideTopPos = $(".slideShow").offset().top;
        $('html,body').animate({ scrollTop: slideTopPos }, 'slow');
      });

      $(".prev").on('click', function(e) {
        var prevSlide = slide.currentSlide-1;
        slide.goToSlide(prevSlide, "left");
      });

      $(".next").on('click', function(e) {
        var nextSlide = slide.currentSlide+1;
        var direction = 'right';
        if (nextSlide == slide.slideSize) {
          nextSlide = 0;
          direction = 'left';
        }
        slide.goToSlide(nextSlide, direction);
      });

      $(".backIntro").on('click', function(e) {
        var introPosTop = $(".intro").offset().top;
        $('html,body').animate({ scrollTop: introPosTop }, 'slow');
        slide.goToSlide(0, "left");
      });

      if (window.location.hash)
      {
          var idSlide = parseInt(window.location.hash.replace('#slide', ''));
          slide.goToSlide(idSlide, 'right');
      }

      var margTopSlider = ($(".slideShow .slider-video").innerHeight()/2) - ($(".nav-slider").innerHeight()/2) + 30;
      $(".nav-slider").css("top", margTopSlider);

      // Fonction UPDATE slider HEIGHT on OrientationChange
      _constant_obj.window.bind('resize orientationchange', function() {
        var h = 0;
        $(".slideShow .wrap").children('ul:not(.noslide)').find('li:not(.noslide)').each(function(){
          h = Math.max(h,$(this).height());
        });
        $(".slideShow").height(h);//40 dots height
        $(".slideShow .wrap").height(h);
        $(".slideShow").children('ul:not(.noslide)').height(h);
      });
  }
  //
  _constant_obj.window.bind('resize orientationchange',onResize);
  _constant_obj.window.trigger('resize');
  //

  //$('.carnet-content').wrapInner('<div class="scroll-block"/>');
});

function onResize(){
  _constant_obj.window_width = _constant_obj.window.width();
  _constant_obj.window_height = _constant_obj.window.height();
  //
  _constant_obj.isTablet = true;
  if($('.tablet').css('display')==='none')_constant_obj.isTablet=false;

  resizeNav();
  if(_dates.get(0) && _constant_obj.isTablet){
      var column = Math.floor($('.content').width()/236);
      var gutter = Math.floor(($('.content').width()-(236*column))/(column-1));
      _dates.masonry({
        itemSelector : 'li.show',
        gutterWidth:gutter
      });

  }
  if(_isImgLoaded)setImgSize();
  if(_timeline.get(0))resizeTimeline();

  //setSlider();

  if(_constant_obj.isTablet)
  {
    //verticalMiddle();
  }
  else
  {
    setBlockOverflow();
  }

  if(_textOverFlow.get(0))resizeTextOverflow();

  //
  setLarge();
  for (var i = 0; i < _constant_obj.slideArray.length; i++) {
    _constant_obj.slideArray[i].setWidth();
  }
}