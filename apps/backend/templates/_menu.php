<?php $current_route = $sf_context->getRouting()->getCurrentRouteName(); ?>
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand" href="<?php echo url_for('@homepage'); ?>"><?php echo image_tag('/images/backend/logo.jpg',array('alt'=>'Veuve Clicquot','id'=>'logo')); ?>Veuve Clicquot</a>
      <div class="nav-collapse collapse">
        <ul class="nav">
          <li class="dropdown<?php echo Utils::startsWith($current_route, array('fiche_produit', 'bloc_fiche', 'note_fiche', 'offre', 'bloc_offre'))?' active':''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Produits <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li<?php echo Utils::startsWith($current_route, array('fiche_produit', 'bloc_fiche', 'note_fiche'))?' class="active"':''; ?>><?php echo link_to('Fiches','@fiche_produit', array('title'=>'Fiches')); ?></li>
              <li<?php echo Utils::startsWith($current_route, array('offre', 'bloc_offre'))?' class="active"':''; ?>><?php echo link_to('Offres', '@offre', array('title' => 'Offres')); ?></li>
            </ul>
          </li>
          <li class="dropdown<?php echo Utils::startsWith($current_route, array('article', 'bloc_article', 'women', 'great_expedition', 'production_cycle', 'carnet_vendange'))?' active':''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Éditorial <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li<?php echo Utils::startsWith($current_route, array('article', 'bloc_article'))?' class="active"':''; ?>><?php echo link_to('Articles', '@article', array('title' => 'Articles')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'women_page')?' class="active"':''; ?>><?php echo link_to('Configuration de la page WOI', '@women_page', array('title' => 'Configuration de la page WOI')); ?></li>
              <li<?php echo Utils::startsWith($current_route, 'women_of')?' class="active"':''; ?>><?php echo link_to('Gestion des WOI', '@women_of_inspiration', array('title' => 'Gestion des WOI')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'great_expedition_page')?' class="active"':''; ?>><?php echo link_to('Configuration de la page Great Expeditions', '@great_expedition_page', array('title' => 'Configuration de la page Great Expeditions')); ?></li>
              <li<?php echo (Utils::startsWith($current_route, 'great_expedition') && !Utils::startsWith($current_route, 'great_expedition_page'))?' class="active"':''; ?>><?php echo link_to('Gestion des Expeditions', '@great_expedition', array('title' => 'Gestion des Expeditions')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'date_clef')?' class="active"':''; ?>><?php echo link_to('Dates clefs', '@date_clef', array('title' => 'Dates clefs')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'production_cycle')?' class="active"':''; ?>><?php echo link_to('Production Cycle', '@production_cycle', array('title' => 'Production Cycle')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'carnet_vendange_page')?' class="active"':''; ?>><?php echo link_to('Configuration de la page Carnet de Vendanges', '@carnet_vendange_page', array('title' => 'Configuration de la page Carnet de Vendanges')); ?></li>
              <li<?php echo (Utils::startsWith($current_route, 'carnet_vendange') && !Utils::startsWith($current_route, 'carnet_vendange_page'))?' class="active"':''; ?>><?php echo link_to('Gestion des Carnets de Vendanges', '@carnet_vendange', array('title' => 'Gestion des Carnets de Vendanges')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'we_are_clicquot_page')?' class="active"':''; ?>><?php echo link_to('Configuration de la page We Are Clicquot', '@we_are_clicquot_page', array('title' => 'Configuration de la page We Are Clicquot')); ?></li>
              <li<?php echo (Utils::startsWith($current_route, 'we_are_clicquot') && !Utils::startsWith($current_route, 'we_are_clicquot_page'))?' class="active"':''; ?>><?php echo link_to('Gestion des épisodes We Are Clicquot', '@we_are_clicquot', array('title' => 'Gestion des épisodes We Are Clicquot')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'page_statique')?' class="active"':''; ?>><?php echo link_to('Pages statiques', '@page_statique', array('title' => 'Pages statiques')); ?></li>
              <li class="divider"></li>
              <li<?php echo Utils::startsWith($current_route, 'actualite')?' class="active"':''; ?>><?php echo link_to('Actualités', '@actualite', array('title' => 'Actualités')); ?></li>
            </ul>
          </li>
          <li<?php echo Utils::startsWith($current_route, 'store')?' class="active"':''; ?>><?php echo link_to('Stores', '@store_locator', array('title' => 'Stores')); ?></li>
          <li<?php echo Utils::startsWith($current_route, 'bloc_push')?' class="active"':''; ?>><?php echo link_to('Blocs push','@bloc_push', array('title'=>'Blocs push')); ?></li>
          <li<?php echo Utils::startsWith($current_route, 'lien')?' class="active"':''; ?>><?php echo link_to('Menus', '@lien', array('title' => 'Menus')); ?></li>
          <li class="dropdown<?php echo Utils::startsWith($current_route, array('langue', 'pays'))?' active':''; ?>">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Internationalisation <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li<?php echo Utils::startsWith($current_route, 'langue')?' class="active"':''; ?>><?php echo link_to('Langues', '@langue', array('title' => 'Langues')); ?></li>
              <li<?php echo Utils::startsWith($current_route, 'pays')?' class="active"':''; ?>><?php echo link_to('Pays', '@pays', array('title' => 'Pays')); ?></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>