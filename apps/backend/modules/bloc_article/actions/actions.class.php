<?php

require_once dirname(__FILE__).'/../lib/bloc_articleGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/bloc_articleGeneratorHelper.class.php';

/**
 * bloc_article actions.
 *
 * @package    veuveclicquot
 * @subpackage bloc_article
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bloc_articleActions extends autoBloc_articleActions {
    public function executeNew(sfWebRequest $request) {
        parent::executeNew($request);
        $this->form->setDefault('article_id', $request->getGetParameter('article_id'));
    }
}
