<?php if (!$article->isNew()): ?>
<h2>Blocs</h2>
<a href="<?php echo url_for('bloc_article/new?article_id=' . $article->getId()); ?>" class="btn btn-success btn-small">Ajouter un bloc</a>
<?php $aBlocsTries = $article->getBlocsTries(); ?>
<?php if (count($aBlocsTries)): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Titre bloc</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aBlocsTries as $oBlocArticle): ?>
    <?php $oBlocArticle instanceof BlocArticle; ?>
    <tr>
      <td><?php echo $oBlocArticle->getId(); ?></td>
      <td><?php echo $oBlocArticle->getTitre(); ?></td>
      <td>
        <?php echo link_to('Éditer', '@bloc_article_edit?id=' . $oBlocArticle->getId(), array('class' => 'btn btn-primary btn-mini')); ?>
      </td>
      <td>
        <?php echo link_to('Supprimer', '@bloc_article_delete?id=' . $oBlocArticle->getId(), array('class' => 'btn btn-danger btn-mini', 'method' => 'delete', 'confirm' => 'Êtes vous sûr ?')); ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<h2>Blocs Push</h2>
<?php $aBlocsPushTries = AssocArticleBlocPush::getBlocsTries($article->id); ?>
<?php $iNbBlocsPushTries = count($aBlocsPushTries); ?>
<?php if ($iNbBlocsPushTries > 0): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Description</th>
      <th colspan="3">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aBlocsPushTries as $index => $oBlocPushTrie): ?>
    <?php $oBlocPushTrie instanceof AssocOffreBlocPush; ?>
    <tr>
      <td><?php echo $oBlocPushTrie->getBlocPushId(); ?></td>
      <td><?php echo $oBlocPushTrie->getBlocPush()->getDescription(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsPushTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_a?article_id=' . $article->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=bas'); ?>"><icon class="icon-arrow-down" /></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_a?article_id=' . $article->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=haut'); ?>"><icon class="icon-arrow-up" /></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td><a href="<?php echo url_for('@suppr_assoc_bp_a?article_id=' . $article->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId()); ?>" class="btn btn-mini btn-danger">Supprimer</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<form action="<?php echo url_for('@ajout_assoc_bp_a'); ?>" method="post">
  <input type="hidden" name="article_id" value="<?php echo $article->getId(); ?>" />
  <?php $aBlocsPush = AssocArticleBlocPush::getBlocsNotIn($article->getId()); ?>
  <select name="bloc_push_id">
    <option value="">- Aucun -</option>
    <?php foreach($aBlocsPush as $oBlocPush): ?>
    <option value="<?php echo $oBlocPush->getId(); ?>"><?php echo $oBlocPush->getDescription(); ?></option>
    <?php endforeach; ?>
  </select>&nbsp;&nbsp;
  <input type="submit" name="act" value="Ajouter" />
</form>
<?php endif; ?>