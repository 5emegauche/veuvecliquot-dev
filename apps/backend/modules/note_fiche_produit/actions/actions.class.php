<?php

require_once dirname(__FILE__).'/../lib/note_fiche_produitGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/note_fiche_produitGeneratorHelper.class.php';

/**
 * note_fiche_produit actions.
 *
 * @package    veuveclicquot
 * @subpackage note_fiche_produit
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class note_fiche_produitActions extends autoNote_fiche_produitActions {
    public function executeNew(sfWebRequest $request) {
        parent::executeNew($request);
        $this->form->setDefault('fiche_produit_id', $request->getGetParameter('fiche_produit_id'));
    }

    public function executeOrdre(sfWebRequest $request) {
    	$iFicheId = $request->getParameter('fiche_produit_id');
        $iNoteId  = $request->getParameter('note_id');
        $sSens    = $request->getParameter('sens');

        $oNote = Doctrine::getTable('NoteFicheProduit')->find($iNoteId);

        if (!$oNote) {
        	$this->getUser()->setFlash('error', 'Note inexistante');
        }
        else {
        	$iAnciennePosition = $oNote->getOrdre();
	        switch($sSens) {
	            case 'bas':
	                $iNouvellePosition = $iAnciennePosition + 1;
	                break;
	            case 'haut':
	                $iNouvellePosition = $iAnciennePosition - 1;
	                break;
	        }
	        $oEchange = Doctrine::getTable('NoteFicheProduit')
	        					->createQuery('n')
	        					->where('n.fiche_produit_id = ?', $iFicheId)
	        					->andWhere('n.ordre = ?', $iNouvellePosition)
	        					->fetchOne();
	        if (!$oEchange) {
	            $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
	        }
	        else {
	            try {
	                $oNote->setOrdre($iNouvellePosition)->save();
	                $oEchange->setOrdre($iAnciennePosition)->save();
	                $this->getUser()->setFlash('notice', 'Déplacement effectué');
	            } catch (Exception $e) {
	                $this->getUser()->setFlash('error', $e->getMessage());
	            }
	        }
        }
        $this->redirect($request->getReferer());
    }
}
