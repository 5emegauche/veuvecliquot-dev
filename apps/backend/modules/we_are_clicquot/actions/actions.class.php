<?php

require_once dirname(__FILE__).'/../lib/we_are_clicquotGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/we_are_clicquotGeneratorHelper.class.php';

/**
 * we_are_clicquot actions.
 *
 * @package    veuveclicquot
 * @subpackage we_are_clicquot
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class we_are_clicquotActions extends autoWe_are_clicquotActions
{
    public function executeChangeordre(sfWebRequest $request) {
        $aPages = $request->getPostParameter('pageweareclicquot');
        $iOrdre = 1;
        foreach($aPages as $iPage) {
            $oPageWeAreClicquot = Doctrine::getTable('WeAreClicquot')->find($iPage);
            $oPageWeAreClicquot->setOrdre($iOrdre)->save();
            $iOrdre++;
        }
    }
}
