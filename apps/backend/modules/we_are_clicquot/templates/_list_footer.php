<h2>Ordre des pages</h2>
<ul id="sortable">
  <?php foreach(WeAreClicquot::retrievePagesWeAreClicquot() as $oPageWAC): ?>
  <li class="ui-state-default" id="pagecarnet_<?php echo $oPageWAC['id']; ?>">
    <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
    <?php echo strip_tags($oPageWAC['titre']); ?>
  </li>
  <?php endforeach; ?>
</ul>
<button id="btn-change-order">Enregistrer ordre</button><br />

<div id="messageModal" style="display:none;">
Enregistrement de l'ordre en cours<span class="point1 point_de_suspension">.</span><span class="point2 point_de_suspension">.</span><span class="point3 point_de_suspension">.</span>
</div>

<script type="text/javascript">
$(function() {
  $('#sortable').sortable({
    revert: true
  });
  $('#sortable').disableSelection();

  jQuery('#messageModal').dialog({
    height: 150,
    width: 200,
    modal: true,
    autoOpen: false,
    closeOnEscape: false,
    resizable: false,
    draggable: false,
    title: "Duplication de carte",
    open: function(event, ui) { jQuery('.ui-dialog-titlebar-close').hide(); }
  });

  $('#btn-change-order').on('click', function(e) {
    e.preventDefault();
    jQuery('#messageModal').dialog('open');
    var sChaineEnvoi = $('#sortable').sortable("serialize");
    $.ajax({
      url: '<?php echo url_for('ajax_ordre_we_are_clicquot'); ?>',
      type: 'POST',
      async: true,
      cache: false,
      crossDomain: false,
      data: sChaineEnvoi,
      dataType: 'json',
      success: function(data) {
        jQuery('#messageModal').dialog('close');
      }
    });
  })
});
</script>