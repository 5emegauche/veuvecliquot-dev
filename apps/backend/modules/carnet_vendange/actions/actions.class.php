<?php

require_once dirname(__FILE__).'/../lib/carnet_vendangeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/carnet_vendangeGeneratorHelper.class.php';

/**
 * carnet_vendange actions.
 *
 * @package    veuveclicquot
 * @subpackage carnet_vendange
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class carnet_vendangeActions extends autoCarnet_vendangeActions {
    public function executeChangeordre(sfWebRequest $request) {
        $aPages = $request->getPostParameter('pagecarnet');
        $iOrdre = 1;
        foreach($aPages as $iPage) {
            $oPageCarnet = Doctrine::getTable('CarnetVendange')->find($iPage);
            $oPageCarnet->setOrdre($iOrdre)->save();
            $iOrdre++;
        }
    }
}
