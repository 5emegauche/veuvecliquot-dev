<?php $cultures = sfConfig::get('app_cultures'); ?>
<div class="tabbable tab-manager">
  <ul class="nav nav-tabs">
    <?php foreach($cultures as $i => $c): ?>
    <li<?php echo $i == 'fr' ? ' class="active"' : '' ?>><a href="#form<?php echo $i ?>" data-toggle="tab"><img src="/images/flags/<?php echo $i; ?>.png" />&nbsp;<?php echo $c ?></a></li>
    <?php endforeach ?>
  </ul>
  <div class="tab-content">
    <?php foreach($cultures as $i => $c): ?>
    <div class="tab-pane<?php echo $i == 'fr' ? ' active' : '' ?>" id="form<?php echo $i ?>">
      <?php echo $form[$i]->render();?>
    </div>
    <?php endforeach ?>
  </div>
</div>