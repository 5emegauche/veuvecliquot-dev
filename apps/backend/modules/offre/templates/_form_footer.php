<?php if (!$offre->isNew()): ?>
<h2 id="blocs">Blocs</h2>
<a href="<?php echo url_for('bloc_offre/new?offre_id=' . $offre->getId()); ?>" class="btn btn-success btn-small">Ajouter un bloc</a>
<?php $aBlocsTries = $offre->getBlocsTries(); ?>
<?php if (count($aBlocsTries)): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Titre bloc</th>
      <th>Ordre</th>
      <th colspan="4">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $iNbBlocsTries = count($aBlocsTries); ?>
    <?php foreach($aBlocsTries as $index => $oBlocOffre): ?>
    <?php $oBlocOffre instanceof BlocOffre; ?>
    <tr>
      <td><?php echo $oBlocOffre->getId(); ?></td>
      <td><?php echo $oBlocOffre->getTitre(); ?></td>
      <td><?php echo $oBlocOffre->getOrdre(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_bloc_offre?offre_id=' . $offre->id . '&bloc_id=' . $oBlocOffre->id . '&sens=bas'); ?>"><i class="icon-arrow-down">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_bloc_offre?offre_id=' . $offre->id . '&bloc_id=' . $oBlocOffre->id . '&sens=haut'); ?>"><i class="icon-arrow-up">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php echo link_to('Éditer', '@bloc_offre_edit?id=' . $oBlocOffre->getId(), array('class' => 'btn btn-primary btn-mini')); ?>
      </td>
      <td>
        <?php echo link_to('Supprimer', '@bloc_offre_delete?id=' . $oBlocOffre->getId(), array('class' => 'btn btn-danger btn-mini', 'method' => 'delete', 'confirm' => 'Êtes vous sûr ?')); ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<h2>Blocs Push</h2>
<?php $aBlocsPushTries = AssocOffreBlocPush::getBlocsTries($offre->id); ?>
<?php $iNbBlocsPushTries = count($aBlocsPushTries); ?>
<?php if ($iNbBlocsPushTries > 0): ?>
<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Description</th>
      <th colspan="3">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aBlocsPushTries as $index => $oBlocPushTrie): ?>
    <?php $oBlocPushTrie instanceof AssocOffreBlocPush; ?>
    <tr>
      <td><?php echo $oBlocPushTrie->getBlocPushId(); ?></td>
      <td><?php echo $oBlocPushTrie->getBlocPush()->getDescription(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsPushTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_o?offre_id=' . $offre->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=bas'); ?>"><i class="icon-arrow-down">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_o?offre_id=' . $offre->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=haut'); ?>"><i class="icon-arrow-up">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td><a href="<?php echo url_for('@suppr_assoc_bp_o?offre_id=' . $offre->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId()); ?>" class="btn btn-mini btn-danger">Supprimer</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<form action="<?php echo url_for('@ajout_assoc_bp_o'); ?>" method="post">
  <input type="hidden" name="offre_id" value="<?php echo $offre->getId(); ?>" />
  <?php $aBlocsPush = AssocOffreBlocPush::getBlocsNotIn($offre->getId()); ?>
  <select name="bloc_push_id">
    <option value="">- Aucun -</option>
    <?php foreach($aBlocsPush as $oBlocPush): ?>
    <option value="<?php echo $oBlocPush->getId(); ?>"><?php echo $oBlocPush->getDescription(); ?></option>
    <?php endforeach; ?>
  </select>&nbsp;&nbsp;
  <input type="submit" name="act" value="Ajouter" />
</form>
<?php endif; ?>