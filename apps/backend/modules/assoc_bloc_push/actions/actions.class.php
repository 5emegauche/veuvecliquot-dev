<?php

/**
 * assoc_bloc_push actions.
 *
 * @package    veuveclicquot
 * @subpackage assoc_bloc_push
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class assoc_bloc_pushActions extends sfActions {
    
    /***********************\
    * Ajouts d'associations *
    \***********************/
    
    public function executeAjoutfp(sfWebRequest $request) {
        $iFicheProduitId = $request->getPostParameter('fiche_produit_id', null);
        if (!$iFicheProduitId) {
            $this->getUser()->setFlash('error', 'Aucune fiche produit définie');
            $this->redirect('@homepage');
        }
        else {
            $iBlocPushId = $request->getPostParameter('bloc_push_id', null);
            if (!$iBlocPushId) {
                $this->getUser()->setFlash('error', 'Aucun bloc push défini');
            }
            else {
                $oMaxAssoc = Doctrine::getTable('AssocFicheProduitBlocPush')
                                     ->createQuery('afpbp')
                                     ->where('afpbp.fiche_produit_id = ?', $iFicheProduitId)
                                     ->orderBy('afpbp.ordre DESC')
                                     ->fetchOne();
                
                $iOrdre = ($oMaxAssoc)?($oMaxAssoc->getOrdre() + 1):1;
                
                try {
                    $oAssoc = new AssocFicheProduitBlocPush();
                    $oAssoc->setFicheProduitId($iFicheProduitId)
                           ->setBlocPushId($iBlocPushId)
                           ->setOrdre($iOrdre)
                           ->save();
                    
                    $this->getUser()->setFlash('notice', 'Association créée');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
            $this->redirect($request->getReferer());
        }
    }
    
    public function executeAjoutpst(sfWebRequest $request) {
        $iPageStatiqueId = $request->getPostParameter('page_statique_id', null);
        if (!$iPageStatiqueId) {
            $this->getUser()->setFlash('error', 'Aucune page statique définie');
            $this->redirect('@homepage');
        }
        else {
            $iBlocPushId = $request->getPostParameter('bloc_push_id', null);
            if (!$iBlocPushId) {
                $this->getUser()->setFlash('error', 'Aucun bloc push défini');
            }
            else {
                $oMaxAssoc = Doctrine::getTable('AssocPageStatiqueBlocPush')
                                     ->createQuery('a')
                                     ->where('a.page_statique_id = ?', $iPageStatiqueId)
                                     ->orderBy('a.ordre DESC')
                                     ->fetchOne();
                
                $iOrdre = ($oMaxAssoc)?($oMaxAssoc->getOrdre() + 1):1;
                
                try {
                    $oAssoc = new AssocPageStatiqueBlocPush();
                    $oAssoc->setPageStatiqueId($iPageStatiqueId)
                           ->setBlocPushId($iBlocPushId)
                           ->setOrdre($iOrdre)
                           ->save();
                    $this->getUser()->setFlash('notice', 'Association créée');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
            $this->redirect($request->getReferer());
        }
    }
    
    public function executeAjouto(sfWebRequest $request) {
        $iOffreId = $request->getPostParameter('offre_id', null);
        if (!$iOffreId) {
            $this->getUser()->setFlash('error', 'Aucune offre définie');
            $this->redirect('@offre');
        }
        else {
            $iBlocPushId = $request->getPostParameter('bloc_push_id', null);
            if (!$iBlocPushId) {
                $this->getUser()->setFlash('error', 'Aucun bloc push défini');
            }
            else {
                $oMaxAssoc = Doctrine::getTable('AssocOffreBlocPush')
                                     ->createQuery('a')
                                     ->where('a.offre_id = ?', $iOffreId)
                                     ->orderBy('a.ordre DESC')
                                     ->fetchOne();
                
                $iOrdre = ($oMaxAssoc)?($oMaxAssoc->getOrdre() + 1):1;
                
                try {
                    $oAssoc = new AssocOffreBlocPush();
                    $oAssoc->setOffreId($iOffreId)
                           ->setBlocPushId($iBlocPushId)
                           ->setOrdre($iOrdre)
                           ->save();
                    
                    $this->getUser()->setFlash('notice', 'Association créée');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
            $this->redirect($request->getReferer());
        }
    }
    
    public function executeAjouta(sfWebRequest $request) {
        $iArticleId = $request->getPostParameter('article_id', null);
        if (!$iArticleId) {
            $this->getUser()->setFlash('error', 'Aucune offre définie');
            $this->redirect('@article');
        }
        else {
            $iBlocPushId = $request->getPostParameter('bloc_push_id', null);
            if (!$iBlocPushId) {
                $this->getUser()->setFlash('error', 'Aucun bloc push défini');
            }
            else {
                $oMaxAssoc = Doctrine::getTable("AssocArticleBlocPush")
                                     ->createQuery('a')
                                     ->where('a.article_id = ?', $iArticleId)
                                     ->orderBy('a.ordre DESC')
                                     ->fetchOne();
                
                $iOrdre = ($oMaxAssoc)?($oMaxAssoc->getOrdre() + 1):1;
                
                try {
                    $oAssoc = new AssocArticleBlocPush();
                    $oAssoc->setArticleId($iArticleId)
                           ->setBlocPushId($iBlocPushId)
                           ->setOrdre($iOrdre)
                           ->save();
                    
                    $this->getUser()->setFlash('notice', 'Association créée');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
            $this->redirect($request->getReferer());
        }
    }
    
    public function executeAjoutst(sfWebRequest $request) {
        $sStatiqueId = $request->getPostParameter('statique_id', null);
        if (!$sStatiqueId) {
            $this->getUser()->setFlash('error', 'Aucune page définie');
        }
        else {
            $iBlocPushId = $request->getPostParameter('bloc_push_id', null);
            if (!$iBlocPushId) {
                $this->getUser()->setFlash('error', 'Aucun bloc push défini');
            }
            else {
                $oMaxAssoc = Doctrine::getTable('AssocStatiqueBlocPush')
                                     ->createQuery('a')
                                     ->where('a.statique_id = ?', $sStatiqueId)
                                     ->orderBy('a.ordre DESC')
                                     ->fetchOne();
                
                $iOrdre = ($oMaxAssoc)?($oMaxAssoc->getOrdre() + 1):1;
                
                try {
                    $oAssoc = new AssocStatiqueBlocPush();
                    $oAssoc->setStatiqueId($sStatiqueId)
                           ->setBlocPushId($iBlocPushId)
                           ->setOrdre($iOrdre)
                           ->save();
                    
                    $this->getUser()->setFlash('notice', 'Association créée');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->redirect($request->getReferer());
    }
    
    /***********************************\
    * Modification de l'ordre des blocs *
    \***********************************/
    
    public function executeOrdrefp(sfWebRequest $request) {
        $iFicheProduitId = $request->getParameter('fiche_produit_id');
        $iBlocPushId     = $request->getParameter('bloc_push_id');
        $sSens           = $request->getParameter('sens');
        
        $oAssoc = Doctrine::getTable('AssocFicheProduitBlocPush')
                          ->createQuery('a')
                          ->where('a.fiche_produit_id = ?', $iFicheProduitId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            $iAnciennePosition = $oAssoc->getOrdre();
            switch($sSens) {
                case 'bas':
                    $iNouvellePosition = $iAnciennePosition + 1;
                    break;
                case 'haut':
                    $iNouvellePosition = $iAnciennePosition - 1;
                    break;
            }
            $oAssocEchange = Doctrine::getTable('AssocFicheProduitBlocPush')
                                     ->createQuery('ae')
                                     ->where('ae.fiche_produit_id = ?', $iFicheProduitId)
                                     ->andWhere('ae.ordre = ?', $iNouvellePosition)
                                     ->fetchOne();
            if (!$oAssocEchange) {
                $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
            }
            else {
                try {
                    $oAssoc->setOrdre($iNouvellePosition)->save();
                    $oAssocEchange->setOrdre($iAnciennePosition)->save();
                    $this->getUser()->setFlash('notice', 'Déplacement effectué');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeOrdreo(sfWebRequest $request) {
        $iOffreId    = $request->getParameter('offre_id');
        $iBlocPushId = $request->getParameter('bloc_push_id');
        $sSens       = $request->getParameter('sens');
        
        $oAssoc = Doctrine::getTable('AssocOffreBlocPush')
                          ->createQuery('a')
                          ->where('a.offre_id = ?', $iOffreId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            $iAnciennePosition = $oAssoc->getOrdre();
            switch($sSens) {
                case 'bas':
                    $iNouvellePosition = $iAnciennePosition + 1;
                    break;
                case 'haut':
                    $iNouvellePosition = $iAnciennePosition - 1;
                    break;
            }
            $oAssocEchange = Doctrine::getTable('AssocOffreBlocPush')
                                     ->createQuery('ae')
                                     ->where('ae.offre_id = ?', $iOffreId)
                                     ->andWhere('ae.ordre = ?', $iNouvellePosition)
                                     ->fetchOne();
            if (!$oAssocEchange) {
                $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
            }
            else {
                try {
                    $oAssoc->setOrdre($iNouvellePosition)->save();
                    $oAssocEchange->setOrdre($iAnciennePosition)->save();
                    $this->getUser()->setFlash('notice', 'Déplacement effectué');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeOrdrea(sfWebRequest $request) {
        $iArticleId  = $request->getParameter('article_id');
        $iBlocPushId = $request->getParameter('bloc_push_id');
        $sSens       = $request->getParameter('sens');
        
        $oAssoc = Doctrine::getTable('AssocArticleBlocPush')
                          ->createQuery('a')
                          ->where('a.article_id = ?', $iArticleId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            $iAnciennePosition = $oAssoc->getOrdre();
            switch($sSens) {
                case 'bas':
                    $iNouvellePosition = $iAnciennePosition + 1;
                    break;
                case 'haut':
                    $iNouvellePosition = $iAnciennePosition - 1;
                    break;
            }
            $oAssocEchange = Doctrine::getTable('AssocArticleBlocPush')
                                     ->createQuery('ae')
                                     ->where('ae.article_id = ?', $iArticleId)
                                     ->andWhere('ae.ordre = ?', $iNouvellePosition)
                                     ->fetchOne();
            if (!$oAssocEchange) {
                $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
            }
            else {
                try {
                    $oAssoc->setOrdre($iNouvellePosition)->save();
                    $oAssocEchange->setOrdre($iAnciennePosition)->save();
                    $this->getUser()->setFlash('notice', 'Déplacement effectué');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeOrdrepst(sfWebRequest $request) {
        $iPageStatiqueId = $request->getParameter('page_statique_id');
        $iBlocPushId     = $request->getParameter('bloc_push_id');
        $sSens           = $request->getParameter('sens');
        
        $oAssoc = Doctrine::getTable('AssocPageStatiqueBlocPush')
                          ->createQuery('a')
                          ->where('a.page_statique_id = ?', $iPageStatiqueId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            $iAnciennePosition = $oAssoc->getOrdre();
            switch($sSens) {
                case 'bas':
                    $iNouvellePosition = $iAnciennePosition + 1;
                    break;
                case 'haut':
                    $iNouvellePosition = $iAnciennePosition - 1;
                    break;
            }
            $oAssocEchange = Doctrine::getTable('AssocPageStatiqueBlocPush')
                                     ->createQuery('ae')
                                     ->where('ae.page_statique_id = ?', $iPageStatiqueId)
                                     ->andWhere('ae.ordre = ?', $iNouvellePosition)
                                     ->fetchOne();
            if (!$oAssocEchange) {
                $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
            }
            else {
                try {
                    $oAssoc->setOrdre($iNouvellePosition)->save();
                    $oAssocEchange->setOrdre($iAnciennePosition)->save();
                    $this->getUser()->setFlash('notice', 'Déplacement effectué');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeOrdrest(sfWebRequest $request) {
        $sStatiqueId = $request->getParameter('statique_id');
        $iBlocPushId = $request->getParameter('bloc_push_id');
        $sSens       = $request->getParameter('sens');

        $oAssoc = Doctrine::getTable('AssocStatiqueBlocPush')
                          ->createQuery('a')
                          ->where('a.statique_id = ?', $sStatiqueId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            $iAnciennePosition = $oAssoc->getOrdre();
            $iNouvellePosition = null;
            switch($sSens) {
                case 'bas':
                    $iNouvellePosition = $iAnciennePosition + 1;
                    break;
                case 'haut':
                    $iNouvellePosition = $iAnciennePosition - 1;
                    break;
            }
            $oAssocEchange = Doctrine::getTable('AssocStatiqueBlocPush')
                                     ->createQuery('ae')
                                     ->where('ae.statique_id = ?', $sStatiqueId)
                                     ->andWhere('ae.ordre = ?', $iNouvellePosition)
                                     ->fetchOne();
            
            if (!$oAssocEchange) {
                $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
            }
            else {
                try {
                    $oAssoc->setOrdre($iNouvellePosition)->save();
                    $oAssocEchange->setOrdre($iAnciennePosition)->save();
                    $this->getUser()->setFlash('notice', 'Déplacement effectué');
                } catch (Exception $e) {
                    $this->getUser()->setFlash('error', $e->getMessage());
                }
            }
        }
        $this->redirect($request->getReferer());
    }
    
    /****************************\
    * Suppression d'associations *
    \****************************/
    
    public function executeSupprfp(sfWebRequest $request) {
        $iFicheProduitId = $request->getParameter('fiche_produit_id');
        $iBlocPushId     = $request->getParameter('bloc_push_id');
        
        $oAssoc = Doctrine::getTable('AssocFicheProduitBlocPush')
                          ->createQuery('a')
                          ->where('a.fiche_produit_id = ?', $iFicheProduitId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            try {
                $iPosition = $oAssoc->getOrdre();
                Doctrine::getTable('AssocFicheProduitBlocPush')
                        ->createQuery('au')
                        ->update()
                        ->set('au.ordre', 'au.ordre - 1')
                        ->where('au.ordre > ?', $iPosition)
                        ->andWhere('au.fiche_produit_id = ?', $iFicheProduitId)
                        ->execute();
                $oAssoc->delete();
                $this->getUser()->setFlash('notice', 'Association supprimée');
            } catch (Exception $e) {
                $this->getUser()->setFlash('error', $e->getMessage());
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeSuppro(sfWebRequest $request) {
        $iOffreId    = $request->getParameter('offre_id');
        $iBlocPushId = $request->getParameter('bloc_push_id');
        
        $oAssoc = Doctrine::getTable('AssocOffreBlocPush')
                          ->createQuery('a')
                          ->where('a.offre_id = ?', $iOffreId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            try {
                $iPosition = $oAssoc->getOrdre();
                Doctrine::getTable('AssocOffreBlocPush')
                        ->createQuery('au')
                        ->update()
                        ->set('au.ordre', 'au.ordre - 1')
                        ->where('au.ordre > ?', $iPosition)
                        ->andWhere('au.offre_id = ?', $iOffreId)
                        ->execute();
                $oAssoc->delete();
                $this->getUser()->setFlash('notice', 'Association supprimée');
            } catch (Exception $e) {
                $this->getUser()->setFlash('error', $e->getMessage());
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeSuppra(sfWebRequest $request) {
        $iArticleId  = $request->getParameter('article_id');
        $iBlocPushId = $request->getParameter('bloc_push_id');
        
        $oAssoc = Doctrine::getTable('AssocArticleBlocPush')
                          ->createQuery('a')
                          ->where('a.article_id = ?', $iArticleId)
                          ->andWhere('a.bloc_push_id = ?' ,$iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            try {
                $iPosition = $oAssoc->getOrdre();
                Doctrine::getTable('AssocArticleBlocPush')
                        ->createQuery('au')
                        ->update()
                        ->set('au.ordre', 'au.ordre - 1')
                        ->where('au.ordre > ?', $iPosition)
                        ->andWhere('au.article_id = ?', $iArticleId)
                        ->execute();
                $oAssoc->delete();
                $this->getUser()->setFlash('notice', 'Association supprimée');
            } catch (Exception $e) {
                $this->getUser()->setFlash('error', $e->getMessage());
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeSupprpst(sfWebRequest $request) {
        $iPageStatiqueId = $request->getParameter('page_statique_id');
        $iBlocPushId     = $request->getParameter('bloc_push_id');
        
        $oAssoc = Doctrine::getTable('AssocPageStatiqueBlocPush')
                          ->createQuery('a')
                          ->where('a.page_statique_id = ?', $iPageStatiqueId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            try {
                $iPosition = $oAssoc->getOrdre();
                Doctrine::getTable('AssocPageStatiqueBlocPush')
                        ->createQuery('au')
                        ->update()
                        ->set('au.ordre', 'au.ordre - 1')
                        ->where('au.ordre > ?', $iPosition)
                        ->andWhere('au.page_statique_id = ?', $iPageStatiqueId)
                        ->execute();
                $oAssoc->delete();
                $this->getUser()->setFlash('notice', 'Association supprimée');
            } catch (Exception $e) {
                $this->getUser()->setFlash('error', $e->getMessage());
            }
        }
        $this->redirect($request->getReferer());
    }
    
    public function executeSupprst(sfWebRequest $request) {
        $sStatiqueId = $request->getParameter('statique_id');
        $iBlocPushId = $request->getParameter('bloc_push_id');
        
        $oAssoc = Doctrine::getTable('AssocStatiqueBlocPush')
                          ->createQuery('a')
                          ->where('a.statique_id = ?', $sStatiqueId)
                          ->andWhere('a.bloc_push_id = ?', $iBlocPushId)
                          ->fetchOne();
        
        if (!$oAssoc) {
            $this->getUser()->setFlash('error', 'Association inexistante');
        }
        else {
            try {
                $iPosition = $oAssoc->getOrdre();
                Doctrine::getTable('AssocStatiqueBlocPush')
                        ->createQuery('au')
                        ->update()
                        ->set('au.ordre', 'au.ordre - 1')
                        ->where('au.ordre > ?', $iPosition)
                        ->andWhere('au.statique_id = ?', $sStatiqueId)
                        ->execute();
                $oAssoc->delete();
                $this->getUser()->setFlash('notice', 'Association supprimée');
            } catch (Exception $e) {
                $this->getUser()->setFlash('error', $e->getMessage());
            }
        }
        $this->redirect($request->getReferer());
    }
}
