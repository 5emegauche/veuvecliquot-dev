<?php

/**
 * importtrad actions.
 *
 * @package    veuveclicquot
 * @subpackage importtrad
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class importtradActions extends sfActions {
    public function executeIndex(sfWebRequest $request) {
        $sRepertoire = getcwd();
        chdir(sfConfig::get('sf_root_dir'));
        $oTask = new veuveLoadtraductionTask($this->dispatcher, new sfFormatter());
        $fDebut = microtime(true);
        ob_start();
        $oTask->run();
        ob_end_clean();
        $fFin = microtime(true);
        
        chdir($sRepertoire);
        
        $this->fTempsPasse = $fFin - $fDebut;
        return sfView::NONE;
    }
    
    public function executeMigrate(sfWebRequest $request) {
        $sRepertoire = getcwd();
        chdir(sfConfig::get('sf_root_dir'));
        $oTask = new sfDoctrineMigrateTask($this->dispatcher, new sfFormatter());
        $fDebut = microtime(true);
        ob_start();
        $oTask->run();
        ob_end_clean();
        $fFin = microtime(true);
        
        chdir($sRepertoire);
        
        $this->fTempsPasse = $fFin - $fDebut;
        return sfView::NONE;
    }
    
    public function executeCacheclear(sfWebRequest $request) {
        $sRepertoire = getcwd();
        chdir(sfConfig::get('sf_root_dir'));
        $oTask = new sfCacheClearTask($this->dispatcher, new sfFormatter());
        $fDebut = microtime(true);
        ob_start();
        $oTask->run();
        ob_end_clean();
        $fFin = microtime(true);
        
        chdir($sRepertoire);
        
        $this->fTempsPasse = $fFin - $fDebut;
        return sfView::NONE;
    }
    
    public function executeImportnews(sfWebRequest $request) {
        $sRepertoire = getcwd();
        chdir(sfConfig::get('sf_root_dir'));
        $oTask = new veuveImportnewsTask($this->dispatcher, new sfFormatter());
        $fDebut = microtime(true);
        ob_start();
        $oTask->run();
        ob_end_clean();
        $fFin = microtime(true);
        
        chdir($sRepertoire);
        
        $this->fTempsPasse = $fFin - $fDebut;
        return sfView::NONE;
    }
    
    public function executeInitordres(sfWebRequest $request) {
        $sRepertoire = getcwd();
        chdir(sfConfig::get('sf_root_dir'));
        $oTask = new veuveInitordresTask($this->dispatcher, new sfFormatter());
        $fDebut = microtime(true);
        ob_start();
        $oTask->run();
        ob_end_clean();
        $fFin = microtime(true);
        
        chdir($sRepertoire);
        
        $this->fTempsPasse = $fFin - $fDebut;
        return sfView::NONE;
    }
    
    public function executeInitordresnotes(sfWebRequest $request) {
        $sRepertoire = getcwd();
        chdir(sfConfig::get('sf_root_dir'));
        $oTask = new veuveInitordresnotesTask($this->dispatcher, new sfFormatter());
        $fDebut = microtime(true);
        ob_start();
        $oTask->run();
        ob_end_clean();
        $fFin = microtime(true);
        
        chdir($sRepertoire);
        
        $this->fTempsPasse = $fFin - $fDebut;
        return sfView::NONE;
    }
}
