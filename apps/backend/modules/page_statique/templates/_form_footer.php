<?php if (!$page_statique->isNew()): ?>
<h2>Blocs Push</h2>
<?php $aBlocsPushTries = AssocPageStatiqueBlocPush::getBlocsTries($page_statique->id); ?>
<?php $iNbBlocsPushTries = count($aBlocsPushTries); ?>
<?php if ($iNbBlocsPushTries > 0): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Description</th>
      <th colspan="3">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aBlocsPushTries as $index => $oBlocPushTrie): ?>
    <?php $oBlocPushTrie instanceof AssocFicheProduitBlocPush; ?>
    <tr>
      <td><?php echo $oBlocPushTrie->getBlocPushId(); ?></td>
      <td><?php echo $oBlocPushTrie->getBlocPush()->getDescription(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsPushTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_pst?page_statique_id=' . $page_statique->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=bas'); ?>"><icon class="icon-arrow-down" /></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_pst?page_statique_id=' . $page_statique->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=haut'); ?>"><icon class="icon-arrow-up" /></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td><a href="<?php echo url_for('@suppr_assoc_bp_pst?page_statique_id=' . $page_statique->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId()); ?>" class="btn btn-mini btn-danger">Supprimer</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<form action="<?php echo url_for('@ajout_assoc_bp_pst'); ?>" method="post">
  <input type="hidden" name="page_statique_id" value="<?php echo $page_statique->getId(); ?>" />
  <?php $aBlocsPush = AssocPageStatiqueBlocPush::getBlocsNotIn($page_statique->getId()); ?>
  <select name="bloc_push_id">
    <option value="">- Aucun -</option>
    <?php foreach($aBlocsPush as $oBlocPush): ?>
    <option value="<?php echo $oBlocPush->getId(); ?>"><?php echo $oBlocPush->getDescription(); ?></option>
    <?php endforeach; ?>
  </select>&nbsp;&nbsp;
  <input type="submit" name="act" value="Ajouter" />
</form>
<?php endif; ?>