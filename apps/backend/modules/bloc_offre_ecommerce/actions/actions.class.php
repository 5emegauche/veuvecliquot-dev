<?php

require_once dirname(__FILE__).'/../lib/bloc_offre_ecommerceGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/bloc_offre_ecommerceGeneratorHelper.class.php';

/**
 * bloc_offre_ecommerce actions.
 *
 * @package    veuveclicquot
 * @subpackage bloc_offre_ecommerce
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bloc_offre_ecommerceActions extends autoBloc_offre_ecommerceActions {
    public function executeListing(sfWebRequest $request) {
        $boe_id = $request->getPostParameter('boe_id', null);
        if ($boe_id) {
            Doctrine::getTable('BlocOffreEcommerce')
                    ->createQuery('b')
                    ->delete()
                    ->andWhere('b.id = ?', $boe_id)
                    ->execute();
        }

        $this->blocs = Doctrine::getTable('BlocOffreEcommerce')
                               ->createQuery('boe')
                               ->select('boe.id')
                               ->addSelect('p.nom nom_pays')
                               ->addSelect('l.nom nom_langue')
                               ->leftJoin('boe.Pays p')
                               ->leftJoin('boe.Langue l')
                               ->where('boe.bloc_offre_id = ?', $request->getParameter('id'))
                               ->execute();
        $this->id = $request->getParameter('id');
        $this->blocoffre = Doctrine::getTable('BlocOffre')->find($this->id);
    }

    public function executeCreer(sfWebRequest $request) {
        $this->form = new BackendBlocOffreEcommerceForm();
        $this->id = $request->getParameter('id');
        $this->_processCreerForm($request, $this->form, $this->id);
    }

    private function _processCreerForm(sfWebRequest $request, BackendBlocOffreEcommerceForm $form, $id) {
        if ($form->isSubmitted($request)) {
            $form->bindParameters($request);
            if ($form->isValid()) {
                $aPays = $form->getValue('pays_id', array(null));
                $aLangues = $form->getValue('langue_id', array(null));
                foreach($aPays as $iPays) {
                    foreach($aLangues as $iLangue) {
                        $oBloc = new BlocOffreEcommerce();
                        $oBloc->setBlocOffreId($id)
                              ->setPaysId($iPays)
                              ->setLangueId($iLangue)
                              ->setAutorise($form->getValue('autorise'))
                              ->setLien($form->getValue('lien'))
                              ->save();
                    }
                }
                $this->redirect('@bloc_offre_ecommerce_listing?id=' . $id);
            }
        }
    }
}
