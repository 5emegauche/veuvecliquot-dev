<div id="sf_admin_container">
  <h1>Création de règles E-Commerce</h1>
  <div id="sf_admin_header"></div>
  <div id="sf_admin_content">
    <div class="sf_admin_form">
      <form method="post" action="" enctype="multipart/form-data">
        <?php echo $form->renderHiddenFields(); ?>
        <fieldset id="sf_fieldset_infos">
          <h2>Infos</h2>
          <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_pays_id">
            <div>
              <?php echo $form['pays_id']->renderLabel('Pays'); ?>
              <div class="content"><?php echo $form['pays_id']->render(array('size' => 10)); ?></div>
            </div>
          </div>
          <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_langue_id">
            <div>
              <?php echo $form['langue_id']->renderLabel('Langues'); ?>
              <div class="content"><?php echo $form['langue_id']->render(); ?></div>
            </div>
          </div>
          <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_autorise">
            <div>
              <?php echo $form['autorise']->renderLabel('Autorisé ?'); ?>
              <div class="content"><?php echo $form['autorise']->render(); ?></div>
            </div>
          </div>
          <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_lien">
            <div>
              <?php echo $form['lien']->renderLabel('Lien'); ?>
              <div class="content"><?php echo $form['lien']->render(); ?></div>
            </div>
          </div>
        </fieldset>
        <ul class="sf_admin_actions">
          <li><a class="btn" href="<?php echo url_for('@bloc_offre_ecommerce_listing?id=' . $id); ?>">Back to list</a></li>
          <li><input type="submit" value="Save" class="btn btn-primary"></li>
        </ul>
      </form>
    </div>
  </div>
</div>