<?php if (!$fiche_produit->isNew()): ?>
<h2 id="blocs">Blocs</h2>
<a href="<?php echo url_for('bloc_fiche_produit/new?fiche_produit_id=' . $fiche_produit->getId()); ?>" class="btn btn-success btn-small">Ajouter un bloc</a>
<?php $aBlocsTries = $fiche_produit->getBlocsTries(); ?>
<?php if (count($aBlocsTries)): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Description bloc</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $iNbBlocsTries = count($aBlocsTries); ?>
    <?php foreach($aBlocsTries as $index => $oBlocProduit): ?>
    <tr>
      <td><?php echo $oBlocProduit->getId(); ?></td>
      <td><?php echo $oBlocProduit->getDescription(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_bloc_fiche_produit?fiche_produit_id=' . $fiche_produit->id . '&bloc_id=' . $oBlocProduit->id . '&sens=bas'); ?>"><i class="icon-arrow-down">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_bloc_fiche_produit?fiche_produit_id=' . $fiche_produit->id . '&bloc_id=' . $oBlocProduit->id . '&sens=haut'); ?>"><i class="icon-arrow-up">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php echo link_to('Éditer', '@bloc_fiche_produit_edit?id=' . $oBlocProduit->getId(), array('class' => 'btn btn-primary btn-mini')); ?>
      </td>
      <td>
        <?php echo link_to('Supprimer', '@bloc_fiche_produit_delete?id=' . $oBlocProduit->getId(), array('class' => 'btn btn-danger btn-mini', 'method' => 'delete', 'confirm' => 'Êtes vous sûr ?')); ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<h2>Notes</h2>
<a href="<?php echo url_for('note_fiche_produit/new?fiche_produit_id=' . $fiche_produit->getId()); ?>" class="btn btn-success btn-small">Ajouter une note</a>
<?php $aNotesTriees = $fiche_produit->getNotesTriees(); ?>
<?php if (count($aNotesTriees)): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Titre</th>
      <th>Note</th>
      <th colspan="4">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php $iNbNotesTriees = count($aNotesTriees); ?>
    <?php foreach($aNotesTriees as $index => $oNoteProduit): ?>
    <tr>
      <td><?php echo $oNoteProduit->getId(); ?></td>
      <td><?php echo $oNoteProduit->getTitre(); ?></td>
      <td><?php echo $oNoteProduit->getNoteVin(); ?></td>
      <td>
        <?php if ($index < ($iNbNotesTriees - 1)): ?>
        <a href="<?php echo url_for('@ordre_note_fiche_produit?fiche_produit_id=' . $fiche_produit->getId() . '&note_id=' . $oNoteProduit->getId() . '&sens=bas'); ?>"><i class="icon-arrow-down">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_note_fiche_produit?fiche_produit_id=' . $fiche_produit->getId() . '&note_id=' . $oNoteProduit->getId() . '&sens=haut'); ?>"><i class="icon-arrow-up">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php echo link_to('Éditer', '@note_fiche_produit_edit?id=' . $oNoteProduit->getId(), array('class' => 'btn btn-primary btn-mini')); ?>
      </td>
      <td>
        <?php echo link_to('Supprimer', '@note_fiche_produit_delete?id=' . $oNoteProduit->getId(), array('class' => 'btn btn-danger btn-mini', 'method' => 'delete', 'confirm' => 'Êtes vous sûr ?')); ?>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<h2>Blocs Push</h2>
<?php $aBlocsPushTries = AssocFicheProduitBlocPush::getBlocsTries($fiche_produit->id); ?>
<?php $iNbBlocsPushTries = count($aBlocsPushTries); ?>
<?php if ($iNbBlocsPushTries > 0): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Description</th>
      <th colspan="3">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aBlocsPushTries as $index => $oBlocPushTrie): ?>
    <?php $oBlocPushTrie instanceof AssocFicheProduitBlocPush; ?>
    <tr>
      <td><?php echo $oBlocPushTrie->getBlocPushId(); ?></td>
      <td><?php echo $oBlocPushTrie->getBlocPush()->getDescription(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsPushTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_fp?fiche_produit_id=' . $fiche_produit->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=bas'); ?>"><i class="icon-arrow-down">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_fp?fiche_produit_id=' . $fiche_produit->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=haut'); ?>"><i class="icon-arrow-up">&nbsp;</i></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td><a href="<?php echo url_for('@suppr_assoc_bp_fp?fiche_produit_id=' . $fiche_produit->id . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId()); ?>" class="btn btn-mini btn-danger">Supprimer</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<form action="<?php echo url_for('@ajout_assoc_bp_fp'); ?>" method="post">
  <input type="hidden" name="fiche_produit_id" value="<?php echo $fiche_produit->getId(); ?>" />
  <?php $aBlocsPush = AssocFicheProduitBlocPush::getBlocsNotIn($fiche_produit->getId()); ?>
  <select name="bloc_push_id">
    <option value="">- Aucun -</option>
    <?php foreach($aBlocsPush as $oBlocPush): ?>
    <option value="<?php echo $oBlocPush->getId(); ?>"><?php echo $oBlocPush->getDescription(); ?></option>
    <?php endforeach; ?>
  </select>&nbsp;&nbsp;
  <input type="submit" name="act" value="Ajouter" />
</form>
<?php endif; ?>