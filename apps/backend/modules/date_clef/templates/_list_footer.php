<h2>Blocs Push</h2>
<?php $aBlocsPushTries = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_KEY_DATES); ?>
<?php $iNbBlocsPushTries = count($aBlocsPushTries); ?>
<?php if ($iNbBlocsPushTries > 0): ?>
<table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th>Description</th>
      <th colspan="3">Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aBlocsPushTries as $index => $oBlocPushTrie): ?>
    <?php $oBlocPushTrie instanceof AssocStatiqueBlocPush; ?>
    <tr>
      <td><?php echo $oBlocPushTrie->getBlocPushId(); ?></td>
      <td><?php echo $oBlocPushTrie->getBlocPush()->getDescription(); ?></td>
      <td>
        <?php if ($index < ($iNbBlocsPushTries - 1)): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_st?statique_id=' . VeuveTranslator::ARTICLE_KEY_DATES . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=bas'); ?>"><icon class="icon-arrow-down" /></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td>
        <?php if ($index > 0): ?>
        <a href="<?php echo url_for('@ordre_assoc_bp_st?statique_id=' . VeuveTranslator::ARTICLE_KEY_DATES . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId() . '&sens=haut'); ?>"><icon class="icon-arrow-up" /></a>
        <?php else: ?>
        &nbsp;
        <?php endif; ?>
      </td>
      <td><a href="<?php echo url_for('@suppr_assoc_bp_st?statique_id=' . VeuveTranslator::ARTICLE_KEY_DATES . '&bloc_push_id=' . $oBlocPushTrie->getBlocPushId()); ?>" class="btn btn-mini btn-danger">Supprimer</a></td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<form action="<?php echo url_for('@ajout_assoc_bp_st'); ?>" method="post">
  <input type="hidden" name="statique_id" value="<?php echo VeuveTranslator::ARTICLE_KEY_DATES; ?>" />
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsNotIn(VeuveTranslator::ARTICLE_KEY_DATES); ?>
  <select name="bloc_push_id">
    <option value="">- Aucun -</option>
    <?php foreach($aBlocsPush as $oBlocPush): ?>
    <option value="<?php echo $oBlocPush->getId(); ?>"><?php echo $oBlocPush->getDescription(); ?></option>
    <?php endforeach; ?>
  </select>&nbsp;&nbsp;
  <input type="submit" name="act" value="Ajouter" />
</form>