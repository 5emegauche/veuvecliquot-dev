<ul class="sf_admin_actions">
<?php if ($form->isNew()): ?>
  <?php echo $helper->linkToDelete($form->getObject(), array(  'params' =>   array(  ),  'confirm' => 'Are you sure?',  'class_suffix' => 'delete',  'label' => 'Delete',)) ?>
  <li><a class="btn" href="javascript:history.back();">Back to list</a></li>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
  <?php echo $helper->linkToSaveAndAdd($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save_and_add',  'label' => 'Save and add',)) ?>
<?php else: ?>
  <li><a class="btn btn-inverse" onclick="if (confirm('Are you sure?')) { var f = document.createElement('form'); f.style.display = 'none'; this.parentNode.appendChild(f); f.method = 'post'; f.action = this.href;var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', 'sf_method'); m.setAttribute('value', 'delete'); f.appendChild(m);var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', '_csrf_token'); m.setAttribute('value', 'ba889d567b24af4b407a099cbd6e24ef');var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', 'bfpe_id'); m.setAttribute('value', '<?php echo $form->getObject()->getId(); ?>'); f.appendChild(m);f.submit(); };return false;" href="/backend.php/bloc_fiche_produit_ecommerce/liste/<?php echo $form->getObject()->getBlocFicheProduitId(); ?>">Delete</a></li>
  <li><a class="btn" href="<?php echo url_for('@bloc_fiche_produit_ecommerce_listing?id=' . $bloc_fiche_produit_ecommerce->bloc_fiche_produit_id); ?>">Back to list</a></li>
  <?php echo $helper->linkToSave($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save',  'label' => 'Save',)) ?>
  <?php echo $helper->linkToSaveAndAdd($form->getObject(), array(  'params' =>   array(  ),  'class_suffix' => 'save_and_add',  'label' => 'Save and add',)) ?>
<?php endif; ?>
</ul>