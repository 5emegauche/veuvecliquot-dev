<div id="sf_admin_container">
  <h1>Blocs Ecommerce</h1>
  <div id="sf_admin_header"></div>
  <div id="sf_admin_content">
    <form action="" method="post">
      <div class="sf_admin_list">
        <table cellspacing="0" class="table table-striped">
          <thead>
            <tr>
              <th>Pays</th>
              <th>Langue</th>
              <th>Autorisé</th>
              <th>Lien</th>
              <th id="sf_admin_list_th_actions"></th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th colspan="3"><?php echo count($blocs); ?> résultats</th>
            </tr>
          </tfoot>
          <tbody>
            <?php foreach($blocs as $oBloc): ?>
            <tr class="sf_admin_row odd" id="bloc_fiche_produit_ecommerce-_<?php echo $oBloc->id; ?>">
              <td><?php if($oBloc->pays_id): ?><?php echo $oBloc->Pays; ?><?php endif; ?></td>
              <td><?php if($oBloc->langue_id): ?><?php echo $oBloc->Langue; ?><?php endif; ?></td>
              <td><?php echo $oBloc->autorise; ?></td>
              <td><?php echo $oBloc->lien; ?></td>
              <td>
                <ul class="sf_admin_td_actions">
                  <li><a class="btn" href="/backend.php/bloc_fiche_produit_ecommerce/<?php echo $oBloc->id; ?>/edit">Edit</a></li>
                  <li><a class="btn btn-inverse" onclick="if (confirm('Are you sure?')) { var f = document.createElement('form'); f.style.display = 'none'; this.parentNode.appendChild(f); f.method = 'post'; f.action = this.href;var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', 'sf_method'); m.setAttribute('value', 'delete'); f.appendChild(m);var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', '_csrf_token'); m.setAttribute('value', 'ba889d567b24af4b407a099cbd6e24ef');var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', 'bfpe_id'); m.setAttribute('value', '<?php echo $oBloc->id; ?>'); f.appendChild(m);f.submit(); };return false;" href="<?php echo $_SERVER['SCRIPT_URL']; ?>">Delete</a></li>
                </ul>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <ul class="sf_admin_actions">
        <li><a class="btn btn-primary" href="<?php echo url_for('@bloc_fiche_produit_ecommerce_generation?id=' . $id); ?>">New</a></li>
        <li><a class="btn" href="<?php echo url_for('@fiche_produit'); ?>/<?php echo $blocficheproduit->fiche_produit_id; ?>/edit#blocs">Back to bloc</a></li>
      </ul>
    </form>
  </div>

  <div id="sf_admin_footer"></div>
</div>