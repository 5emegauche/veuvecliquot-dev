<?php

require_once dirname(__FILE__).'/../lib/bloc_fiche_produit_ecommerceGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/bloc_fiche_produit_ecommerceGeneratorHelper.class.php';

/**
 * bloc_fiche_produit_ecommerce actions.
 *
 * @package    veuveclicquot
 * @subpackage bloc_fiche_produit_ecommerce
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bloc_fiche_produit_ecommerceActions extends autoBloc_fiche_produit_ecommerceActions {
    public function executeListing(sfWebRequest $request) {
        $bfpe_id = $request->getPostParameter('bfpe_id', null);
        if ($bfpe_id) {
            Doctrine::getTable('BlocFicheProduitEcommerce')
                    ->createQuery('b')
                    ->delete()
                    ->andWhere('b.id = ?', $bfpe_id)
                    ->execute();
        }

        $this->blocs = Doctrine::getTable('BlocFicheProduitEcommerce')
                               ->createQuery('bfpe')
                               ->select('bfpe.id')
                               ->addSelect('p.nom nom_pays')
                               ->addSelect('l.nom nom_langue')
                               ->leftJoin('bfpe.Pays p')
                               ->leftJoin('bfpe.Langue l')
                               ->where('bfpe.bloc_fiche_produit_id = ?', $request->getParameter('id'))
                               ->execute();
        $this->id = $request->getParameter('id');
        $this->blocficheproduit = Doctrine::getTable('BlocFicheProduit')->find($this->id);
    }

    public function executeCreer(sfWebRequest $request) {
        $this->form = new BackendBlocFicheProduitEcommerceForm();
        $this->id = $request->getParameter('id');
        $this->_processCreerForm($request, $this->form, $this->id);
    }

    private function _processCreerForm(sfWebRequest $request, BackendBlocFicheProduitEcommerceForm $form, $id) {
        if ($form->isSubmitted($request)) {
            $form->bindParameters($request);
            if ($form->isValid()) {
                $aPays = $form->getValue('pays_id', array(null));
                $aLangues = $form->getValue('langue_id', array(null));
                foreach($aPays as $iPays) {
                    foreach($aLangues as $iLangue) {
                        $oBloc = new BlocFicheProduitEcommerce();
                        $oBloc->setBlocFicheProduitId($id)
                              ->setPaysId($iPays)
                              ->setLangueId($iLangue)
                              ->setAutorise($form->getValue('autorise'))
                              ->setLien($form->getValue('lien'))
                              ->save(); //
                    }
                }
                $this->redirect('@bloc_fiche_produit_ecommerce_listing?id=' . $id);
            }
        }
    }
}
