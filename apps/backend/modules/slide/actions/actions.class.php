<?php

require_once dirname(__FILE__).'/../lib/slideGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/slideGeneratorHelper.class.php';

/**
 * slide actions.
 *
 * @package    veuveclicquot
 * @subpackage slide
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class slideActions extends autoSlideActions
{
    public function executeNew(sfWebRequest $request) {
        parent::executeNew($request);
        $this->form->setDefault('bloc_id', $request->getGetParameter('bloc_id'));
    }
}
