<?php if(!$bloc_fiche_produit->isNew()): ?>
<a class="btn btn-primary" href="<?php echo url_for('@bloc_fiche_produit_ecommerce_listing?id=' . $bloc_fiche_produit->id); ?>">Gérer Liens Ecommerce</a>
<?php endif; ?>
<script type="text/javascript">
jQuery(document).ready(function() {
  jQuery('input[type=checkbox]').filter(function() {
    return this.id.match(/bloc_fiche_produit_([a-z]{2})_bouton_shop/);
  }).on('change', function(e) {
    var idCheckbox = jQuery(this).attr('id');
    var idInput = '#' + idCheckbox.replace('bouton_shop', 'lien_bouton_shop');
    if (jQuery(this).is(':checked')) {
        jQuery(idInput).parent().parent().slideDown();
    }
    else {
        jQuery(idInput).parent().parent().slideUp();
    }
  });
});
</script>