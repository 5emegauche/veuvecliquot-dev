<?php

require_once dirname(__FILE__).'/../lib/bloc_fiche_produitGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/bloc_fiche_produitGeneratorHelper.class.php';

/**
 * bloc_fiche_produit actions.
 *
 * @package    veuveclicquot
 * @subpackage bloc_fiche_produit
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bloc_fiche_produitActions extends autoBloc_fiche_produitActions {
    public function executeNew(sfWebRequest $request) {
        parent::executeNew($request);
        $this->form->setDefault('fiche_produit_id', $request->getGetParameter('fiche_produit_id'));
    }

    public function executeOrdre(sfWebRequest $request) {
    	$iFicheProduitId = $request->getParameter('fiche_produit_id');
        $iBlocId         = $request->getParameter('bloc_id');
        $sSens           = $request->getParameter('sens');

        $oBloc = Doctrine::getTable('BlocFicheProduit')->find($iBlocId);

        if (!$oBloc) {
        	$this->getUser()->setFlash('error', 'Bloc inexistant');
        }
        else {
        	$iAnciennePosition = $oBloc->getOrdre();
	        switch($sSens) {
	            case 'bas':
	                $iNouvellePosition = $iAnciennePosition + 1;
	                break;
	            case 'haut':
	                $iNouvellePosition = $iAnciennePosition - 1;
	                break;
	        }
	        $oEchange = Doctrine::getTable('BlocFicheProduit')
	        					->createQuery('b')
	        					->where('b.fiche_produit_id = ?', $iFicheProduitId)
	        					->andWhere('b.ordre = ?', $iNouvellePosition)
	        					->fetchOne();
	        if (!$oEchange) {
	            $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
	        }
	        else {
	            try {
	                $oBloc->setOrdre($iNouvellePosition)->save();
	                $oEchange->setOrdre($iAnciennePosition)->save();
	                $this->getUser()->setFlash('notice', 'Déplacement effectué');
	            } catch (Exception $e) {
	                $this->getUser()->setFlash('error', $e->getMessage());
	            }
	        }
        }
        $this->redirect($request->getReferer());
    }
}
