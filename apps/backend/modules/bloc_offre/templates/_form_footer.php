<?php if (!$bloc_offre->isNew()): ?>
<h2>Textes slider</h2>
<a href="<?php echo url_for('slide/new?bloc_id=' . $bloc_offre->getId()); ?>" class="btn btn-success btn-small">Ajouter un texte</a>
<?php $aSlidesTries = $bloc_offre->getSlider(); ?>
<?php if (count($aSlidesTries)): ?>
<table class="table table-striped">
  <thead>
    <tr>
      <th>#</th>
      <th>Titre</th>
      <th colspan="2">Action</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($aSlidesTries as $oSlide): ?>
    <?php $oSlide instanceof Slide; ?>
    <tr>
      <td><?php echo $oSlide->getId(); ?></td>
      <td><?php echo $oSlide->getTitre(); ?></td>
      <td>
        <?php echo link_to('Éditer', '@slide_edit?id=' . $oSlide->getId(), array('class' => 'btn btn-primary btn-mini')); ?>
      </td>
      <td>
        <?php echo link_to('Supprimer', '@slide_delete?id=' . $oSlide->getId(), array('class' => 'btn btn-danger btn-mini', 'method' => 'delete', 'confirm' => 'Êtes vous sûr ?')); ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>
<a class="btn btn-primary" href="<?php echo url_for('@bloc_offre_ecommerce_listing?id=' . $bloc_offre->id); ?>">Gérer Liens Ecommerce</a>
<?php endif; ?>