<?php

require_once dirname(__FILE__).'/../lib/bloc_offreGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/bloc_offreGeneratorHelper.class.php';

/**
 * bloc_offre actions.
 *
 * @package    veuveclicquot
 * @subpackage bloc_offre
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bloc_offreActions extends autoBloc_offreActions {
    public function executeNew(sfWebRequest $request) {
        parent::executeNew($request);
        $this->form->setDefault('offre_id', $request->getGetParameter('offre_id'));
    }

    public function executeOrdre(sfWebRequest $request) {
    	$iOffreId = $request->getParameter('offre_id');
        $iBlocId  = $request->getParameter('bloc_id');
        $sSens    = $request->getParameter('sens');

        $oBloc = Doctrine::getTable('Bloc')->find($iBlocId);

        if (!$oBloc) {
        	$this->getUser()->setFlash('error', 'Bloc inexistant');
        }
        else {
        	$iAnciennePosition = $oBloc->getOrdre();
	        switch($sSens) {
	            case 'bas':
	                $iNouvellePosition = $iAnciennePosition + 1;
	                break;
	            case 'haut':
	                $iNouvellePosition = $iAnciennePosition - 1;
	                break;
	        }
	        $oEchange = Doctrine::getTable('Bloc')
	        					->createQuery('b')
	        					->where('b.offre_id = ?', $iOffreId)
	        					->andWhere('b.ordre = ?', $iNouvellePosition)
	        					->fetchOne();
	        if (!$oEchange) {
	            $this->getUser()->setFlash('error', 'Erreur lors du déplacement');
	        }
	        else {
	            try {
	                $oBloc->setOrdre($iNouvellePosition)->save();
	                $oEchange->setOrdre($iAnciennePosition)->save();
	                $this->getUser()->setFlash('notice', 'Déplacement effectué');
	            } catch (Exception $e) {
	                $this->getUser()->setFlash('error', $e->getMessage());
	            }
	        }
        }
        $this->redirect($request->getReferer());
    }
}
