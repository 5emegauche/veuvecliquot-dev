<?php $pays instanceof Pays; ?>
<footer class="main-footer">
  <div class="clearfix">
    <ul class="social">
      <?php if ($pays->getAfficheFacebook()): ?>
      <li><a href="<?php echo ($pays->getFacebook())?$pays->getFacebook():sfConfig::get('app_social_facebook'); ?>" target="_blank" class="network facebook" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Official Page', socialNetwork: 'Facebook' });">facebook</a></li>
      <?php endif; ?>
      <?php if ($pays->getAfficheTwitter()): ?>
      <li><a href="<?php echo ($pays->getTwitter())?$pays->getTwitter():sfConfig::get('app_social_twitter'); ?>" target="_blank" class="network twitter" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Official Page', socialNetwork: 'Twitter' });">twitter</a></li>
      <?php endif; ?>
      <?php if ($pays->getAfficheYoutube()): ?>
      <li><a href="<?php echo ($pays->getYoutube())?$pays->getYoutube():sfConfig::get('app_social_youtube'); ?>" target="_blank" class="network youtube" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Official Page', socialNetwork: 'Youtube' });">youtube</a></li>
      <?php endif; ?>
      <!--
      <li><a href="<?php echo ($pays->getPinterest())?$pays->getPinterest():sfConfig::get('app_social_pinterest'); ?>" target="_blank" class="network pinterest">pinterest</a></li>
      -->
    </ul>
    <a href="#" class="top">top</a>
  </div>
  <hr >
  <div class="clearfix">
    <ul class="links">
      <?php foreach($Racine->getLiensTries(0, $liensinterdits) as $oLien): ?>
      <?php include_partial('common/lien', array('oItem' => $oLien, 'liens' => $liens, 'liensinterdits' => $liensinterdits)); ?>
      <?php endforeach; ?>
    </ul>
    <ul class="links tablet">
      <?php foreach($Racine->getLiensTries(1, $liensinterdits) as $oLien): ?>
      <?php include_partial('common/lien', array('oItem' => $oLien, 'liens' => $liens, 'liensinterdits' => $liensinterdits)); ?>
      <?php endforeach; ?>
    </ul>
  </div>
  <a href="<?php echo url_for('@homepage'); ?>" class="back-home">home</a>
</footer>