<?php $pays = Doctrine::getTable('Pays')->findOneBy('slug', $sf_user->getAttribute('pays')); ?>
<?php if (($pays->getAfficheFacebook()) || ($pays->getAfficheTwitter()) || ($pays->getAffichePinterest()) || (isset($forcefb) && $forcefb == true) || (isset($forcetw) && $forcetw == true)): ?>
<?php $sCurrentUrl = $_SERVER['SCRIPT_URI']; ?>
<?php $sImage = str_replace($_SERVER['SCRIPT_URL'], $image, $_SERVER['SCRIPT_URI']); ?>
<?php if (((isset($fullbloc)) && ($fullbloc="yes")) || (!isset($fullbloc))): ?>
<div class="share-bloc">
  <hr>
<?php endif; ?>
  <div class="share">
    <span><?php echo __('Share'); ?></span>
    <ul>
      <?php if ($pays->getAfficheFacebook() || (isset($forcefb) && $forcefb == true)): ?>
      <li><a id="facebook-share-button" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $sCurrentUrl; ?>" class="network facebook" target="_blank" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Share', socialNetwork: 'Facebook' });">facebook</a></li>
      <?php endif; ?>
      <?php if ($pays->getAfficheTwitter() || (isset($forcetw) && $forcetw == true)): ?>
      <li><a id="twitter-share-button" href="https://twitter.com/share" class="network twitter" target="_blank" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Share', socialNetwork: 'Twitter' });">twitter</a></li>
      <?php endif; ?>
      <?php if ($pays->getAffichePinterest()): ?>
      <li><a id="pinterest-share-button" href="#" onclick="selectPinImage();return false;" data-pin-do="buttonBookmark" class="network pinterest" target="_blank" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Share', socialNetwork: 'Pinterest' });">pinterest</a></li>
      <?php endif; ?>
    </ul>
  </div>
<?php if (((isset($fullbloc)) && ($fullbloc="yes")) || (!isset($fullbloc))): ?>
</div>
<?php endif; ?>

<?php endif; ?>
