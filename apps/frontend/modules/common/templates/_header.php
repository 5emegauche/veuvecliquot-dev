<?php $pays instanceof Pays; ?>
<header class="main-header">
  <a href="#" class="menu">Menu</a>
  <div class="logo"><a href="<?php echo url_for('@homepage'); ?>">Veuve Clicquot</a></div>
  <ul class="social tablet">
    <?php if ($pays->getAfficheFacebook()): ?>
    <li><a href="<?php echo ($pays->getFacebook())?$pays->getFacebook():sfConfig::get('app_social_facebook'); ?>" target="_blank" class="network facebook" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Official Page', socialNetwork: 'Facebook' });">facebook</a></li>
    <?php endif; ?>
    <?php if ($pays->getAfficheTwitter()): ?>
    <li><a href="<?php echo ($pays->getTwitter())?$pays->getTwitter():sfConfig::get('app_social_twitter'); ?>" target="_blank" class="network twitter" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Official Page', socialNetwork: 'Twitter' });">twitter</a></li>
    <?php endif; ?>
    <?php if ($pays->getAfficheYoutube()): ?>
    <li><a href="<?php echo ($pays->getYoutube())?$pays->getYoutube():sfConfig::get('app_social_youtube'); ?>" target="_blank" class="network youtube" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'social', socialAction: 'Official Page', socialNetwork: 'Youtube' });">youtube</a></li>
    <?php endif; ?>
    <!-- 
    <li><a href="<?php echo ($pays->getPinterest())?$pays->getPinterest():sfConfig::get('app_social_pinterest'); ?>" target="_blank" class="network pinterest">pinterest</a></li>
    -->
  </ul>
</header>
<nav class="main-nav">
  <ul class="main-ul">
    <?php foreach($Racine->getLiensTries(null, $liensinterdits) as $oLien): ?>
      <?php if ($oLien->getTypeLien() == Lien::TYPE_LIEN_MENU): ?>
      <?php include_partial('common/menu', array('oItem' => $oLien, 'liens' => $liens, 'liensinterdits' => $liensinterdits)); ?>
      <?php else: ?>
      <?php include_partial('common/lien', array('oItem' => $oLien, 'liens' => $liens, 'liensinterdits' => $liensinterdits)); ?>
      <?php endif; ?>
    <?php endforeach; ?>
    <?php if ($pays->getEcommerce()): ?>
    <li><a target="_blank" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, event: 'click', pageName: 'Ecommerce Link in Menu');" href="<?php echo $pays->getLienEcommerce(); ?>"><?php echo __('Shop now'); ?></a></li>
    <?php endif; ?>
    <li class="phone footLink"></li>
    <?php foreach($RacineF->getLiensTries(1, $liensinterdits) as $oLien): ?>
    <?php $oLien instanceof Lien; ?>
    <li class="phone footLink"><a href="<?php echo ($oLien->getTypeLien() == Lien::TYPE_LIEN_EXTERNE)?$oLien->getLienExterne():$liens[$oLien->getLienInterne()]; ?>"><?php echo $oLien->getLibelle(); ?></a></li>
    <?php endforeach; ?>
  </ul>
</nav>