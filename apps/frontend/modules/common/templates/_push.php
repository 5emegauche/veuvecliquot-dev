<ul class="articles-list clearfix">
  <?php foreach($blocs as $oBlocPush): ?>
  <?php $oBlocPush instanceof BlocPush; ?>
  <li>
    <article class="push">
      <figure>
        <img src="<?php echo $oBlocPush->getUrlImage(); ?>" alt="push" />
      </figure>
      <h3><?php echo $oBlocPush->getTitre(); ?></h3>
      <hr />
      <a href="<?php echo $oBlocPush->getLien(); ?>" class="link"><?php echo $oBlocPush->getTexteLien(); ?></a>
    </article>
  </li>
  <?php endforeach; ?>
</ul>