<?php $oItem instanceof Lien; ?>
<li>
  <a href="#" class="span"><?php echo $oItem->getLibelle(); ?></a>
  <ul>
    <li class="phone"><a href="#" class="back"><?php echo $oItem->getLibelle(); ?></a></li>
    <?php foreach($oItem->getLiensTries(null, $liensinterdits) as $oLien): ?>
    <?php if ($oLien->getTypeLien() == Lien::TYPE_LIEN_MENU): ?>
    <?php include_partial('common/menu', array('oItem' => $oLien, 'liens' => $liens, 'liensinterdits' => $liensinterdits)); ?>
    <?php else: ?>
    <?php include_partial('common/lien', array('oItem' => $oLien, 'liens' => $liens, 'liensinterdits' => $liensinterdits)); ?>
    <?php endif; ?>
    <?php endforeach; ?>
  </ul>
</li>