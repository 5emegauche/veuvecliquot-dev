<?php

class commonComponents extends sfComponents {
    public function executeFooter() {
        $this->Racine = Doctrine::getTable('Lien')->findOneBy('type_lien', Lien::TYPE_LIEN_RFOOTER);
    }
    
    public function executeHeader() {
        $this->RacineF = Doctrine::getTable('Lien')->findOneBy('type_lien', Lien::TYPE_LIEN_RFOOTER);
        $this->Racine = Doctrine::getTable('Lien')->findOneBy('type_lien', Lien::TYPE_LIEN_RHEADER);
    }
    
    public function executeShare() {
        
    }
    
    public function executePush() {
        
    }
}