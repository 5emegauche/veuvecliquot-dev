<?php

/**
 * common actions.
 *
 * @package    veuveclicquot
 * @subpackage common
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class commonActions extends VeuveActions {
    public function executeNocategoryparser(sfWebRequest $request) {
        $this->forward('statique', 'parser');
    }

    public function executeCategoryparser(sfWebRequest $request) {
        if ($request->getParameter('ancre') && !($this->isSocialBot($_SERVER['HTTP_USER_AGENT']))) {
          return $this->redirect('@category?sf_culture=' . $request->getParameter('sf_culture') . '&categoryslug=' . $request->getParameter('categoryslug') . '&pageslug=' . $request->getParameter('pageslug') . '#' . $request->getParameter('ancre'));
        }
        $sCategory = $request->getParameter('categoryslug');
        $sRedirectCategory = VeuveTranslator::getCategoryFromSlug($sCategory, $request->getParameter('sf_culture'));
        if (!$sRedirectCategory) {
            $this->forward404();
        }
        $this->getResponse()->setSlot('gtm_section_name', $sCategory);
        $this->forward($sRedirectCategory, 'parser');
    }

    public function executeQrcodecpparser(sfWebRequest $request) {
        $sIdProduit = 'cave-privee';
        $sAncre = $request->getParameter('ancre');
        switch($sAncre) {
          case 'carte-jaune':
            $sIdProduit = 'yellow-label';
            $sAncre = 'yellow-label';
            break;
          case 'rose':
            $sIdProduit = 'rose';
            break;
          case 'vintages':
          case 'vintagerose':
            $sIdProduit = 'vintages';
            break;
          case 'la-grande-dame':
          case 'la-grande-dame-rose':
            $sIdProduit = 'la-grande-dame';
            break;
          case 'demi-sec':
            $sIdProduit = 'demi-sec';
            break;
        }
        $sLangue = $this->getUser()->getCulture();
        $oFicheProduit = Doctrine::getTable('FicheProduit')
                                 ->createQuery('fp')
                                 ->innerJoin('fp.Translation t')
                                 ->where('t.lang = ?', $sLangue)
                                 ->andWhere('fp.idproduit = ?', $sIdProduit)
                                 ->fetchOne();
        if (!$oFicheProduit) {
            $this->forward404();
        }
        $this->redirect('@category?sf_culture=' . $sLangue . '&categoryslug=' . VeuveTranslator::getCategorySlug(VeuveTranslator::CATEGORY_CHAMPAGNE, $sLangue) . '&pageslug=' . $oFicheProduit->Translation[$sLangue]->slug . '#' . $sAncre);
    }

    public function executeQrcodeparser(sfWebRequest $request) {
        $sTypePage = $request->getParameter('pageproduit');
        $sIdProduit = $request->getParameter('idproduit');
        $sAncre = $request->getParameter('ancre');
        $sLangue = $this->getUser()->getCulture();
        switch($sTypePage) {
            case 'fiche':
                $oFicheProduit = Doctrine::getTable('FicheProduit')
                                 ->createQuery('fp')
                                 ->innerJoin('fp.Translation t')
                                 ->where('t.lang = ?', $sLangue)
                                 ->andWhere('fp.idproduit = ?', $sIdProduit)
                                 ->fetchOne();
                if (!$oFicheProduit) {
                    $this->forward404();
                }
                $this->redirect('@category?sf_culture=' . $sLangue . '&categoryslug=' . VeuveTranslator::getCategorySlug(VeuveTranslator::CATEGORY_CHAMPAGNE, $sLangue) . '&pageslug=' . $oFicheProduit->Translation[$sLangue]->slug . '#' . $sAncre);
                break;
            case 'offre':
                $oOffre = Doctrine::getTable('Offre')
                              ->createQuery('o')
                              ->innerJoin('o.Translation t')
                              ->where('t.lang = ?', $request->getParameter('sf_culture'))
                              ->andWhere('t.slug = ?', $sPageSlug)
                              ->fetchOne();
                if (!$oOffre) {
                    $this->forward404();
                }
                $this->redirect('@category?sf_culture=' . $sLangue . '&categoryslug=' . VeuveTranslator::getCategorySlug(VeuveTranslator::CATEGORY_CHAMPAGNE, $sLangue) . '&pageslug=' . $oOffre->Translation[$sLangue]->slug . '#' . $sAncre);
                break;
            default:
                break;
        }
    }
    
    public function executeLanguagechanger(sfWebRequest $request) {
        $this->redirect('@homepage');
    }
}
