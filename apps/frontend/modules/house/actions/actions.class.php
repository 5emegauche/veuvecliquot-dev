<?php

/**
 * house actions.
 *
 * @package    veuveclicquot
 * @subpackage house
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class houseActions extends VeuveActions {
    public function executeParser(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');
        
        $sArticleName = VeuveTranslator::getArticleFromSlug($sPageSlug, $request->getParameter('sf_culture'));
        switch($sArticleName) {
            case VeuveTranslator::ARTICLE_KEY_DATES:
                $this->forward('house', 'keydates');
                break;
            case VeuveTranslator::ARTICLE_PRODUCTION_CYCLE:
                $this->forward('house', 'productioncycle');
                break;
            case VeuveTranslator::ARTICLE_WOMEN_INSPIRATION:
                $this->forward('house', 'womenofinspiration');
                break;
            case VeuveTranslator::ARTICLE_GREAT_EXPEDITIONS:
                $this->forward('house', 'greatexpeditions');
                break;
            case VeuveTranslator::ARTICLE_CARNET_VENDANGES:
                $this->forward('house', 'carnetdevendange');
                break;
            case VeuveTranslator::ARTICLE_WE_ARE_CLICQUOT:
                $this->forward('house', 'weareclicquot');
                break;
            default:
                $oArticle = Doctrine::getTable('Article')
                                    ->createQuery('a')
                                    ->innerJoin('a.Translation t')
                                    ->where('t.lang = ?', $request->getParameter('sf_culture'))
                                    ->andWhere('t.slug = ?', $sPageSlug)
                                    ->fetchOne();
                
                if ($oArticle) {
                    $this->forward('house', 'article');
                }
                else {
                    $this->forward404();
                }
                break;
        }
    }
    
    public function executeArticle(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');

        $this->getResponse()->setSlot('gtm_content_name', 'Article : ' . $sPageSlug);

        $oArticle = Doctrine::getTable('Article')
                            ->createQuery('a')
                            ->innerJoin('a.Translation t')
                            ->where('t.lang = ?', $request->getParameter('sf_culture'))
                            ->andWhere('t.slug = ?', $sPageSlug)
                            ->fetchOne();
        $this->Article = $oArticle;
    }
    
    public function executeKeydates(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'Article : Key Dates');

        $this->Keydates = Doctrine::getTable('DateClef')
                                  ->createQuery('d')
                                  ->orderBy('d.annee ASC')
                                  ->execute();
    }
    
    public function executeWomenofinspiration(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'Article : Women of Inspiration');
        $this->ConfigPage = Doctrine::getTable('WomenPage')->findOneBy('active', 1);
        $this->Women = Doctrine::getTable('WomenOfInspiration')
                               ->createQuery('w')
                               ->orderBy('w.ordre ASC')
                               ->execute();
    }
    
    public function executeGreatexpeditions(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'Article : Great Expeditions');
        $this->ConfigPage = Doctrine::getTable('GreatExpeditionPage')->findOneBy('active', 1);
        $this->Expeditions = Doctrine::getTable('GreatExpedition')
                                     ->createQuery('ge')
                                     ->orderBy('ge.annee ASC')
                                     ->addOrderBy('ge.ordre ASC')
                                     ->execute();
    }
    
    public function executeProductioncycle(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'Article : Production Cycle');
        $this->ConfigPage = Doctrine::getTable('ProductionCycle')->findOneBy('active', 1);
    }

    public function executeCarnetdevendange(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'Article : Carnet de Vendanges');

        if ($request->getParameter('ancre') && ($this->isSocialBot($_SERVER['HTTP_USER_AGENT']))) {
          $this->sAncre = $request->getParameter('ancre');
        }

        $this->ConfigPage = Doctrine::getTable('CarnetVendangePage')->findOneBy('active', 1);
        $this->Carnets = Doctrine::getTable('CarnetVendange')
                                 ->createQuery('cv')
                                 ->where('cv.visible = ?', 1)
                                 ->orderBy('cv.ordre ASC')
                                 ->execute();
    }

    public function executeWeareclicquot(sfWebRequest $request)
    {
        $this->getResponse()->setSlot('gtm_content_name', 'Article : We Are Clicquot');

        if ($request->getParameter('ancre') && ($this->isSocialBot($_SERVER['HTTP_USER_AGENT']))) {
            $this->sAncre = $request->getParameter('ancre');
        }

        $this->ConfigPage = Doctrine::getTable('WeAreClicquotPage')->findOneBy('active', 1);
        $this->WeAreClicquots = Doctrine::getTable('WeAreClicquot')
                                        ->createQuery('wac')
                                        ->where('wac.visible = ?', 1)
                                        ->orderBy('wac.ordre ASC')
                                        ->execute();
    }
}
