<?php $sImage = null; ?>
<span class="ariane"><?php echo __('House'); ?></span>
<h1><?php echo __('Key dates'); ?></h1>
<div class="content">
  <div class="timeline-article">
    <div class="timeline-article-content">
      <?php include_partial('article/keydate', array('keydate' => $Keydates[0])); ?>
    </div>
  </div>
  
  <div class="timeline">
    <span><?php echo $Keydates[0]->getAnnee(); ?></span>
    <img src="/images/content/loading.gif" alt="" />
    <ul>
      <?php foreach($Keydates as $index => $oKeydate): ?>
      <?php if (!$sImage) $sImage = $oKeydate->getUrlImage(); ?>
      <li><a href="<?php echo url_for('@ajax_keydate?annee=' . $oKeydate->getAnnee()); ?>" data-year="<?php echo $oKeydate->getAnnee(); ?>"<?php echo ($index == 0)?' class="select"':''; ?>><?php echo $oKeydate->getAnnee(); ?></a></li>
      <?php endforeach; ?>
    </ul>
  </div>
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $sImage)); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_KEY_DATES); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>