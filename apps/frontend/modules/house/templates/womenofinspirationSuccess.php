<?php $sImage = null; ?>
<?php $ConfigPage instanceof WomenPage; ?>
<span class="ariane"><?php echo __('House'); ?></span>
<h1><?php echo __('Women of inspiration'); ?></h1>
<div class="content">
  <?php $Article = ($ConfigPage->getArticleId())?$ConfigPage->getArticle():null; ?>
  <?php 
  if ($Article):
    foreach($Article->getBlocsTries() as $oBloc):
      $oBloc instanceof BlocArticle;
      if ($oBloc->getTexte()):
        if (!$sImage) $sImage = $oBloc->getUrlImage();
        switch($oBloc->getTypeBloc()):
          case Bloc::BLOCTYPE_UNEIMAGE:
          case Bloc::BLOCTYPE_UNEIMAGELARGE:
            $aParams = array(
                'shop'              => 'non',
                'image_src'         => $oBloc->getUrlImage(),
                'titre'             => $oBloc->getTitre(),
                'texte'             => $oBloc->getTexte(),
                'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
                'wrdSlide'          => $oBloc->getSlider(),
                'ecommerce'         => false,
                'lienecommerce'     => '',
            );
            include_partial('article/uneimage', $aParams);
            break;
          case Bloc::BLOCTYPE_DEUXIMAGES:
            $aParams = array(
                'shop'              => 'non',
                'image_src'         => $oBloc->getUrlImage(),
                'image2_src'        => $oBloc->getUrlImage2(),
                'titre'             => $oBloc->getTitre(),
                'texte'             => $oBloc->getTexte(),
                'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
                'wrdSlide'          => $oBloc->getSlider(),
                'ecommerce'         => false,
                'lienecommerce'     => '',
            );
            include_partial('article/deuximages', $aParams);
            break;
          case Bloc::BLOCTYPE_VIDEO:
            $aParams = array(
                'shop'              => 'non',
                'video_preview_src' => $oBloc->getUrlImage(),
                'video_src'         => $oBloc->getUrlVideo(),
                'titre'             => $oBloc->getTitre(),
                'texte'             => $oBloc->getTexte(),
                'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
                'wrdSlide'          => $oBloc->getSlider(),
                'ecommerce'         => false,
                'lienecommerce'     => '',
            );
            include_partial('article/video', $aParams);
            break;
        endswitch;
      endif;
    endforeach;
  endif;
  ?>
</div>
<article class="women">
  <h3><?php echo $ConfigPage->getTitre(); ?></h3>
  <hr />
  <?php echo html_entity_decode($ConfigPage->getTexte()); ?>
  <ul>
    <?php foreach($Women as $oWoman): ?>
    <?php $oWoman instanceof WomenOfInspiration; ?>
    <?php if (!$sImage) $sImage = $oWoman->getUrlPhoto(); ?>
    <li class="clearfix">
      <figure>
        <img src="<?php echo $oWoman->getUrlPhoto(); ?>" alt="<?php echo $oWoman->getNom();?>" />
      </figure>
      <div class="text">
        <h5><?php echo $oWoman->getNom(); ?></h5>
        <strong><?php echo $oWoman->getPays(); ?></strong>
        <?php echo html_entity_decode($oWoman->getTexte()); ?>
      </div>
    </li>
    <?php endforeach; ?>
  </ul>
</article>
<div class="content">
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $sImage)); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_WOMEN_INSPIRATION); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>