<?php $sImage = null; ?>
<?php $Article instanceof Article; ?>
<span class="ariane"><?php echo $Article->getAriane(); ?></span>
<h1><?php echo $Article->getTitre(); ?></h1>
<div class="content">
  <video id="video" controls="controls" style="display:none;position:absolute;">
    <source src="" />
  </video>
  <!-- Blocs article -->
  <?php
  foreach($Article->getBlocsTries() as $index => $oBloc):
    $oBloc instanceof BlocArticle;
    if($oBloc->getTexte()):
      if (!$sImage) $sImage = $oBloc->getUrlImage();
      switch($oBloc->getTypeBloc()):
        case Bloc::BLOCTYPE_UNEIMAGE:
        case Bloc::BLOCTYPE_UNEIMAGELARGE:
          $aParams = array(
            'shop'              => 'non',
            'image_src'         => $oBloc->getUrlImage(),
            'titre'             => $oBloc->getTitre(),
            'texte'             => $oBloc->getTexte(),
            'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
            'wrdSlide'          => $oBloc->getSlider(),
            'ecommerce'         => false,
            'lienecommerce'     => '',
          );
          include_partial('article/uneimage', $aParams);
          break;
        case Bloc::BLOCTYPE_DEUXIMAGES:
          $aParams = array(
            'shop'              => 'non',
            'image_src'         => $oBloc->getUrlImage(),
            'image2_src'        => $oBloc->getUrlImage2(),
            'titre'             => $oBloc->getTitre(),
            'texte'             => $oBloc->getTexte(),
            'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
            'wrdSlide'          => $oBloc->getSlider(),
            'ecommerce'         => false,
            'lienecommerce'     => '',
          );
          include_partial('article/deuximages', $aParams);
          break;
        case Bloc::BLOCTYPE_VIDEO:
          $aParams = array(
            'shop' => 'non',
            'video_preview_src' => $oBloc->getUrlImage(),
            'video_src'         => $oBloc->getUrlVideo(),
            'titre'             => $oBloc->getTitre(),
            'texte'             => $oBloc->getTexte(),
            'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
            'wrdSlide'          => $oBloc->getSlider(),
            'ecommerce'         => false,
            'lienecommerce'     => '',
          );
          include_partial('article/video', $aParams);
          break;
      endswitch;
    endif;
  endforeach;
  ?>
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $sImage)); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocArticleBlocPush::getBlocsTries($Article->getId()); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>