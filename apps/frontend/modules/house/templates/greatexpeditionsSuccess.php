<?php $sImage = null; ?>
<?php $ConfigPage instanceof GreatExpeditionPage; ?>
<span class="ariane"><?php echo __('History'); ?></span>
<h1><?php echo __('Great Expeditions'); ?></h1>
<div class="content">
  <?php $Article = ($ConfigPage->getArticleId())?$ConfigPage->getArticle():null; ?>
  <?php 
  if ($Article):
    foreach($Article->getBlocsTries() as $oBloc):
      $oBloc instanceof BlocArticle;
      if ($oBloc->getTexte()):
        if (!$sImage) $sImage = $oBloc->getUrlImage();
        switch($oBloc->getTypeBloc()):
          case Bloc::BLOCTYPE_UNEIMAGE:
          case Bloc::BLOCTYPE_UNEIMAGELARGE:
            $aParams = array(
                'shop'              => 'non',
                'image_src'         => $oBloc->getUrlImage(),
                'titre'             => $oBloc->getTitre(),
                'texte'             => $oBloc->getTexte(),
                'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
                'wrdSlide'          => $oBloc->getSlider(),
                'ecommerce'         => false,
                'lienecommerce'     => '',
            );
            include_partial('article/uneimage', $aParams);
            break;
          case Bloc::BLOCTYPE_DEUXIMAGES:
            $aParams = array(
                'shop'              => 'non',
                'image_src'         => $oBloc->getUrlImage(),
                'image2_src'        => $oBloc->getUrlImage2(),
                'titre'             => $oBloc->getTitre(),
                'texte'             => $oBloc->getTexte(),
                'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
                'wrdSlide'          => $oBloc->getSlider(),
                'ecommerce'         => false,
                'lienecommerce'     => '',
            );
            include_partial('article/deuximages', $aParams);
            break;
          case Bloc::BLOCTYPE_VIDEO:
            $aParams = array(
                'shop' => 'non',
                'video_preview_src' => $oBloc->getUrlImage(),
                'video_src'         => $oBloc->getUrlVideo(),
                'titre'             => $oBloc->getTitre(),
                'texte'             => $oBloc->getTexte(),
                'url_shop'          => (isset($oBloc->lien_bouton_shop))?$oBloc->getLienBoutonShop():'',
                'wrdSlide'          => $oBloc->getSlider(),
                'ecommerce'         => false,
                'lienecommerce'     => '',
            );
            include_partial('article/video', $aParams);
            break;
        endswitch;
      endif;
    endforeach;
  endif;
  ?>
  
  <ul class="dates-menu">
    <li><a href="#" class="select">1700</a></li>
    <li><a href="#">1800</a></li>
  </ul>
  <ul class="dates">
    <?php foreach($Expeditions as $oExpedition): ?>
    <?php $oExpedition instanceof GreatExpedition; ?>
    <li class="year<?php echo $oExpedition->getSiecleFromAnnee(); ?>">
      <?php if ($oExpedition->getTypeBloc() == GreatExpedition::EXPEDITION_IMAGE): ?>
      <img src="<?php echo $oExpedition->getUrlImage(); ?>" alt="" />
      <?php else: ?>
      <article>
        <h4><?php echo $oExpedition->getAnnee(); ?></h4>
        <hr />
        <h5><?php echo $oExpedition->getPays(); ?></h5>
        <?php echo html_entity_decode($oExpedition->getTexte()); ?>
      </article>
      <?php endif; ?>
    </li>
    <?php endforeach; ?>
  </ul>
  
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $sImage)); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_GREAT_EXPEDITIONS); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>