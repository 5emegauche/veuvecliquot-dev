<?php $sImage = '/images/content/weareclicquot/intro_img.jpg'; ?>
<?php $sTitre = null; ?>
<?php $sDescription = null; ?>
<?php $ConfigPage instanceof CarnetVendangePage; ?>
<?php $pays = Doctrine::getTable('Pays')->findOneBy('slug', $sf_user->getAttribute('pays')); ?>

<?php $pageUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

<div class='page-weareclicquot'>

    <div class="intro">
        <h1><?php echo __('weareclicquot.discover'); ?></h1>

        <div class="intro_img_content">
          <img class="intro_img" src="/images/content/weareclicquot/intro_img.jpg">
        </div>

        <p><?php echo __('weareclicquot.introduction'); ?></p>

        <div class="intro_foot">
            <div class="link_episode">
                <a id="episode_nextSlide" href="#slide0" class="after-icon-right"><?php echo __('weareclicquot.episodes') ?></a>
            </div>
        </div>
    </div>

    <div class="slideShow">
        <ul class="clearfix">
            <?php
              $counter = 1;
              $nbEpisodes = count($WeAreClicquots);
              foreach ($WeAreClicquots as $index => $WeAreClicquot):
                  /** @var WeAreClicquot $WeAreClicquot */
                  if ($sTitre == null) $sTitre = $WeAreClicquot->getTitre();
                  if ($sDescription == null) $sDescription = $WeAreClicquot->getTexte();
                  $sIdZone = 'slide' . $index;
                  if ($sAncre == $sIdZone) {
                      $sTitre = $WeAreClicquot->getTitre();
                      $sDescription = $WeAreClicquot->getTexte();
                  }
                  $sCurrentUrl = $pageUrl . '/' . $sIdZone;
            ?>
	        <li data-slide="<?php echo $index; ?>" data-id="<?php echo $sIdZone; ?>" id="<?php echo $sIdZone; ?>" data-fbshare="https://www.facebook.com/sharer/sharer.php?u=<?php echo $sCurrentUrl; ?>&t=<?php echo $WeAreClicquot->getTitre(); ?>" data-twshare="https://twitter.com/share?url=<?php echo $sCurrentUrl; ?>&text=<?php echo urlencode(html_entity_decode($WeAreClicquot->getTitre())); ?>">
		        <article class="article news">
		            <figure>
		                <a href="#article">
		                  <iframe class="slider-video" src="<?php echo $WeAreClicquot->getVideo(); ?>" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
		                </a>
		            </figure>
		            <div class="nav-slider">
  			        	<img class="arrow-left <?php if ($index == 0): ?>backIntro<?php else: ?>prev<?php endif; ?>" src="/images/content/weareclicquot/arrow_left.png" alt="<" width="25" height="33" />
  			        	<img class="arrow-right next" src="/images/content/weareclicquot/arrow_right.png" alt=">" width="25" height="33" />
  			        </div>
                <?php if(0): ?>
                <ul class="social noslide">
                    <li class="facebook noslide"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $sCurrentUrl; ?>&t=<?php echo $title; ?>" class="image-text" title="Facebook" target="_blank">Facebook</a></li>
                    <li class="twitter noslide"><a href="https://twitter.com/share?url=<?php echo $sCurrentUrl; ?>&text=<?php echo urlencode(html_entity_decode($WeAreClicquot->getTitre())); ?>" class="image-text" title="Twitter" target="_blank">Twitter</a></li>
                </ul>
                <?php endif; ?>
		            <h4>We Are Clicquot</h4>
		            <hr />
		            <h3><?php echo $WeAreClicquot->getTitre(); ?></h3>
		            <p class="slider-desc"><?php echo html_entity_decode($WeAreClicquot->getTexte()); ?></p>
		        </article>
		    </li>
            <?php
              endforeach;
            ?>

	    </ul>
        <!-- Partage -->
	</div>

  <?php include_component('common', 'share', array('image' => $sImage, 'forcefb' => true, 'forcetw' => true)); ?>

  <div class="push-margin">
    <!-- Pushs -->
    <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_WE_ARE_CLICQUOT); ?>
    <?php $aBlocs = array(); ?>
    <?php foreach($aBlocsPush as $oBlocPush): ?>
    <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
    <?php endforeach; ?>
    <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
    <?php if ($sImage) slot('ogimage', $sImage); ?>
    <?php if ($sTitre) slot('ogtitle', 'Veuve Clicquot - ' . strip_tags(html_entity_decode($sTitre))); ?>
    <?php if ($sDescription) slot('ogdescription', strip_tags(html_entity_decode($sDescription))); ?>
  </div>
</div>