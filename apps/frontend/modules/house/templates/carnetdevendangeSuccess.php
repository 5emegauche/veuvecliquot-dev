<?php $sImage = null; ?>
<?php $sTitre = null; ?>
<?php $sDescription = null; ?>
<?php $ConfigPage instanceof CarnetVendangePage; ?>

<?php $pageUrl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>

<div class="content">
  
  <ul class="carnet slideme">
    <?php

      $overflow = ' class="content-overflow"';
      
      foreach($Carnets as $index => $PageCarnet):
      if ($sImage == null) $sImage = $PageCarnet->getVisuelSrc();
      if ($sTitre == null) $sTitre = $PageCarnet->getTitre();
      if ($sDescription == null) $sDescription = $PageCarnet->getTexte1();
      $supClass = '';
      if($index == 0)
      {
        $supClass = ' first';
      }
      else
      {
        if(($index+1) % 2 == 0)
        {
          $supClass = ' even middle';
        }
        else
        {
          $supClass = ' odd middle';
        }
      }
    ?>
    <li id="carnet-block-<?php echo $index; ?>" class="pagecarnet block-overflow<?php echo $supClass; ?>">
    <div>
      <h2 class="h2forphone"><?php echo html_entity_decode($PageCarnet->getTitre()); ?></h2>
      <figure>
        <img src="<?php echo $PageCarnet->getVisuelSrc(); ?>" />
      </figure>
      <div class="carnet-content" id="carnet-<?php echo $index; ?>">
        <div class="scroll-block">
          <h2 class="h2fortablet"><?php echo html_entity_decode($PageCarnet->getTitre()); ?></h2>
          <div class="carousel">
        
        <ul class="date-vendange">
          <?php
            $sIdZone = 'carnet-' . $index . '-date-1';
            if ($sAncre == $sIdZone) {
              $sImage = $PageCarnet->getVisuelSrc();
              $sTitre = $PageCarnet->getTitre() . ' - ' . $PageCarnet->getDate1();
              $sDescription = $PageCarnet->getTexte1();
            }
          ?>
          <li data-id="<?php echo $sIdZone; ?>">
            
            <h3><?php echo html_entity_decode($PageCarnet->getDate1()); ?></h3>
            <div>
            <?php echo html_entity_decode($PageCarnet->getTexte1()); ?>
            </div>

            <?php $pays = Doctrine::getTable('Pays')->findOneBy('slug', $sf_user->getAttribute('pays')); ?>
<?php if (($pays->getAfficheFacebook()) || ($pays->getAfficheTwitter())): ?>

<?php $dateUrl = urlencode($pageUrl.'/'.$sIdZone); ?>
<?php $title = urlencode($PageCarnet->getTitre()).' :: '.html_entity_decode($PageCarnet->getDate1()); ?>

        <ul class="social">
          <?php if ($pays->getAfficheFacebook()): ?>
<li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $dateUrl; ?>&t=<?php echo $title; ?>" class="image-text" title="Facebook" target="_blank">Facebook</a></li>
<?php endif; ?>
<?php if ($pays->getAfficheTwitter()): ?>
<li class="twitter"><a href="https://twitter.com/share?url=<?php echo $dateUrl; ?>&text=<?php echo html_entity_decode($PageCarnet->getTitre()).' :: '.html_entity_decode($PageCarnet->getDate1()); ?>" class="image-text" title="Twitter" target="_blank">Twitter</a></li>
<?php endif; ?>
            </ul>
            <?php endif; ?>

            <form action="" target="_blank"><p class="shop-block"><span class="shop-action"><input type="submit" name="" value="Shop now"/></span></p></form>

          </li>
          <?php if (($PageCarnet->getDate2()) && ($PageCarnet->getTexte2())): ?>
          <?php
            $sIdZone = 'carnet-' . $index . '-date-2';
            if ($sAncre == $sIdZone) {
              $sImage = $PageCarnet->getVisuelSrc();
              $sTitre = $PageCarnet->getTitre() . ' - ' . $PageCarnet->getDate1();
              $sDescription = $PageCarnet->getTexte1();
            }
          ?>
          <li data-id="<?php echo $sIdZone; ?>">
            
            <h3><?php echo html_entity_decode($PageCarnet->getDate2()); ?></h3>
            <div>
            <?php echo html_entity_decode($PageCarnet->getTexte2()); ?>
            </div>
            <?php $pays = Doctrine::getTable('Pays')->findOneBy('slug', $sf_user->getAttribute('pays')); ?>
<?php if (($pays->getAfficheFacebook()) || ($pays->getAfficheTwitter())): ?>
<?php $dateUrl = urlencode($pageUrl.'/'.$sIdZone); ?>
        <ul class="social">
          <?php if ($pays->getAfficheFacebook()): ?>
<li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $dateUrl; ?>" class="image-text" title="Facebook" target="_blank">Facebook</a></li>
<?php endif; ?>
<?php if ($pays->getAfficheTwitter()): ?>
<li class="twitter"><a href="https://twitter.com/share?url=<?php echo $dateUrl; ?>&text=<?php echo html_entity_decode($PageCarnet->getTitre()).' :: '.html_entity_decode($PageCarnet->getDate2()); ?>" class="image-text" title="Twitter" target="_blank">Twitter</a></li>
<?php endif; ?>
            </ul>
            <?php endif; ?>
            
          </li>
          <?php endif; ?>
          <?php if (($PageCarnet->getDate3()) && ($PageCarnet->getTexte3())): ?>
          <?php
            $sIdZone = 'carnet-' . $index . '-date-3';
            if ($sAncre == $sIdZone) {
              $sImage = $PageCarnet->getVisuelSrc();
              $sTitre = $PageCarnet->getTitre() . ' - ' . $PageCarnet->getDate3();
              $sDescription = $PageCarnet->getTexte3();
            }
          ?>
          <li data-id="<?php echo $sIdZone; ?>">
            
            <h3><?php echo html_entity_decode($PageCarnet->getDate3()); ?></h3>
            <div>
            <?php echo html_entity_decode($PageCarnet->getTexte3()); ?>
            </div>

            <?php $pays = Doctrine::getTable('Pays')->findOneBy('slug', $sf_user->getAttribute('pays')); ?>
<?php if (($pays->getAfficheFacebook()) || ($pays->getAfficheTwitter())): ?>
<?php $dateUrl = urlencode($pageUrl.'/'.$sIdZone); ?>
        <ul class="social">
          <?php if ($pays->getAfficheFacebook()): ?>
<li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $dateUrl; ?>" class="image-text" title="Facebook" target="_blank">Facebook</a></li>
<?php endif; ?>
<?php if ($pays->getAfficheTwitter()): ?>
<li class="twitter"><a href="https://twitter.com/share?url=<?php echo $dateUrl; ?>&text=<?php echo html_entity_decode($PageCarnet->getTitre()).' :: '.html_entity_decode($PageCarnet->getDate3()); ?>" class="image-text" title="Twitter" target="_blank">Twitter</a></li>
<?php endif; ?>
            </ul>
            <?php endif; ?>
            
          </li>
          <?php endif; ?>
        </ul>
        
      </div>
    </div>

      </div>

      </div>
    </li>
    <?php 
      endforeach;
    ?>
  </ul>
  
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $sImage)); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_CARNET_VENDANGES); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
  <?php if ($sImage) slot('ogimage', $sImage); ?>
  <?php if ($sTitre) slot('ogtitle', 'Veuve Clicquot - ' . strip_tags(html_entity_decode($sTitre))); ?>
  <?php if ($sDescription) slot('ogdescription', strip_tags(html_entity_decode($sDescription))); ?>
</div>
