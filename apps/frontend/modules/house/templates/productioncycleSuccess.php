<span class="ariane"><?php echo __('Art of Wine'); ?></span>
<h1><?php echo __('Production Cycle'); ?></h1>
<figure class="schema">
  <img src="<?php echo $ConfigPage->getUrlImage(); ?>" alt="schema">
</figure>
<div class="content">
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $ConfigPage->getUrlImage())); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::ARTICLE_PRODUCTION_CYCLE); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>