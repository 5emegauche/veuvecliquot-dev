<div class="wrap">
  <img src="/images/content/logo-prehome.png" alt="veuve clicquot" />
  <?php if($displayCookieFrame): ?>
  <div id="cookie-usage" class="cookie_frame ui-state-active">
    <?php //echo __('En accédant au site veuveclicquot.com, vous acceptez l\'utilisation de cookies pour améliorer votre expérience et collecter des informations sur votre usage de notre site internet. Pour plus d\'information sur les cookies utilisés et savoir comment les supprimer, consultez <a href="/pdf/Données Personnelles - Cookies.pdf" target="_blank">notre politique concernant les cookies</a>.'); ?>
    By entering veuve-clicquot.com, you accept the use of cookies to enhance your user experience and to collect information on the use of our website. To find out more about cookies and <a href="http://info.evidon.com/pub_info/3492" target="_blank">how to delete and control them</a>, please consult our <a href="/pdf/Cookies Anglais AGE GATE.pdf" target="_blank">cookies policy</a>.
  </div>
  <?php endif; ?>

  <div class="visit">
  <?php //echo __('visit.message'); ?>
  To visit the veuve-clicquot site, you must be of legal drinking age in your country of residence.
If there is no legal age for consuming alcohol in your country, you must be over 21 years old.
  </div>
  
  <form action="" method="post" id="form_ph">
    <fieldset>
    <h4><?php //echo __('Pays'); ?>Country</h4>
    <select name="pays" id="pays">
      <?php foreach($Pays as $sIndex => $aPays): ?>
      <?php $aPays = $aPays->getRawValue(); ?>
      <?php $selected = ($aPays['selected'] ? 'selected="selected"' : '') ?>
      <option value="<?php echo $sIndex; ?>" data-age="<?php echo $aPays['age']; ?>" data-autorise="<?php echo $aPays['autorise']; ?>" data-langues="<?php echo implode(',', $aPays['langues']); ?>" <?php echo $selected ?>><?php echo $aPays['nom']; ?></option>
      <?php endforeach; ?>
    </select>
    </fieldset>
    <fieldset>
    <h4><?php //echo __('Langue'); ?>Language</h4>
    <select name="lang" id="lang">
      <?php foreach($Langues as $oLangue): ?>
      <?php $oLangue instanceof Langue; ?>
      <option value="<?php echo $oLangue->getCode(); ?>" id="<?php echo $oLangue->getCode(); ?>"><?php echo $oLangue->getNom(); ?></option>
      <?php endforeach; ?>
    </select>
    </fieldset>

    <ul class="year-choice">
    <li>
    <h4><?php //echo __('Mois'); ?>Month</h4>
    <select name="mois_nais" id="mois">
      <?php for($i=1;$i<=$Mois;$i++): ?>
      <?php $monthValue = sprintf('%02d',$i); ?>
      <option value="<?php echo $monthValue; ?>"><?php echo $monthValue; ?></option>
      <?php endfor; ?>
    </select>
    </li>
    <li>
    <h4><?php //echo __('Jour'); ?>Day</h4>
    <select name="jour_nais" id="jours">
      <?php for($i=1;$i<=$Jours;$i++): ?>
      <?php $dayValue = sprintf('%02d',$i); ?>
      <option value="<?php echo $dayValue; ?>"><?php echo $dayValue; ?></option>
      <?php endfor; ?>
    </select>
    </li>
    <li class="year">
    <h4><?php //echo __('Année'); ?>Year</h4>
    <select name="annee_nais" id="annee">
      <?php for($i=$Annees['start'];$i<=$Annees['end'];$i++): ?>
      <?php $yearValue = sprintf('%d',$i); ?>
      <option value="<?php echo $yearValue; ?>"<?php if($yearValue=='1905'){ echo ' selected="selected"'; } ?>><?php echo $yearValue; ?></option>
      <?php endfor; ?>
    </select>
    </li>
    </ul>

    <?php /*
    <input type="hidden" name="jour_nais" value="<?php echo date('d'); ?>" />
    <input type="hidden" name="mois_nais" value="<?php echo date('m'); ?>" />
    <input type="hidden" name="annee_nais" id="input_annee_naissance" />
    */ ?>
    <?php /*
    <p id="age_legal"><?php echo str_replace(array('#DDNJOUR#', '#DDNMOIS#'), array(date('d'), date('m')), __('Etes vous né(e) avant le #DDNJOUR#/#DDNMOIS#/<span id="annee_ph">1975</span>&nbsp;?')); ?></p>
    
    <p id="trop_jeune" style="display:none;color:red;"><?php echo __('msg.trop_jeune'); ?></p>
    <a href="#" id="btn-yes"><?php echo __('OUI'); ?></a><a href="#" id="btn-no"><?php echo __('NON'); ?></a>
    */ ?>

    <p id="trop_jeune" class="error message"><?php //echo __('msg.trop_jeune'); ?>Sorry, to visit the veuve-clicquot site, you must be of legal drinking age in your country of residence <a href="http://www.responsibledrinking.eu/">http://www.responsibledrinking.eu/</a></p>
    <p class="action"><input type="submit" name="ok" class="ok" value="<?php //echo __('Entrer'); ?>Enter" /></p>
  </form>
  <div class="conditions">
    <?php /*
    <p><?php echo __('L’abus d’alcool est dangeureux pour la santé. À consommer avec modération'); ?><br />
    <?php echo __('PAR LE BIAIS DU GROUPE AUQUEL IL APPARTIENT, VEUVECLICQUOT EST MEMBRE DE SpiritsEUROPE, LE FORUM EUROPÉEN SUR LA CONSOMMATION RESPONSABLE DE L\'ALCOOL (<a href="http://www.responsibledrinking.eu" target="_blank">www.responsibledrinking.eu</a>), D\'ENTREPRISE ET PRÉVENTION (www.preventionalcool.com) ET DU GROUPE DE RÉFLEXION ET D\'INITIATIVE DES PRODUCTEURS DE BOISSONS (<a href="http://www.wineinmoderation.eu" target="_blank">www.wineinmoderation.eu</a>).'); ?></p>
    <hr>
    <p><?php echo __('En entrant sur ce site, vous acceptez d\'être soumis à ses termes et conditions d\'utilisation, ainsi qu\'aux conditions de protection des données personnelles'); ?></p>
*/ ?>
    <p><?php //echo __('alcohol.abuse'); ?>The abuse of alcohol is dangerous for your health<br />
    <?php //echo __('veuveclicquot.groupe'); ?>VEUVE CLICQUOT SUPPORTS THE RESPONSIBLE CONSUMPTION OF WINES AND SPIRITS, THROUGH MOËT HENNESSY, MEMBER OF THE EUROPEAN FORUM FOR RESPONSIBLE DRINKING (<a href="http://www.responsibledrinking.eu">www.responsibledrinking.eu</a>) AND ENTREPRISE ET PRÉVENTION (<a href="http://www.preventionalcool.com">WWW.PREVENTIONALCOOL.COM</a>), CEEV (<a href="http://www.wineinmoderation.eu">www.wineinmoderation.eu</a>).</p>
    <hr>
    <p><?php //echo __('terms.conditions'); ?>By entering this site you acknowledge that you accept its terms and conditions of use and privacy policy.</p>
  </div>
  <div class="tampon"></div>
</div>
<?php /*
<footer><p><?php echo __('L’abus d’alcool est dangeureux pour la santé. À consommer avec modération'); ?></p></footer>
*/ ?>
