<?php

/**
 * prehome actions.
 *
 * @package    veuveclicquot
 * @subpackage prehome
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class prehomeActions extends sfActions {
    public function preExecute() {
        if (sfConfig::get('app_environnement_redirection', 1)) {
            $oMobileDetect = new MobileDetect();
            if ((!$oMobileDetect->isMobile()) && (!$oMobileDetect->isTablet())) {
                $this->redirect('http://www.veuve-clicquot.com/');
            }
        }
    }
    
    public function executeIndex(sfWebRequest $request) {
        if ($request->isMethod(sfWebRequest::POST)) {
            $this->_processChoix($request);
        }
        
        if ($this->getUser()->isAuthenticated()) {
            $this->redirect('@homepage');
        }
        
        $this->setLayout('prehome');
        
        $qPays = Doctrine::getTable('Pays')
                         ->createQuery('p')
                         ->orderBy('p.ordre = 0')
                         ->addOrderBy('p.ordre ASC')
                         ->addOrderBy('p.nom ASC')
                         ->execute();
        
        $aPays = array();

        if (function_exists('geoip_country_code_by_name') && $remote_country = geoip_country_code_by_name($_SERVER['REMOTE_ADDR'])) {
            $remote_country = strtolower($remote_country);
        } else {
            $remote_country = 'us';
        }

        foreach($qPays as $oPays) {
            $oPays instanceof Pays;
            $aPays[$oPays->getSlug()] = array('nom' => $oPays->getNom(), 'age' => $oPays->getAgeLegal(), 'autorise' => $oPays->getAutorise(), 'langues' => array(), 'selected' => false);

            if (strtolower($oPays->getCodeiso()) == $remote_country) {
                $aPays[$oPays->getSlug()]['selected'] = true;
            }

            foreach($oPays->getLangue() as $oLangue)
                $aPays[$oPays->getSlug()]['langues'][] = $oLangue->getCode();
        }
        
        $qLangues = Doctrine::getTable('Langue')
                            ->createQuery('l')
                            ->orderBy('l.nom ASC')
                            ->execute();
        
        $this->Pays    = $aPays;
        $this->Langues = $qLangues;

        $this->Mois = 12;
        $this->Jours = 31;
        $this->Annees = array(
          'start' => 1900,
          'end' => date('Y')
        );

        if (!isset($_COOKIE['accept_cookie'])) {
            $this->displayCookieFrame = true;
        }
        else {
            $this->displayCookieFrame = false;
        }
        
        if (($_SERVER['HTTP_HOST'] == 'm.veuve-clicquot.com.lvmh-lin3-wbdpp01-pub.lvmh.lbn.fr') && (isset($_GET['lang']))) {
            $culture = $_GET['lang'];
        }
        else {
            $aLanguesNavigateur = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
            $bLangueTrouvee = false;
            $culture = 'en';
            foreach($aLanguesNavigateur as $sLangueNavigateur) {
                if (!$bLangueTrouvee && in_array(substr($sLangueNavigateur, 0, 2), array_keys(sfConfig::get('app_cultures')))) {
                    $culture = substr($sLangueNavigateur, 0, 2);
                    $bLangueTrouvee = true;
                }
            }
        }
        $this->getUser()->setCulture($culture);
    }
    
    private function _processChoix(sfWebRequest $request) {
        $sPays = $request->getPostParameter('pays');
        $oPays = Doctrine::getTable('Pays')->findOneBy('slug', $sPays);
        $sLangue = $request->getPostParameter('lang');
        $aDDN = array(
            'jour'  => $request->getPostParameter('jour_nais'),
            'mois'  => $request->getPostParameter('mois_nais'),
            'annee' => $request->getPostParameter('annee_nais')
        );
        
        $this->getUser()->setAuthenticated(true);
        $this->getUser()->setAttribute('pays', $sPays);
        $this->getUser()->setAttribute('pays_iso', strtoupper($oPays->getCodeiso()));
        $this->getUser()->setAttribute('ddn', $aDDN);
        $this->getUser()->setCulture($sLangue);
        
        $sRedirection = $this->getUser()->getAttribute('redirection');

        if ($sRedirection) {
            $this->getUser()->setAttribute('redirection', null);
            $this->redirect($sRedirection);
        }
        else {
            $this->redirect('@homepage');
        }
    }
}
