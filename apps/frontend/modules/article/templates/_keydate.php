<?php $keydate instanceof DateClef; ?>
<article class="article">
  <figure class="slideShow">
    <img src="<?php echo $keydate->getUrlImage(); ?>" alt="" draggable="false">
  </figure>
  <h3><?php echo $keydate->getTitre(); ?></h3>
  <hr>
  <div class="overflow">
    <?php echo html_entity_decode($keydate->getTexte()); ?>
  </div>
</article>