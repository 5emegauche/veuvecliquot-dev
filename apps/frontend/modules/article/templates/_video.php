<?php $overflow = (isset($overflow))?$overflow:'non'; ?>
<article class="article">
  <figure>
    <a href="#" class="video" data-src="<?php echo $video_src; ?>">
      <img src="<?php echo $video_preview_src; ?>" alt="">
    </a>
  </figure>


  <?php if (($shop == 'non') && ($overflow == 'non')): ?>
    <?php if ($titre): ?>
      <h3><?php echo $titre; ?></h3>
      <hr>
    <?php endif; ?>
    <?php echo html_entity_decode($texte); ?>
  <?php else: ?>
  <div class="overflow">
    <?php if ($titre): ?>
      <h3><?php echo $titre; ?></h3>
      <hr>
    <?php endif; ?>
    <?php echo html_entity_decode($texte); ?>
    <?php if ((0) && ($shop == 'oui')): ?>
  <div class="shop-bt">
    <?php if (has_slot("gtm_product_name")): ?>
    <a href="<?php echo $url_shop; ?>" target="_blank" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, productName: sProductName, countryCode: sCountryCode, languageCode: sLanguageCode, eventAction: 'Online Purchase' });"><?php echo __('Shop online'); ?></a>
    <?php else: ?>
    <a href="<?php echo $url_shop; ?>" target="_blank" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, eventAction: 'Online Purchase' });"><?php echo __('Shop online'); ?></a>
    <?php endif; ?>
  </div>
    <?php endif; ?>
    <?php if ($ecommerce): ?>
    <form action="<?php echo $lienecommerce; ?>" target="_blank">
      <p class="shop-block">
        <span class="shop-action">
          <input type="submit" class="ecommerce-button" name="" value="<?php echo __('Shop now'); ?>" onclick="FiftyFiveTrack({ event: 'ecommerceButton', productName: '<?php echo addslashes(strip_tags(html_entity_decode($titre))); ?>' });" />
        </span>
      </p>
    </form>
    <?php endif; ?>
  </div>
  <?php endif; ?>
  <?php if(count($wrdSlide)):?>
  <div class="slideShow">
    <ul class="clearfix">
      <?php foreach($wrdSlide as $index => $slide): ?>
      <li data-slide="<?php echo $index; ?>">
        <div class="citation">
          <span><?php echo $slide->getTitre(); ?></span>
          <strong><?php echo $slide->getTexte(); ?></strong>
        </div>
      </li>
      <?php endforeach; ?>
    </ul>
  </div>
  <?php endif;?>
</article>