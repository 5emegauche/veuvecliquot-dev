<?php

/**
 * ajax actions.
 *
 * @package    veuveclicquot
 * @subpackage ajax
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ajaxActions extends sfActions {
    public function executeKeydate(sfWebRequest $request) {
        $iAnnee = $request->getParameter('annee');
        $oKeydate = Doctrine::getTable('DateClef')->findOneBy('annee', $iAnnee);
        if (!$oKeydate) $this->forward404();
        $this->Keydate = $oKeydate;
    }
}
