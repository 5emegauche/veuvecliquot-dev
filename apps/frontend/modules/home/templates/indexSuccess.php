<?php
function strip_tags_content ($text, $tags = '', $invert = FALSE) {
    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);

    if (is_array($tags) and count($tags) > 0) {
        if ($invert == FALSE) {
            return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
        } else {
            return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
        }
    } elseif ($invert == FALSE) {
        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
}

use_stylesheet('home.css');
$oMyLinkgrabber = new Linkgrabber();
?>
<div class="content">
    <!-- Début Slideshow d'articles -->
    <div class="slideShow articles">
        <ul>
      <?php foreach($Articles as $index => $Article): ?>
      <?php $Article instanceof Actualite; ?>
      <li data-slide="<?php echo $index; ?>">
                <article class="article news">
                    <figure>
                        <?php if ($Article->getCustomReadMore()): ?>
                        <a href="<?php echo $oMyLinkgrabber->replaceDesktopLinks($Article->getCustomReadMore(), $sf_user->getCulture()); ?>" class="link noanchor" target="_blank">
                        <?php else: ?>
                        <a href="#article<?php echo $index; ?>" class="link">
                        <?php endif; ?>
                          <img src="<?php echo $Article->getImage(); ?>" alt="" draggable="false" />
                        </a>
                    </figure>
                    <h4><?php echo $Article->getCategorie(); ?></h4>
                    <hr />
                    <h3><?php echo html_entity_decode($Article->getTitre()); ?></h3>
                    <?php if ($Article->getCustomReadMore()): ?>
                    <a href="<?php echo $oMyLinkgrabber->replaceDesktopLinks($Article->getCustomReadMore(), $sf_user->getCulture()); ?>" class="link noanchor" target="_blank"><?php if ($Article->getCustomLabelReadMore()): ?><?php echo $Article->getCustomLabelReadMore(); ?><?php else: ?><?php echo __('Read more'); ?><?php endif; ?></a>
                    <?php else: ?>
                    <a href="#article<?php echo $index; ?>" class="link"><?php if ($Article->getCustomLabelReadMore()): ?><?php echo $Article->getCustomLabelReadMore(); ?><?php else: ?><?php echo __('Read more'); ?><?php endif; ?></a>
                    <?php endif; ?>
                </article>
            </li>
      <?php endforeach; ?>
    </ul>
    </div>
    <!-- Fin Slideshow d'articles -->


  <?php foreach($Articles as $index => $Article): ?>
  <?php $Article instanceof Actualite; ?>
  <article class="article text-overflow">
        <figure>
          <?php if ($Article->getVideo()): ?>
          <iframe type="text/html" width="100%" height="100%" src="<?php echo str_replace('/v/', '/embed/', $Article->getVideo()); ?>" frameborder="0"></iframe>
          <?php else: ?>
          <img src="<?php echo $Article->getBlocImage(); ?>" />
          <?php endif; ?>
        </figure>
        <h4><?php echo $Article->getCategorie(); ?></h4>
        <h3><?php echo html_entity_decode($Article->getBlocTitre()); ?></h3>
        <hr />
        <div class="overflow">
      <?php echo $oMyLinkgrabber->replaceDesktopLinks(str_replace(array('<p></p>', '</p><p>', 'href="/'), array('', '<br /><br />', 'href="http://www.veuve-clicquot.com/'), strip_tags(html_entity_decode($Article->getBlocTexte()),'<p><a>')), $sf_user->getCulture()); ?>

      
      <?php include_partial('common/share', array('fullbloc' => 'no')); ?>
    </div>
    </article>
  <?php endforeach; ?>
</div>
