<?php

/**
 * home actions.
 *
 * @package    veuveclicquot
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class homeActions extends VeuveActions {
    public function executeIndex(sfWebRequest $request) {
        $sLangue = $this->getUser()->getCulture();
        $sPays = strtolower($this->getUser()->getAttribute('pays_iso'));
        
        /*if ($sLangue == 'en') {
            switch($this->getUser()->getAttribute('pays')) {
                case 'japan':
                    $sLangue = 'en-ja';
                    break;
                case 'united-kingdom':
                    $sLangue = 'en-gb';
                    break;
                case 'united-states':
                    $sLangue = 'en-us';
                    break;
                default:
                    break;
            }
        }*/
        
        if (!ImportNews::estAJour($sPays, $sLangue)) ImportNews::mettreAJour($sPays, $sLangue);
        $this->Articles = Doctrine::getTable('Actualite')
                                  ->findBy('lang', $sPays . '-' . $sLangue);
    }
}
