<?php

/**
 * statique actions.
 *
 * @package    veuveclicquot
 * @subpackage statique
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class statiqueActions extends VeuveActions {
    public function executeParser(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');

        $this->getResponse()->setSlot('gtm_section_name', 'statique');

        $sStatiqueName = VeuveTranslator::getStatiqueFromSlug($sPageSlug, $request->getParameter('sf_culture'));
        switch ($sStatiqueName) {
            case VeuveTranslator::STATIQUE_CONTACT:
                $this->forward('statique', 'contact');
                break;
            case VeuveTranslator::STATIQUE_VISITE:
                $this->forward('statique', 'visite');
                break;
            case VeuveTranslator::STATIQUE_PRESSE:
                $this->forward('statique', 'presse');
                break;
            case VeuveTranslator::STATIQUE_STORE_LOCATOR:
                $this->forward('statique', 'storelocator');
                break;
            case VeuveTranslator::STATIQUE_VISITE:
                $this->forward('statique', 'visite');
                break;
            default:
                $this->forward('statique', 'statique');
                break;
        }
    }

    public function executeStorelocator(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'store locator');
        $this->Stores = Doctrine::getTable('StoreLocator')
                                ->createQuery('s')
                                ->orderBy('s.ordre ASC')
                                ->execute();
    }

    public function executeContact(sfWebRequest $request) {
        $this->getResponse()->setSlot('gtm_content_name', 'contact');
        $oFormBuilder = new FormBuilder(sfConfig::get('app_contactform_url'));
        $this->Formulaire = $oFormBuilder->getStructure($request->getParameter('sf_culture'));
        $this->langueSite = Doctrine::getTable('Langue')->findOneBy('code', $request->getParameter('sf_culture'));
        if ($request->isMethod(sfWebRequest::POST)) {
            $this->envoiDonnees = $oFormBuilder->sendData($request->getParameter('sf_culture'), $request);
            $this->donneesPost = $request->getPostParameters();
            $this->logMessage('Retour Curl : ' . $this->envoiDonnees['debug']);
        }
    }

    public function executeVisite(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');
        
        $oPageStatique = Doctrine::getTable('PageStatique')
                                 ->createQuery('p')
                                 ->innerJoin('p.Translation t')
                                 ->where('t.lang = ?', $this->getUser()->getCulture())
                                 ->andWhere('t.slug = ?', $sPageSlug)
                                 ->fetchOne();
        if ($oPageStatique)
            $this->PageStatique = $oPageStatique;

        $this->getResponse()->setSlot('gtm_content_name', 'visite');
        $oFormBuilder = new FormBuilder(sfConfig::get('app_visiteform_url'));
        $this->Formulaire = $oFormBuilder->getStructure($request->getParameter('sf_culture'));
        $this->langueSite = Doctrine::getTable('Langue')->findOneBy('code', $request->getParameter('sf_culture'));
        if ($request->isMethod(sfWebRequest::POST)) {
            $this->envoiDonnees = $oFormBuilder->sendData($request->getParameter('sf_culture'), $request);
            $this->donneesPost = $request->getPostParameters();
            $this->logMessage('Retour Curl : ' . $this->envoiDonnees['debug']);
        }
    }

    public function executeStatique(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');
        $this->getResponse()->setSlot('gtm_content_name', $sPageSlug);

        $oPageStatique = Doctrine::getTable('PageStatique')
                                 ->createQuery('p')
                                 ->innerJoin('p.Translation t')
                                 ->where('t.lang = ?', $this->getUser()->getCulture())
                                 ->andWhere('t.slug = ?', $sPageSlug)
                                 ->fetchOne();

        if (!$oPageStatique) {
            $this->forward404();
        }
        $this->PageStatique = $oPageStatique;
    }

    public function executeNotFound(sfWebRequest $request)  {

    }
}
