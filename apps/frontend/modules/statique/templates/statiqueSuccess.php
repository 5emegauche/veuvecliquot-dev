<?php $PageStatique instanceof PageStatique; ?>
<span class="ariane">&nbsp;</span>
<h1><?php echo $PageStatique->getTitre(); ?></h1>
<div class="content static <?php echo $PageStatique->getPageId(); ?>">
  <?php echo html_entity_decode($PageStatique->getTexte()); ?>
  <?php include_partial('common/share'); ?>
  <?php $aBlocsPush = AssocPageStatiqueBlocPush::getBlocsTries($PageStatique->getId()); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>