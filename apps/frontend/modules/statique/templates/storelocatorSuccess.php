<?php $sImage = null; ?>
<span class="ariane">&nbsp;</span>
<h1><?php echo __('store locator'); ?></h1>
<div class="content">
  <ul class="articles-list clearfix">
    <?php foreach($Stores as $index => $oStore): ?>
    <?php $oStore instanceof StoreLocator; ?>
    <?php if (!$sImage) $sImage = $oStore->getUrlImage(); ?>
    <li<?php echo ($index == 0)?' class="large"':''; ?>>
      <article class="store">
        <figure>
          <img src="<?php echo $oStore->getUrlImage(); ?>" alt="store" >
        </figure>
        <h3><?php echo $oStore->getVille(); ?></h3>
        <hr>
        <address><?php echo $oStore->getNom(); ?><br>
        <?php echo $oStore->getAdresse(); ?>,<br>
        <?php echo $oStore->getCodePostal(); ?> <?php echo $oStore->getVille(); ?>, <?php echo $oStore->getPays(); ?></address>
      </article>
    </li>
    <?php endforeach; ?>
  </ul>
  <!-- Partage -->
  <?php include_component('common', 'share'); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocStatiqueBlocPush::getBlocsTries(VeuveTranslator::STATIQUE_STORE_LOCATOR); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>