<span class="ariane">&nbsp;</span>
<h1><?php echo ($PageStatique)?$PageStatique->getTitre():'visit'; ?></h1>

<?php $aChampsIgnores = array('address_2', 'nationality', 'fax'); ?>

<?php if (isset($envoiDonnees)): ?>
<?php if ($envoiDonnees['retour'] == 'ok'): ?>
<script type="text/javascript">
alert('<?php echo addcslashes($envoiDonnees['message'], '\''); ?>'); //'
</script>
<?php $aChampsErreur = array(); ?>
<?php $aValeursChamps = array(); ?>
<?php else:
$aChampsErreur = $envoiDonnees['champs']->getRawValue();
$aValeursChamps = $donneesPost->getRawValue();
 endif; ?>
<?php endif; ?>

<?php if ($PageStatique): ?>
<div class="content static <?php echo $PageStatique->getPageId(); ?>">
  <?php echo html_entity_decode($PageStatique->getTexte()); ?>
</div>
<?php endif; ?>

<form action="?visitform=submitted" method="post">
  <span id="champrequis">* <?php echo __('Required field'); ?></span>
  <?php 
  foreach($Formulaire as $Champ):
    if (in_array($Champ['nom'], $aChampsIgnores)):
      // Le champ en cours doit être ignoré, on passe au suivant
      continue;
    endif;
    if ($Champ['nom'] == 'choice'):
  ?>
  <hr class="form_spacer" />
  <h2><?php echo __('Your Visit'); ?></h2>
  <?php
    elseif ($Champ['nom'] == 'civility'):
  ?>
  <hr class="form_spacer" />
  <h2><?php echo __('Your Details'); ?></h2>
  <?php
    endif;
    switch ($Champ['typec']):
      case 'hidden':
  ?>
  <input type="hidden" name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>" value="<?php echo ($Champ['nom'] == 'site_lang')?$langueSite->getNomWebservice():$Champ['valeur']; ?>" />
  <?php
        break;
      case 'number':
      case 'textfield':
        // Champ texte
  ?>
  <?php if(isset($aValeursChamps[$Champ['nom']])): ?>
  <input type="<?php echo ($Champ['nom'] == 'email')?'text':'text'; ?>" name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>" value="<?php echo $aValeursChamps[$Champ['nom']]; ?>" />
  <?php else: ?>
  <input type="<?php echo ($Champ['nom'] == 'email')?'text':'text'; ?>" name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>" value="<?php echo $Champ['label']; ?><?php echo ($Champ['obligatoire']=='1')?'*':''; ?>" />
  <?php endif; ?>
  <?php 
        break;
      case 'textarea':
        // Zone de texte
  ?>
  <textarea name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>"><?php if(isset($aValeursChamps[$Champ['nom']])): ?><?php echo $aValeursChamps[$Champ['nom']]; ?><?php else: ?><?php echo $Champ['label']; ?><?php echo ($Champ['obligatoire']=='1')?'*':''; ?><?php endif; ?></textarea>
  <?php
        break;
      case 'select':
        // Liste déroulante
        if ($Champ['aslist'] == '1'):
  ?>
  <select name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>">
    <option value="" disabled <?php if(!isset($aValeursChamps[$Champ['nom']])): ?>selected<?php endif; ?>><?php echo $Champ['label']; ?><?php echo ($Champ['obligatoire']=='1')?'*':''; ?></option>
    <?php $aChoix = explode("\r\n", $Champ['items']); ?>
    <?php foreach($aChoix as $sChoix): ?>
    <?php $aSepChoix = explode('|', $sChoix); ?>
    <option value="<?php echo $aSepChoix[0]; ?>" <?php if((isset($aValeursChamps[$Champ['nom']]))&&($aValeursChamps[$Champ['nom']] == $aSepChoix[0])): ?>selected<?php endif; ?>><?php echo $aSepChoix[1]; ?></option>
    <?php endforeach; ?>
  </select>
  <?php
        elseif ($Champ['nom'] == 'optin1'):
          $aChoix = explode("\r\n", $Champ['items']);
          $aValeur = explode('|', $aChoix[0]);
  ?>
  <input type="hidden" name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>" value="<?php echo $aValeur[0]; ?>" />
  <?php
        else:
          $aChoix = explode("\r\n", $Champ['items']);
          $aValeur = explode('|', $aChoix[0]);
  ?>
  <input type="checkbox" name="<?php echo $Champ['nom']; ?>" id="<?php echo $Champ['nom']; ?>" value="<?php echo $aValeur[0]; ?>" />&nbsp;
  <?php 
          switch($Champ['nom']):
            case 'optin2':
              echo __('Please do not send me information about products or services offered by other entities of the Moët Hennessy group');
              break;
            default:
              break;
          endswitch;
        endif;
        break;
    endswitch;
  endforeach;
  ?>
  <p class="static"><?php echo __('The personal data collected though this form may be used by Veuve Clicquot for the purposes of managing our relationship with you and customizing and personalizing our service and our marketing communications. We may share them with other entities of the Moet Hennessey group as further described in our <a href="/en/private-policy">privacy policy</a>.</p>
  <p>You have a right to access the personal data that is held about you, to correct any mistakes or to object to the processing of such data. If you wish to exercise any of the above rights, please contact us at: <a href="mailto:communication@veuve-clicquot.fr">communication@veuve-clicquot.fr</a> or write to Veuve Clicquot 12 rue du temple 51100 Reims, France.'); ?></p>
  <input type="submit" value="<?php echo __('Send'); ?>" onclick="FiftyFiveTrack({ sectionName: sSectionName, contentName: sContentName, countryCode: sCountryCode, languageCode: sLanguageCode, EventAction: 'Visit Form' });" />
</form>

<?php if (count($aChampsErreur)): ?>
<?php slot('additional_js_form'); ?>
<script type="text/javascript">
jQuery('#champrequis').addClass('erreurchamp');
<?php foreach($aChampsErreur as $sChampErreur): ?>
jQuery('#<?php echo $sChampErreur; ?>').addClass('erreurchamp');
<?php endforeach; ?>
</script>
<?php end_slot(); ?>
<?php endif; ?>