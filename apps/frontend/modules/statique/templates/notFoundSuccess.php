<div class="notFound">
  <img src="/images/content/404.jpg" alt="not found">
  <p><?php echo __('Désolé, ce que vous recherchez n\'existe pas chez Veuve-Clicquot.'); ?></p>
  <a href="<?php echo url_for('@homepage'); ?>" class="link"><?php echo __('Retour a la page d\'accueil'); ?></a>
</div>