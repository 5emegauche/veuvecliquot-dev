<?php

/**
 * champagne actions.
 *
 * @package    veuveclicquot
 * @subpackage champagne
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class champagneActions extends VeuveActions {
    public function executeParser(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');
        // Détermination type de page
        $oFicheProduit = Doctrine::getTable('FicheProduit')
                                 ->createQuery('fp')
                                 ->innerJoin('fp.Translation t')
                                 ->where('t.lang = ?', $request->getParameter('sf_culture'))
                                 ->andWhere('t.slug = ?', $sPageSlug)
                                 ->fetchOne();
        if ($oFicheProduit) {
            $this->forward('champagne', 'ficheproduit');
        }
        else {
            $oOffre = Doctrine::getTable('Offre')
                              ->createQuery('o')
                              ->innerJoin('o.Translation t')
                              ->where('t.lang = ?', $request->getParameter('sf_culture'))
                              ->andWhere('t.slug = ?', $sPageSlug)
                              ->fetchOne();
            if ($oOffre) {
                $this->forward('champagne', 'offres');
            }
            else {
                $this->forward404();
            }
        }
    }
    
    public function executeOffres(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');
        $this->oOffre = Doctrine::getTable('Offre')
                                ->createQuery('o')
                                ->innerJoin('o.Translation t')
                                ->where('t.lang = ?', $request->getParameter('sf_culture'))
                                ->andWhere('t.slug = ?', $sPageSlug)
                                ->fetchOne();
        $this->getResponse()->setSlot('gtm_content_name', 'Offres : ' . $sPageSlug);
        $this->getResponse()->setSlot('gtm_product_name', $sPageSlug);
    }
        
    public function executeFicheproduit(sfWebRequest $request) {
        $sPageSlug = $request->getParameter('pageslug');
        $oFicheProduit = Doctrine::getTable('FicheProduit')
                                       ->createQuery('fp')
                                       ->innerJoin('fp.Translation t')
                                       ->where('t.lang = ?', $request->getParameter('sf_culture'))
                                       ->andWhere('t.slug = ?', $sPageSlug)
                                       ->fetchOne();
        
        $this->getResponse()->setSlot('gtm_content_name', 'Fiche produit : ' . $sPageSlug);
        $this->getResponse()->setSlot('gtm_product_name', $sPageSlug);

        $oFicheProduit instanceof FicheProduit;
        if ($oFicheProduit->getIdproduit() == FicheProduit::PRODUIT_CAVE_PRIVEE)
            $this->setTemplate('caveprivee');
        $this->oFicheProduit = $oFicheProduit;
    }
}
