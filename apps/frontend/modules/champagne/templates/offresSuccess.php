<?php 
$sPays = $sf_user->getAttribute('pays_iso');
$sLangue = $sf_user->getCulture();
?>
<?php $sImage = null; ?>
<span class="ariane">Champagne</span>
<h1><?php echo $oOffre->getTitre(); ?></h1>
<div class="content">
  <video id="video" controls="controls" style="display:none;position:absolute;">
    <source src="">
  </video>
  <!-- Blocs offres -->
  <ul class="articles-list clearfix">
    <?php 
    foreach($oOffre->getBlocsTries() as $index => $oBloc):
      $oBloc instanceof BlocOffre;
      if (!$sImage) $sImage = $oBloc->getUrlImage();
    ?>
    <li<?php echo (in_array($oBloc->getTypeBloc(), array(Bloc::BLOCTYPE_DEUXIMAGES, Bloc::BLOCTYPE_UNEIMAGELARGE)))?' class="large"':''; ?>>
    <?php if ($oBloc->getAncre()): ?>
    <a name="<?php echo $oBloc->getAncre(); ?>"></a>
    <?php endif; ?>
    <?php
      switch($oBloc->getTypeBloc()):
        case Bloc::BLOCTYPE_UNEIMAGE:
        case Bloc::BLOCTYPE_UNEIMAGELARGE:
          $aParams = array(
              'shop'              => 'non', 
              'image_src'         => $oBloc->getUrlImage(),
              'titre'             => $oBloc->getTitre(),
              'texte'             => $oBloc->getTexte(),
              'url_shop'          => '',
              'wrdSlide'          => $oBloc->getSlider(),
              'overflow'          => 'oui',
              'ecommerce'         => $oBloc->canDisplayEcommerce($sPays, $sLangue),
              'lienecommerce'     => $oBloc->getLienEcommerce($sPays, $sLangue),
          );
          include_partial('article/uneimage', $aParams);
          break;
        case Bloc::BLOCTYPE_DEUXIMAGES:
          $aParams = array(
              'shop'              => 'non', 
              'image_src'         => $oBloc->getUrlImage(),
              'image2_src'        => $oBloc->getUrlImage2(),
              'titre'             => $oBloc->getTitre(),
              'texte'             => $oBloc->getTexte(),
              'url_shop'          => '',
              'wrdSlide'          => $oBloc->getSlider(),
              'overflow'          => 'oui',
              'ecommerce'         => $oBloc->canDisplayEcommerce($sPays, $sLangue),
              'lienecommerce'     => $oBloc->getLienEcommerce($sPays, $sLangue),
          );
          include_partial('article/deuximages', $aParams);
          break;
        case Bloc::BLOCTYPE_VIDEO:
          $aParams = array(
              'shop'              => 'non', 
              'video_preview_src' => $oBloc->getUrlImage(),
              'video_src'         => $oBloc->getUrlVideo(),
              'titre'             => $oBloc->getTitre(),
              'texte'             => $oBloc->getTexte(),
              'url_shop'          => '',
              'wrdSlide'          => $oBloc->getSlider(),
              'overflow'          => 'oui',
              'ecommerce'         => $oBloc->canDisplayEcommerce($sPays, $sLangue),
              'lienecommerce'     => $oBloc->getLienEcommerce($sPays, $sLangue),
          );
          include_partial('article/video', $aParams);
          break;
      endswitch;
    ?>
    </li>
    <?php 
    endforeach; 
    ?>
  </ul>
  <!-- Partage -->
  <?php include_component('common', 'share', array('image' => $sImage)); ?>
  <!-- Pushs -->
  <?php $aBlocsPush = AssocOffreBlocPush::getBlocsTries($oOffre->getId()); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>