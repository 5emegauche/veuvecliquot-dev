<?php $oFicheProduit instanceof FicheProduit; ?>
<?php 
$sPays = $sf_user->getAttribute('pays_iso');
$sLangue = $sf_user->getCulture();
?>
<ul class="caves">
  <?php foreach($oFicheProduit->getBlocsTries() as $index => $oBlocFicheProduit): ?>
  <?php $oBlocFicheProduit instanceof BlocFicheProduit; ?>
  <li>
    <?php if ($index == 0): ?>
    <span class="ariane">&nbsp;</span>
    <h1><?php echo __('Cave Privée'); ?></h1>
    <?php endif; ?>
    <article<?php echo ($oBlocFicheProduit->getAncre())?' id="'.$oBlocFicheProduit->getAncre().'"':''; ?>>
      <h2><?php echo html_entity_decode($oBlocFicheProduit->getTitre()); ?></h2>
      <h3><?php echo html_entity_decode($oBlocFicheProduit->getSousTitre()); ?></h3>
    <figure>
      <img src="/images/content/champagne/cave/cave<?php echo $index + 1; ?>.png" alt="" />
    </figure>
    
    <div class="article-content">
      <?php echo html_entity_decode($oBlocFicheProduit->getContenu()); ?>
      </div>
      <?php if ((0) && ($oBlocFicheProduit->getBoutonShop())): ?>
      <div class="shop-bt">
        <a href="<?php echo $oBlocFicheProduit->getLienBoutonShop(); ?>"><?php echo __('Shop online'); ?></a>
      </div>
      <?php endif; ?>
      <?php if ($oBlocFicheProduit->canDisplayEcommerce($sPays, $sLangue)): ?>
      <form action="<?php echo $oBlocFicheProduit->getLienEcommerce($sPays, $sLangue); ?>" target="_blank">
        <p class="shop-block">
          <span class="shop-action">
            <input type="submit" class="ecommerce-button" name="" value="<?php echo __('Shop now'); ?>" onclick="FiftyFiveTrack({ event: 'ecommerceButton', productName: '<?php echo addslashes(strip_tags(html_entity_decode($oBlocFicheProduit->getTitre();))); ?>' });" />
          </span>
        </p>
      </form>
      <?php endif; ?>
    </article>
  </li>
  <?php endforeach; ?>
</ul>
<div class="content">
  <?php if(0): ?>
  <article class="article">
    <figure>
      <img src="<?php echo $oFicheProduit->getUrlImageNote(); ?>" alt="" draggable="false" />
    </figure>
    <div class="slideShow">
      <ul>
        <?php foreach($oFicheProduit->getNotesTriees() as $index => $oNoteFicheProduit): ?>
        <?php $oNoteFicheProduit instanceof NoteFicheProduit; ?>
        <li data-slide="<?php echo $index; ?>">
          <h5><?php echo $oNoteFicheProduit->getNoteVin(); ?></h5>
          <h4><?php echo $oNoteFicheProduit->getTitre(); ?></h4>
          <hr />
          <?php echo html_entity_decode($oNoteFicheProduit->getTexte()); ?>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </article>
  <?php endif; ?>
  <?php include_component('common', 'share', array('image' => '/images/content/champagne/cave/cave1.png')); ?>
  <?php $aBlocsPush = AssocFicheProduitBlocPush::getBlocsTries($oFicheProduit->getId()); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>
