<?php $oFicheProduit instanceof FicheProduit; ?>
<div class="<?php echo $oFicheProduit->getIdproduit(); ?>">
  <span class="ariane">&nbsp;</span>
  <h1><?php echo $oFicheProduit->getTitre(); ?></h1>
</div>
<div class="product <?php echo $oFicheProduit->getIdproduit(); ?>">
  <article class="clearfix">
    <figure id="<?php echo $oFicheProduit->getIdproduit(); ?>"><img src="<?php echo $oFicheProduit->getUrlImage(); ?>" alt="<?php echo $oFicheProduit->getTitre(); ?>" /></figure>
    <div class="text">
      <ul>
        <?php foreach($oFicheProduit->getBlocsTries() as $oBlocFicheProduit): ?>
        <?php $oBlocFicheProduit instanceof BlocFicheProduit; ?>
        <li>
          <?php if ($oBlocFicheProduit->getAncre()): ?>
          <a name="<?php echo $oBlocFicheProduit->getAncre(); ?>"></a>
          <?php endif; ?>
          <?php echo html_entity_decode($oBlocFicheProduit->getContenu()); ?>
        </li>
        <?php if ((0) && ($oBlocFicheProduit->getBoutonShop())): ?>
        <div class="shop-bt">
          <a href="<?php echo $oBlocFicheProduit->getLienBoutonShop(); ?>"><?php echo __('Shop online'); ?></a>
        </div>
        <?php endif; ?>
        <?php 
        $sPays = $sf_user->getAttribute('pays_iso');
        $sLangue = $sf_user->getCulture();
        ?>
        <?php if ($oBlocFicheProduit->canDisplayEcommerce($sPays, $sLangue)): ?>
        <form action="<?php echo $oBlocFicheProduit->getLienEcommerce($sPays, $sLangue); ?>" target="_blank">
          <p class="shop-block">
            <span class="shop-action">
              <input type="submit" class="ecommerce-button" name="" value="<?php echo __('Shop now'); ?>" onclick="FiftyFiveTrack({ event: 'ecommerceButton', productName: '<?php echo addslashes(strip_tags(html_entity_decode($oFicheProduit->getTitre();))); ?>' });" />
            </span>
          </p>
        </form>
        <?php endif; ?>
        <?php endforeach; ?>
      </ul>
    </div>
  </article>
</div>
<div class="content">
  
  <?php if((trim($oFicheProduit->getUrlImageNote()) != '/uploads/assets/noteproduit/') && (count($oFicheProduit->getNotesTriees() > 0))): ?>
  <article class="article">
    <figure>
      <img src="<?php echo $oFicheProduit->getUrlImageNote(); ?>" alt="" draggable="false" />
    </figure>
    <div class="slideShow">
      <ul>
        <?php foreach($oFicheProduit->getNotesTriees() as $index => $oNoteFicheProduit): ?>
        <?php $oNoteFicheProduit instanceof NoteFicheProduit; ?>
        <li data-slide="<?php echo $index; ?>">
          <h5><?php echo $oNoteFicheProduit->getNoteVin(); ?></h5>
          <h4><?php echo $oNoteFicheProduit->getTitre(); ?></h4>
          <hr />
          <?php echo html_entity_decode($oNoteFicheProduit->getTexte()); ?>
        </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </article>
  <?php endif; ?>
  
  <?php include_component('common', 'share', array('image' => $oFicheProduit->getUrlImage())); ?>
  <?php $aBlocsPush = AssocFicheProduitBlocPush::getBlocsTries($oFicheProduit->getId()); ?>
  <?php $aBlocs = array(); ?>
  <?php foreach($aBlocsPush as $oBlocPush): ?>
  <?php $aBlocs[] = $oBlocPush->getBlocPush(); ?>
  <?php endforeach; ?>
  <?php include_component('common', 'push', array('blocs' => $aBlocs)); ?>
</div>
