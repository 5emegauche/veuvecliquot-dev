<?php

class VeuveActions extends sfActions {
    public function isSocialBot($sUserAgent) {
        if (strstr($sUserAgent, 'ipMonitor')) return true;
        if (strstr($sUserAgent, 'facebookexternalhit')) return true;
        if (strstr($sUserAgent, 'Googlebot')) return true;
        return false;
    }
    
    public function preExecute() {
        if (!($this->isSocialBot($_SERVER['HTTP_USER_AGENT']))) {
            if (sfConfig::get('app_environnement_redirection', 1)) {
                $oMobileDetect = new MobileDetect();
                if ((!$oMobileDetect->isMobile()) && (!$oMobileDetect->isTablet())) {
                    $this->redirect(Redirector::getRedirection($_SERVER['REQUEST_URI']));
                }
            } 
            if (!$this->getUser()->isAuthenticated()) {
                if ($this->getRequest()->getParameter('sf_culture')) {
                    $this->getUser()->setCulture($this->getRequest()->getParameter('sf_culture'));
                }
                $this->getUser()->setAttribute('redirection', $this->getContext()->getRouting()->getCurrentInternalUri());
                $this->redirect('@prehome');
            }
            else {
                if (!isset($_COOKIE['accept_cookie'])) {
                    $this->getResponse()->setCookie('accept_cookie', true, (time() + 13 * 30 * 86400));
                }
            }
        }
        if ($this->isSocialBot($_SERVER['HTTP_USER_AGENT'])) {
            if ($this->getRequest()->getParameter('sf_culture')) {
                $this->getUser()->setCulture($this->getRequest()->getParameter('sf_culture'));
            }
            else {
                $this->getUser()->setCulture('fr');
            }
            $this->getUser()->setAttribute('pays', 'france');
            $this->getUser()->setAttribute('pays_iso', 'FR');
        }
        if ($this->getUser()->getAttribute('pays', null) && !$this->getUser()->getAttribute('pays_iso', null)) {
            $oPays = Doctrine::getTable('Pays')->findOneBy('slug', $this->getUser()->getAttribute('pays', null));
            $this->getUser()->setAttribute('pays_iso', strtoupper($oPays->getCodeiso()));
        }
    }
}