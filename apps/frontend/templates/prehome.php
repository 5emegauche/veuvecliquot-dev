<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Veuve Clicquot</title>
    <meta name="description" content="" />
    <meta name="viewport" content="initial-scale=1.0" />
    <link rel="stylesheet" href="/css/main.css" />
    <link rel="stylesheet" href="/css/prehome.css" />
    <script src="/js/tagger.js" type="text/javascript"></script>
    <script type="text/javascript">
      var sSectionName = 'Prehome';
      var sContentName = 'Prehome';
      var sPageName = 'Prehome';
      var sCountryCode = '';
      var sLanguageCode = '';
      var oTagging = { sectionName: sSectionName, contentName: sContentName, pageName: sPageName, countryCode: sCountryCode, languageCode: sLanguageCode };
      FiftyFiveTrack(oTagging);
    </script>
    <script src="/js/gtmCall.js" type="text/javascript"></script>
  </head>
  <body class="prehome">
    <?php echo $sf_content; ?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
    <script src="/js/plugins.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/prehome.js"></script>
    <?php /*<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      var _gaq=[['_setAccount','UA-18404490-2'],['_trackPageview']];
      _gaq.push(['_setDomainName', 'veuve-clicquot.com']);
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>*/ ?>
    <!--
      Ghostery Inc tag
      cid: 3469
      pid: <?php echo __('GHOSTERY_TAG_PID'); ?>
    -->
    <script type=""text/javascript"">
    (function() {
      var hn = document.createElement('script'); hn.type = 'text/javascript'; hn.async = true; hn.setAttribute('data-ev-hover-pid', <?php echo __('GHOSTERY_TAG_PID'); ?>); hn.setAttribute('data-ev-hover-ocid', 3469);
      hn.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'c.betrad.com/geo/h1.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(hn, s);
    })();
    </script>
  </body>
</html>