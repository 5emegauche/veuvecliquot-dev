<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Veuve Clicquot</title>
    <meta name="description" content="">
    <meta name="viewport" content="initial-scale=1.0">
    <meta property="og:title" content="<?php echo (has_slot('ogtitle'))?get_slot('ogtitle'):'Veuve Clicquot'; ?>" />
    <meta property="og:description" content="<?php echo (has_slot('ogdescription'))?get_slot('ogdescription'):'Veuve Clicquot'; ?>" />
    <meta property="og:image" content="http://<?php echo $_SERVER['SERVER_NAME']; ?><?php echo (has_slot('ogimage'))?get_slot('ogimage'):'/uploads/assets/ficheproduit/ficheproduit-1-yellow-label.jpg' ?>" />
    <?php include_stylesheets(); ?>
    <script src="/js/tagger.js" type="text/javascript"></script>
    <script type="text/javascript">
      var sSectionName = '<?php include_slot("gtm_section_name"); ?>';
      var sContentName = '<?php include_slot("gtm_content_name"); ?>';
      var sPageName = '';
      var sCountryCode = '<?php echo $sf_user->getAttribute("pays_iso"); ?>';
      var sLanguageCode = '<?php echo $sf_user->getCulture(); ?>';
      var videoLastEvent = null;

      <?php if (has_slot("gtm_product_name")): ?>
      var sProductName = '<?php include_slot("gtm_product_name"); ?>';
      var oTagging = { sectionName: sSectionName, contentName: sContentName, pageName: sPageName, countryCode: sCountryCode, productName: sProductName, languageCode: sLanguageCode };
      <?php else: ?>
      var oTagging = { sectionName: sSectionName, contentName: sContentName, pageName: sPageName, countryCode: sCountryCode, languageCode: sLanguageCode };
      <?php endif; ?>
      FiftyFiveTrack(oTagging);

    </script>
    <script src="/js/gtmCall.js" type="text/javascript"></script>
  </head>
  <body id="<?php include_slot("id_page"); ?>">
    <?php $oPays = Doctrine::getTable('Pays')->findOneBy('slug', $sf_user->getAttribute('pays')); ?>
    <?php $aLiensInterdits = Lien::getLiensInterdits($sf_user->getAttribute('pays')); ?>
    <?php $aLiens = Lien::getLiensInternes($sf_user->getCulture()); ?>
    <?php include_component('common', 'header', array('liens' => $aLiens, 'liensinterdits' => $aLiensInterdits, 'pays' => $oPays)); ?>
    <?php echo $sf_content; ?>
    <?php include_component('common', 'footer', array('liens' => $aLiens, 'liensinterdits' => $aLiensInterdits, 'pays' => $oPays)); ?>

    <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script-->
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-1.8.2.min.js"><\/script>')</script>
    <script src="/js/plugins.js" type="text/javascript"></script>
    <script src="/js/main.js" type="text/javascript"></script>

    <script type="text/javascript" src="/js/pinimages.js"></script>
    <script>
    var sPageName = '',_isLinkClicked_bool = false;
    var aHashScrolle = new Array();

    function inArray(needle, haystack) {
      var length = haystack.length;
      for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
      }
      return false;
    }

    $(function(){
      $('.slideShow').on('click','.link',function(e){
        _isLinkClicked_bool = true;
        setTimeout(function(){_isLinkClicked_bool=false;},1500);
        var id = $(e.currentTarget).attr('href').replace('#','');
        var article  = $('[data-anchor="'+id+'"]')
        sPageName = '';
        setTag(article,'shortcutMenu');

      });
      $(window).on('scroll',function(){
        if(_isLinkClicked_bool)return;
        var id = -1;
        $('article').each(function(index){
          if($(this).offset().top - $(window).scrollTop() > 0 && id === -1){
            id = index - 1;
          }
        });
        if(id !== -1){
          var article  = $('article').eq(id);
          if(article.parents('.slideShow').length){
            article = article.parents('.slideShow').find('article').eq(1);
          }
          setTag(article,'scrollPage');
        }
      });
    });
    function setTag(article,tag){

      if(article.get(0)){
        if(article.find('h3').get(0)){
          if(sPageName !== article.find('h3').text()){

            sPageName = article.find('h3').text();


            if (!inArray(sPageName, aHashScrolle)) {
              var newTag = {}
              newTag = { event:tag, sectionName: sSectionName, contentName: sContentName, pageName: sPageName, countryCode: sCountryCode, languageCode: sLanguageCode };
              aHashScrolle.push(sPageName);
              FiftyFiveTrack(newTag);

            }

          }
        }
      }
    }
    </script>
    <script type="text/javascript" src="/js/contact.js"></script>

    <?php /*<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      var _gaq=[['_setAccount','UA-18404490-2'],['_trackPageview']];
      _gaq.push(['_setDomainName', 'veuve-clicquot.com']);
      (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
      g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
      s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>*/ ?>

    <?php if (has_slot('additional_js_form')):
      include_slot('additional_js_form'); 
    endif; ?>
  </body>
</html>
