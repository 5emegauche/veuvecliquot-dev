<?php

class veuveRefreshthumbnailsTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'veuve';
    $this->name             = 'refreshthumbnails';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [veuve:refreshthumbnails|INFO] task does things.
Call it with:

  [php symfony veuve:refreshthumbnails|INFO]
EOF;
  }
  
  private function resizeImage($sChemin, $sCheminRetina) {
      if (file_exists($sCheminRetina) && is_file($sCheminRetina)) {
          $oImg = new sfImage($sCheminRetina);
          $aDimensions = array($oImg->getWidth(), $oImg->getHeight());
          $oImg->resize($aDimensions[0] / 2, $aDimensions[1] / 2);
          $oImg->setQuality(100);
          $oImg->saveAs($sChemin);
          unset($oImg);
      }
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here
    $aBlocs = Doctrine::getTable('Bloc')->findAll();
    $sPath = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_offres');
    foreach($aBlocs as $oBloc) {
        $oBloc instanceof Bloc;
        
        $sCheminFichier = $sPath . $oBloc->getImage();
        $sCheminFichierRetina = $sPath . str_replace('.', '@2x.', $oBloc->getImage());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
        
        $sCheminFichier = $sPath . $oBloc->getImage2();
        $sCheminFichierRetina = $sPath . str_replace('.', '@2x.', $oBloc->getImage2());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
    }
    
    $aBlocsPushs = Doctrine::getTable('BlocPush')->findAll();
    $sPath = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_blocpush');
    foreach($aBlocsPushs as $oBlocPush) {
        $oBlocPush instanceof BlocPush;
        
        $sCheminFichier = $sPath . $oBlocPush->getImage();
        $sCheminFichierRetina = $sPath . str_replace('.', '@2x.', $oBlocPush->getImage());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
    }

    $aDatesClefs = Doctrine::getTable('DateClef')->findAll();
    $sPath = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_dateclef');
    foreach($aDatesClefs as $oDateClef) {
        $oDateClef instanceof DateClef;
        
        $sCheminFichier = $sPath . $oDateClef->getImage();
        $sCheminFichierRetina = $sPath . str_replace('.', '@2x.', $oDateClef->getImage());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
    }
    
    $aFichesProduits = Doctrine::getTable('FicheProduit')->findAll();
    $sPath = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_ficheproduit');
    $sPathNote = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_noteproduit');
    foreach($aFichesProduits as $oFicheProduit) {
        $oFicheProduit instanceof FicheProduit;
        
        $sCheminFichier = $sPath . $oFicheProduit->getImage();
        $sCheminFichierRetina = $sPath . str_replace('.', '@2x.', $oFicheProduit->getImage());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
        
        $sCheminFichier = $sPathNote . $oFicheProduit->getImageNote();
        $sCheminFichierRetina = $sPathNote . str_replace('.', '@2x.', $oFicheProduit->getImageNote());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
    }
    
    $aWomens = Doctrine::getTable('WomenOfInterest')->findAll();
    $sPath = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_women');
    foreach($aWomens as $oWoman) {
        $oWoman instanceof WomenOfInterest;
        
        $sCheminFichier = $sPath . $oWoman->getPhoto();
        $sCheminFichierRetina = $sPath . str_replace('.', '@2x.', $oWoman->getPhoto());
        $this->resizeImage($sCheminFichier, $sCheminFichierRetina);
    }
  }
}
