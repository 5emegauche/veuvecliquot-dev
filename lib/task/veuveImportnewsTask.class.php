<?php

class veuveImportnewsTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'veuve';
    $this->name             = 'importnews';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [veuve:importnews|INFO] task does things.
Call it with:

  [php symfony veuve:importnews|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here
    $aLangues = array('en', 'de', 'es', 'fr', 'it', 'ja', 'ko', 'pt', 'en-ja', 'en-us', 'en-gb');
    foreach($aLangues as $sLangue) {
        $oCURLSession = curl_init('http://www.veuve-clicquot.com/services/vcp/news');
        curl_setopt($oCURLSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($oCURLSession, CURLOPT_POST, true);
        curl_setopt($oCURLSession, CURLOPT_POSTFIELDS, array('lang' => $sLangue));
        curl_setopt($oCURLSession, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        if (sfConfig::get('app_contactform_htuser') && sfConfig::get('app_contactform_htpass'))
            curl_setopt($oCURLSession, CURLOPT_USERPWD, sfConfig::get('app_contactform_htuser') . ':' . sfConfig::get('app_contactform_htpass'));
        $sRetourCurl = curl_exec($oCURLSession);
        if ($sRetourCurl === false) {
            // Erreur récupération XML
        }
        else {
            Doctrine::getTable('Actualite')
                    ->createQuery('a')
                    ->delete()
                    ->where('a.lang = ?', $sLangue)
                    ->andWhere('a.custom_read_more IS NULL')
                    ->execute();
            file_put_contents(sfConfig::get('sf_data_dir') . DIRECTORY_SEPARATOR . 'news_' . time() . '.xml', $sRetourCurl);
            $oStructureXML = simplexml_load_string($sRetourCurl);
            foreach($oStructureXML->item as $oItem) {
                $oActualite = Doctrine::getTable('Actualite')
                                      ->createQuery('a')
                                      ->where('a.titre = ?', (string)$oItem->title)
                                      ->andWhere('a.lang = ?', $sLangue)
                                      ->fetchOne();
                if (!$oActualite) {
                  $oActualite = new Actualite();
                }
                $oActualite->setCategorie((string)$oItem->category)
                           ->setTitre((string)$oItem->title)
                           ->setTexte((string)$oItem->body)
                           ->setImage((string)$oItem->image)
                           ->setLang($sLangue)
                           ->setBlocTitre((string)$oItem->screen->title)
                           ->setBlocTexte((string)$oItem->screen->text)
                           ->setBlocImage((string)$oItem->image)
                           ->setVideo((string)$oItem->screen->video)
                           ->save();
                $oSliderXML = $oItem->slider;
                $iOrdreSlide = 0;
                if (isset($oSliderXML->item)) {
                    foreach($oSliderXML->item as $oItemSlider) {
                        $oSlide = new ActualiteSlide();
                        $oSlide->setActualiteId($oActualite->getId())
                               ->setOrdre($iOrdreSlide++)
                               ->setImage((string)$oItemSlider->image)
                               ->save();
                    }
                }
            }
        }
        curl_close($oCURLSession);
    }
  }
}
