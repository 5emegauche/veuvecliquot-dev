<?php

class veuveInitordresTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'veuve';
    $this->name             = 'initordres';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [veuve:initordres|INFO] task does things.
Call it with:

  [php symfony veuve:initordres|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here
    // Gestion des blocs articles
    $oBlocsArticles = Doctrine::getTable('Bloc')
                              ->createQuery('b')
                              ->where('b.article_id IS NOT NULL')
                              ->orderBy('b.article_id ASC')
                              ->addOrderBy('b.id ASC')
                              ->execute();
    $iCurrentArticleId = null;
    $iOrdreCourant = 0;
    foreach($oBlocsArticles as $oBlocArticle) {
      if ($iCurrentArticleId != $oBlocArticle->getArticleId()) {
        $iCurrentArticleId = $oBlocArticle->getArticleId();
        $iOrdreCourant = 1;
      }
      $oBlocArticle->setOrdre($iOrdreCourant)->save();
      $iOrdreCourant++;
    }

    // Gestion des blocs offres
    $oBlocsOffres = Doctrine::getTable('Bloc')
                            ->createQuery('b')
                            ->where('b.offre_id IS NOT NULL')
                            ->orderBy('b.offre_id ASC')
                            ->addOrderBy('b.id ASC')
                            ->execute();
    $iCurrentOffreId = null;
    $iOrdreCourant = 0;
    foreach($oBlocsOffres as $oBlocOffre) {
      if ($iCurrentOffreId != $oBlocOffre->getOffreId()) {
        $iCurrentOffreId = $oBlocOffre->getOffreId();
        $iOrdreCourant = 1;
      }
      $oBlocOffre->setOrdre($iOrdreCourant)->save();
      $iOrdreCourant++;
    }

    // Gestion des blocs fiche produit
    $oBlocsFProduits = Doctrine::getTable('BlocFicheProduit')
                               ->createQuery('b')
                               ->orderBy('b.fiche_produit_id ASC')
                               ->addOrderBy('b.id ASC')
                               ->execute();
    $iCurrentFProduitId = null;
    $iOrdreCourant = 0;
    foreach ($oBlocsFProduits as $oBlocFProduit) {
      if ($iCurrentFProduitId != $oBlocFProduit->getFicheProduitId()) {
        $iCurrentFProduitId = $oBlocFProduit->getFicheProduitId();
        $iOrdreCourant = 1;
      }
      $oBlocFProduit->setOrdre($iOrdreCourant)->save();
      $iOrdreCourant++;
    }
  }
}
