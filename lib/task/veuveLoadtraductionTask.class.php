<?php

class veuveLoadtraductionTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'veuve';
    $this->name             = 'loadtraduction';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [veuve:loadtraduction|INFO] task does things.
Call it with:

  [php symfony veuve:loadtraduction|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // add your code here
    $sNomFichier = sfConfig::get('sf_data_dir') . DIRECTORY_SEPARATOR . 'moulinette.csv';
    $oFichier = new SplFileObject($sNomFichier, "r");
    $bFirstLine = false;
    while(!$oFichier->eof()) {
        $aLigne = $oFichier->fgetcsv(";", "\"", "\\");
        if (!$bFirstLine) {
            $bFirstLine = true;
            continue;
        }
        
        switch($aLigne[0]) {
            case 'bloc':
                $iIdentifiantBloc = intval($aLigne[1]);
                if ($iIdentifiantBloc) {
                    $oBloc = Doctrine::getTable('Bloc')
                                     ->createQuery('b')
                                     ->innerJoin('b.Translation t')
                                     ->where('b.id = ?', $iIdentifiantBloc)
                                     ->setHydrationMode(Doctrine::HYDRATE_RECORD)
                                     ->fetchOne();
                    $oBloc instanceof Bloc;
                    $oBloc->Translation->en->titre = $aLigne[4];
                    $oBloc->Translation->en->texte = '<p>' . $aLigne[5] . '</p>';
                    $oBloc->Translation->de->titre = $aLigne[6];
                    $oBloc->Translation->de->texte = '<p>' . $aLigne[7] . '</p>';
                    $oBloc->Translation->es->titre = $aLigne[8];
                    $oBloc->Translation->es->texte = '<p>' . $aLigne[9] . '</p>';
                    $oBloc->Translation->fr->titre = $aLigne[10];
                    $oBloc->Translation->fr->texte = '<p>' . $aLigne[11] . '</p>';
                    $oBloc->Translation->it->titre = $aLigne[12];
                    $oBloc->Translation->it->texte = '<p>' . $aLigne[13] . '</p>';
                    $oBloc->Translation->ja->titre = $aLigne[14];
                    $oBloc->Translation->ja->texte = '<p>' . $aLigne[15] . '</p>';
                    $oBloc->Translation->ko->titre = $aLigne[16];
                    $oBloc->Translation->ko->texte = '<p>' . $aLigne[17] . '</p>';
                    $oBloc->Translation->pt->titre = $aLigne[18];
                    $oBloc->Translation->pt->texte = '<p>' . $aLigne[19] . '</p>';
                    $oBloc->Translation->en->save();
                    $oBloc->Translation->de->save();
                    $oBloc->Translation->es->save();
                    $oBloc->Translation->fr->save();
                    $oBloc->Translation->it->save();
                    $oBloc->Translation->ja->save();
                    $oBloc->Translation->ko->save();
                    $oBloc->Translation->pt->save();
                }
                break;
            case 'note':
                $iIdentifiantNote = intval($aLigne[1]);
                if ($iIdentifiantNote) {
                    $oNote = Doctrine::getTable('NoteFicheProduit')
                                     ->createQuery('n')
                                     ->innerJoin('n.Translation t')
                                     ->where('n.id = ?', $iIdentifiantNote)
                                     ->setHydrationMode(Doctrine::HYDRATE_RECORD)
                                     ->fetchOne();
                    $oNote instanceof NoteFicheProduit;
                    $oNote->Translation->en->titre = $aLigne[4];
                    $oNote->Translation->en->texte = '<p>' . $aLigne[5] . '</p>';
                    $oNote->Translation->de->titre = $aLigne[6];
                    $oNote->Translation->de->texte = '<p>' . $aLigne[7] . '</p>';
                    $oNote->Translation->es->titre = $aLigne[8];
                    $oNote->Translation->es->texte = '<p>' . $aLigne[9] . '</p>';
                    $oNote->Translation->fr->titre = $aLigne[10];
                    $oNote->Translation->fr->texte = '<p>' . $aLigne[11] . '</p>';
                    $oNote->Translation->it->titre = $aLigne[12];
                    $oNote->Translation->it->texte = '<p>' . $aLigne[13] . '</p>';
                    $oNote->Translation->ja->titre = $aLigne[14];
                    $oNote->Translation->ja->texte = '<p>' . $aLigne[15] . '</p>';
                    $oNote->Translation->ko->titre = $aLigne[16];
                    $oNote->Translation->ko->texte = '<p>' . $aLigne[17] . '</p>';
                    $oNote->Translation->pt->titre = $aLigne[18];
                    $oNote->Translation->pt->texte = '<p>' . $aLigne[19] . '</p>';
                    $oNote->Translation->en->save();
                    $oNote->Translation->de->save();
                    $oNote->Translation->es->save();
                    $oNote->Translation->fr->save();
                    $oNote->Translation->it->save();
                    $oNote->Translation->ja->save();
                    $oNote->Translation->ko->save();
                    $oNote->Translation->pt->save();
                }
                break;
            case 'dateclef':
                $iIdentifiantDate = intval($aLigne[1]);
                if ($iIdentifiantDate) {
                    $oDate = Doctrine::getTable('DateClef')
                                     ->createQuery('d')
                                     ->innerJoin('d.Translation t')
                                     ->where('d.id = ?', $iIdentifiantDate)
                                     ->setHydrationMode(Doctrine::HYDRATE_RECORD)
                                     ->fetchOne();
                    $oDate instanceof DateClef;
                    $oDate->Translation->en->titre = $aLigne[4];
                    $oDate->Translation->en->texte = '<p>' . $aLigne[5] . '</p>';
                    $oDate->Translation->de->titre = $aLigne[6];
                    $oDate->Translation->de->texte = '<p>' . $aLigne[7] . '</p>';
                    $oDate->Translation->es->titre = $aLigne[8];
                    $oDate->Translation->es->texte = '<p>' . $aLigne[9] . '</p>';
                    $oDate->Translation->fr->titre = $aLigne[10];
                    $oDate->Translation->fr->texte = '<p>' . $aLigne[11] . '</p>';
                    $oDate->Translation->it->titre = $aLigne[12];
                    $oDate->Translation->it->texte = '<p>' . $aLigne[13] . '</p>';
                    $oDate->Translation->ja->titre = $aLigne[14];
                    $oDate->Translation->ja->texte = '<p>' . $aLigne[15] . '</p>';
                    $oDate->Translation->ko->titre = $aLigne[16];
                    $oDate->Translation->ko->texte = '<p>' . $aLigne[17] . '</p>';
                    $oDate->Translation->pt->titre = $aLigne[18];
                    $oDate->Translation->pt->texte = '<p>' . $aLigne[19] . '</p>';
                    $oDate->Translation->en->save();
                    $oDate->Translation->de->save();
                    $oDate->Translation->es->save();
                    $oDate->Translation->fr->save();
                    $oDate->Translation->it->save();
                    $oDate->Translation->ja->save();
                    $oDate->Translation->ko->save();
                    $oDate->Translation->pt->save();
                }
                break;
            case 'expedition':
                $iIdentifiantExpedition = intval($aLigne[1]);
                if ($iIdentifiantExpedition) {
                    $oExpe = Doctrine::getTable('GreatExpedition')
                                     ->createQuery('e')
                                     ->innerJoin('e.Translation t')
                                     ->where('e.id = ?' , $iIdentifiantExpedition)
                                     ->setHydrationMode(Doctrine::HYDRATE_RECORD)
                                     ->fetchOne();
                    $oExpe instanceof GreatExpedition;
                    $oExpe->Translation->en->pays  = $aLigne[4];
                    $oExpe->Translation->en->texte = '<p>' . $aLigne[5] . '</p>';
                    $oExpe->Translation->de->pays  = $aLigne[6];
                    $oExpe->Translation->de->texte = '<p>' . $aLigne[7] . '</p>';
                    $oExpe->Translation->es->pays  = $aLigne[8];
                    $oExpe->Translation->es->texte = '<p>' . $aLigne[9] . '</p>';
                    $oExpe->Translation->fr->pays  = $aLigne[10];
                    $oExpe->Translation->fr->texte = '<p>' . $aLigne[11] . '</p>';
                    $oExpe->Translation->it->pays  = $aLigne[12];
                    $oExpe->Translation->it->texte = '<p>' . $aLigne[13] . '</p>';
                    $oExpe->Translation->ja->pays  = $aLigne[14];
                    $oExpe->Translation->ja->texte = '<p>' . $aLigne[15] . '</p>';
                    $oExpe->Translation->ko->pays  = $aLigne[16];
                    $oExpe->Translation->ko->texte = '<p>' . $aLigne[17] . '</p>';
                    $oExpe->Translation->pt->pays  = $aLigne[18];
                    $oExpe->Translation->pt->texte = '<p>' . $aLigne[19] . '</p>';
                    $oExpe->Translation->en->save();
                    $oExpe->Translation->de->save();
                    $oExpe->Translation->es->save();
                    $oExpe->Translation->fr->save();
                    $oExpe->Translation->it->save();
                    $oExpe->Translation->ja->save();
                    $oExpe->Translation->ko->save();
                    $oExpe->Translation->pt->save();
                }
                break;
            case 'women':
                $iWoman = intval($aLigne[1]);
                if ($iWoman) {
                    $oWoman = Doctrine::getTable('WomenOfInspiration')
                                     ->createQuery('w')
                                     ->innerJoin('w.Translation t')
                                     ->where('w.id = ?' , $iWoman)
                                     ->setHydrationMode(Doctrine::HYDRATE_RECORD)
                                     ->fetchOne();
                    $oWoman instanceof WomenOfInspiration;
                    $oWoman->Translation->en->pays  = $aLigne[4];
                    $oWoman->Translation->en->texte = '<p>' . $aLigne[5] . '</p>';
                    $oWoman->Translation->de->pays  = $aLigne[6];
                    $oWoman->Translation->de->texte = '<p>' . $aLigne[7] . '</p>';
                    $oWoman->Translation->es->pays  = $aLigne[8];
                    $oWoman->Translation->es->texte = '<p>' . $aLigne[9] . '</p>';
                    $oWoman->Translation->fr->pays  = $aLigne[10];
                    $oWoman->Translation->fr->texte = '<p>' . $aLigne[11] . '</p>';
                    $oWoman->Translation->it->pays  = $aLigne[12];
                    $oWoman->Translation->it->texte = '<p>' . $aLigne[13] . '</p>';
                    $oWoman->Translation->ja->pays  = $aLigne[14];
                    $oWoman->Translation->ja->texte = '<p>' . $aLigne[15] . '</p>';
                    $oWoman->Translation->ko->pays  = $aLigne[16];
                    $oWoman->Translation->ko->texte = '<p>' . $aLigne[17] . '</p>';
                    $oWoman->Translation->pt->pays  = $aLigne[18];
                    $oWoman->Translation->pt->texte = '<p>' . $aLigne[19] . '</p>';
                    $oWoman->Translation->en->save();
                    $oWoman->Translation->de->save();
                    $oWoman->Translation->es->save();
                    $oWoman->Translation->fr->save();
                    $oWoman->Translation->it->save();
                    $oWoman->Translation->ja->save();
                    $oWoman->Translation->ko->save();
                    $oWoman->Translation->pt->save();
                }
                break;
            default:
                break;
        }
    }
  }
}
