<?php

class veuveInitordresnotesTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'frontend'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'prod'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'veuve';
    $this->name             = 'initordresnotes';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [veuve:initordresnotes|INFO] task does things.
Call it with:

  [php symfony veuve:initordres|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // Gestion des notes fiche produit
    $oNotesFProduits = Doctrine::getTable('NoteFicheProduit')
                               ->createQuery('n')
                               ->orderBy('n.fiche_produit_id ASC')
                               ->addOrderBy('n.id ASC')
                               ->execute();
    $iCurrentFProduitId = null;
    $iOrdreCourant = 0;
    foreach ($oNotesFProduits as $oNoteFProduit) {
      if ($iCurrentFProduitId != $oNoteFProduit->getFicheProduitId()) {
        $iCurrentFProduitId = $oNoteFProduit->getFicheProduitId();
        $iOrdreCourant = 1;
      }
      $oNoteFProduit->setOrdre($iOrdreCourant)->save();
      $iOrdreCourant++;
    }
  }
}
