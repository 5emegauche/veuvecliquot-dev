<?php

/**
 * StoreLocator filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseStoreLocatorFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nom'         => new sfWidgetFormFilterInput(),
      'ville'       => new sfWidgetFormFilterInput(),
      'adresse'     => new sfWidgetFormFilterInput(),
      'code_postal' => new sfWidgetFormFilterInput(),
      'pays'        => new sfWidgetFormFilterInput(),
      'ordre'       => new sfWidgetFormFilterInput(),
      'image'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'nom'         => new sfValidatorPass(array('required' => false)),
      'ville'       => new sfValidatorPass(array('required' => false)),
      'adresse'     => new sfValidatorPass(array('required' => false)),
      'code_postal' => new sfValidatorPass(array('required' => false)),
      'pays'        => new sfValidatorPass(array('required' => false)),
      'ordre'       => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'image'       => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('store_locator_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'StoreLocator';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'nom'         => 'Text',
      'ville'       => 'Text',
      'adresse'     => 'Text',
      'code_postal' => 'Text',
      'pays'        => 'Text',
      'ordre'       => 'Number',
      'image'       => 'Text',
    );
  }
}
