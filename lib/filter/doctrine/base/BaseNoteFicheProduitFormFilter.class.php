<?php

/**
 * NoteFicheProduit filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseNoteFicheProduitFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'fiche_produit_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('FicheProduit'), 'add_empty' => true)),
      'note_vin'         => new sfWidgetFormFilterInput(),
      'ordre'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'fiche_produit_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('FicheProduit'), 'column' => 'id')),
      'note_vin'         => new sfValidatorPass(array('required' => false)),
      'ordre'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('note_fiche_produit_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'NoteFicheProduit';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'fiche_produit_id' => 'ForeignKey',
      'note_vin'         => 'Text',
      'ordre'            => 'Number',
    );
  }
}
