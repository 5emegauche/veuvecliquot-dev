<?php

/**
 * Langue filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseLangueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'           => new sfWidgetFormFilterInput(),
      'nom'            => new sfWidgetFormFilterInput(),
      'nom_webservice' => new sfWidgetFormFilterInput(),
      'pays_list'      => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Pays')),
    ));

    $this->setValidators(array(
      'code'           => new sfValidatorPass(array('required' => false)),
      'nom'            => new sfValidatorPass(array('required' => false)),
      'nom_webservice' => new sfValidatorPass(array('required' => false)),
      'pays_list'      => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Pays', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('langue_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addPaysListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.PaysLangue PaysLangue')
      ->andWhereIn('PaysLangue.pays_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Langue';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'code'           => 'Text',
      'nom'            => 'Text',
      'nom_webservice' => 'Text',
      'pays_list'      => 'ManyKey',
    );
  }
}
