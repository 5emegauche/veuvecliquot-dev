<?php

/**
 * BlocFicheProduit filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBlocFicheProduitFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'fiche_produit_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('FicheProduit'), 'add_empty' => true)),
      'description'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'ancre'            => new sfWidgetFormFilterInput(),
      'ordre'            => new sfWidgetFormFilterInput(),
      'ecommerce'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'fiche_produit_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('FicheProduit'), 'column' => 'id')),
      'description'      => new sfValidatorPass(array('required' => false)),
      'ancre'            => new sfValidatorPass(array('required' => false)),
      'ordre'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ecommerce'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('bloc_fiche_produit_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocFicheProduit';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'fiche_produit_id' => 'ForeignKey',
      'description'      => 'Text',
      'ancre'            => 'Text',
      'ordre'            => 'Number',
      'ecommerce'        => 'Boolean',
    );
  }
}
