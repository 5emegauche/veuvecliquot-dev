<?php

/**
 * ActualiteSlide filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseActualiteSlideFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'actualite_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Actualite'), 'add_empty' => true)),
      'ordre'        => new sfWidgetFormFilterInput(),
      'image'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'actualite_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Actualite'), 'column' => 'id')),
      'ordre'        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'image'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actualite_slide_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ActualiteSlide';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'actualite_id' => 'ForeignKey',
      'ordre'        => 'Number',
      'image'        => 'Text',
    );
  }
}
