<?php

/**
 * WomenOfInspirationTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWomenOfInspirationTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'pays'  => new sfWidgetFormFilterInput(),
      'texte' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'pays'  => new sfValidatorPass(array('required' => false)),
      'texte' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('women_of_inspiration_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WomenOfInspirationTranslation';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'pays'  => 'Text',
      'texte' => 'Text',
      'lang'  => 'Text',
    );
  }
}
