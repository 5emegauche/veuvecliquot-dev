<?php

/**
 * FicheProduit filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseFicheProduitFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'idproduit'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'      => new sfWidgetFormFilterInput(),
      'image_note' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'idproduit'  => new sfValidatorPass(array('required' => false)),
      'image'      => new sfValidatorPass(array('required' => false)),
      'image_note' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('fiche_produit_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'FicheProduit';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'idproduit'  => 'Text',
      'image'      => 'Text',
      'image_note' => 'Text',
    );
  }
}
