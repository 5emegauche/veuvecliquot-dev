<?php

/**
 * BlocFicheProduitEcommerce filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBlocFicheProduitEcommerceFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bloc_fiche_produit_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BlocFicheProduit'), 'add_empty' => true)),
      'pays_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Pays'), 'add_empty' => true)),
      'langue_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Langue'), 'add_empty' => true)),
      'autorise'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'lien'                  => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bloc_fiche_produit_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('BlocFicheProduit'), 'column' => 'id')),
      'pays_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Pays'), 'column' => 'id')),
      'langue_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Langue'), 'column' => 'id')),
      'autorise'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'lien'                  => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_fiche_produit_ecommerce_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocFicheProduitEcommerce';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'bloc_fiche_produit_id' => 'ForeignKey',
      'pays_id'               => 'ForeignKey',
      'langue_id'             => 'ForeignKey',
      'autorise'              => 'Boolean',
      'lien'                  => 'Text',
    );
  }
}
