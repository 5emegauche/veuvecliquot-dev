<?php

/**
 * Slide filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSlideFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'bloc_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Bloc'), 'add_empty' => true)),
      'ordre'   => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'bloc_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Bloc'), 'column' => 'id')),
      'ordre'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('slide_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Slide';
  }

  public function getFields()
  {
    return array(
      'id'      => 'Number',
      'bloc_id' => 'ForeignKey',
      'ordre'   => 'Number',
    );
  }
}
