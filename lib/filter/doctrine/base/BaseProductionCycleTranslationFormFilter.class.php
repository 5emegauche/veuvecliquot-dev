<?php

/**
 * ProductionCycleTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseProductionCycleTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'image' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'image' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('production_cycle_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ProductionCycleTranslation';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'image' => 'Text',
      'lang'  => 'Text',
    );
  }
}
