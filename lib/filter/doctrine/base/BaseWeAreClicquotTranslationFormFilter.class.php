<?php

/**
 * WeAreClicquotTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWeAreClicquotTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titre' => new sfWidgetFormFilterInput(),
      'texte' => new sfWidgetFormFilterInput(),
      'video' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titre' => new sfValidatorPass(array('required' => false)),
      'texte' => new sfValidatorPass(array('required' => false)),
      'video' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('we_are_clicquot_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WeAreClicquotTranslation';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'titre' => 'Text',
      'texte' => 'Text',
      'video' => 'Text',
      'lang'  => 'Text',
    );
  }
}
