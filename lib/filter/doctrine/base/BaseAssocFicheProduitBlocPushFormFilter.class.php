<?php

/**
 * AssocFicheProduitBlocPush filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseAssocFicheProduitBlocPushFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'ordre'            => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'ordre'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('assoc_fiche_produit_bloc_push_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AssocFicheProduitBlocPush';
  }

  public function getFields()
  {
    return array(
      'fiche_produit_id' => 'Number',
      'bloc_push_id'     => 'Number',
      'ordre'            => 'Number',
    );
  }
}
