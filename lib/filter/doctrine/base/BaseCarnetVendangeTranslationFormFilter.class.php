<?php

/**
 * CarnetVendangeTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCarnetVendangeTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titre'  => new sfWidgetFormFilterInput(),
      'date1'  => new sfWidgetFormFilterInput(),
      'texte1' => new sfWidgetFormFilterInput(),
      'date2'  => new sfWidgetFormFilterInput(),
      'texte2' => new sfWidgetFormFilterInput(),
      'date3'  => new sfWidgetFormFilterInput(),
      'texte3' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titre'  => new sfValidatorPass(array('required' => false)),
      'date1'  => new sfValidatorPass(array('required' => false)),
      'texte1' => new sfValidatorPass(array('required' => false)),
      'date2'  => new sfValidatorPass(array('required' => false)),
      'texte2' => new sfValidatorPass(array('required' => false)),
      'date3'  => new sfValidatorPass(array('required' => false)),
      'texte3' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('carnet_vendange_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CarnetVendangeTranslation';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'titre'  => 'Text',
      'date1'  => 'Text',
      'texte1' => 'Text',
      'date2'  => 'Text',
      'texte2' => 'Text',
      'date3'  => 'Text',
      'texte3' => 'Text',
      'lang'   => 'Text',
    );
  }
}
