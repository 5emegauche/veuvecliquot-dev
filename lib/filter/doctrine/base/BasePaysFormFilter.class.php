<?php

/**
 * Pays filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePaysFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'nom'               => new sfWidgetFormFilterInput(),
      'age_legal'         => new sfWidgetFormFilterInput(),
      'autorise'          => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'affiche_facebook'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'facebook'          => new sfWidgetFormFilterInput(),
      'affiche_youtube'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'youtube'           => new sfWidgetFormFilterInput(),
      'affiche_twitter'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'twitter'           => new sfWidgetFormFilterInput(),
      'affiche_pinterest' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'pinterest'         => new sfWidgetFormFilterInput(),
      'codeiso'           => new sfWidgetFormFilterInput(),
      'ordre'             => new sfWidgetFormFilterInput(),
      'ecommerce'         => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'lien_ecommerce'    => new sfWidgetFormFilterInput(),
      'slug'              => new sfWidgetFormFilterInput(),
      'langue_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Langue')),
      'liens_list'        => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Lien')),
    ));

    $this->setValidators(array(
      'nom'               => new sfValidatorPass(array('required' => false)),
      'age_legal'         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'autorise'          => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'affiche_facebook'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'facebook'          => new sfValidatorPass(array('required' => false)),
      'affiche_youtube'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'youtube'           => new sfValidatorPass(array('required' => false)),
      'affiche_twitter'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'twitter'           => new sfValidatorPass(array('required' => false)),
      'affiche_pinterest' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'pinterest'         => new sfValidatorPass(array('required' => false)),
      'codeiso'           => new sfValidatorPass(array('required' => false)),
      'ordre'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ecommerce'         => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'lien_ecommerce'    => new sfValidatorPass(array('required' => false)),
      'slug'              => new sfValidatorPass(array('required' => false)),
      'langue_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Langue', 'required' => false)),
      'liens_list'        => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Lien', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('pays_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addLangueListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.PaysLangue PaysLangue')
      ->andWhereIn('PaysLangue.langue_id', $values)
    ;
  }

  public function addLiensListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.LienPays LienPays')
      ->andWhereIn('LienPays.lien_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Pays';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'nom'               => 'Text',
      'age_legal'         => 'Number',
      'autorise'          => 'Boolean',
      'affiche_facebook'  => 'Boolean',
      'facebook'          => 'Text',
      'affiche_youtube'   => 'Boolean',
      'youtube'           => 'Text',
      'affiche_twitter'   => 'Boolean',
      'twitter'           => 'Text',
      'affiche_pinterest' => 'Boolean',
      'pinterest'         => 'Text',
      'codeiso'           => 'Text',
      'ordre'             => 'Number',
      'ecommerce'         => 'Boolean',
      'lien_ecommerce'    => 'Text',
      'slug'              => 'Text',
      'langue_list'       => 'ManyKey',
      'liens_list'        => 'ManyKey',
    );
  }
}
