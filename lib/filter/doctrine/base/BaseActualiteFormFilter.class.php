<?php

/**
 * Actualite filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseActualiteFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'categorie'              => new sfWidgetFormFilterInput(),
      'titre'                  => new sfWidgetFormFilterInput(),
      'texte'                  => new sfWidgetFormFilterInput(),
      'image'                  => new sfWidgetFormFilterInput(),
      'bloc_titre'             => new sfWidgetFormFilterInput(),
      'bloc_texte'             => new sfWidgetFormFilterInput(),
      'bloc_image'             => new sfWidgetFormFilterInput(),
      'lang'                   => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'video'                  => new sfWidgetFormFilterInput(),
      'custom_label_read_more' => new sfWidgetFormFilterInput(),
      'custom_read_more'       => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'categorie'              => new sfValidatorPass(array('required' => false)),
      'titre'                  => new sfValidatorPass(array('required' => false)),
      'texte'                  => new sfValidatorPass(array('required' => false)),
      'image'                  => new sfValidatorPass(array('required' => false)),
      'bloc_titre'             => new sfValidatorPass(array('required' => false)),
      'bloc_texte'             => new sfValidatorPass(array('required' => false)),
      'bloc_image'             => new sfValidatorPass(array('required' => false)),
      'lang'                   => new sfValidatorPass(array('required' => false)),
      'video'                  => new sfValidatorPass(array('required' => false)),
      'custom_label_read_more' => new sfValidatorPass(array('required' => false)),
      'custom_read_more'       => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actualite_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Actualite';
  }

  public function getFields()
  {
    return array(
      'id'                     => 'Number',
      'categorie'              => 'Text',
      'titre'                  => 'Text',
      'texte'                  => 'Text',
      'image'                  => 'Text',
      'bloc_titre'             => 'Text',
      'bloc_texte'             => 'Text',
      'bloc_image'             => 'Text',
      'lang'                   => 'Text',
      'video'                  => 'Text',
      'custom_label_read_more' => 'Text',
      'custom_read_more'       => 'Text',
    );
  }
}
