<?php

/**
 * Lien filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseLienFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type_lien'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'lien_interne'   => new sfWidgetFormFilterInput(),
      'lien_externe'   => new sfWidgetFormFilterInput(),
      'lien_parent_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('LienParent'), 'add_empty' => true)),
      'ordre'          => new sfWidgetFormFilterInput(),
      'only_tablet'    => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'pays_list'      => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Pays')),
    ));

    $this->setValidators(array(
      'type_lien'      => new sfValidatorPass(array('required' => false)),
      'lien_interne'   => new sfValidatorPass(array('required' => false)),
      'lien_externe'   => new sfValidatorPass(array('required' => false)),
      'lien_parent_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('LienParent'), 'column' => 'id')),
      'ordre'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'only_tablet'    => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'pays_list'      => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Pays', 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('lien_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function addPaysListColumnQuery(Doctrine_Query $query, $field, $values)
  {
    if (!is_array($values))
    {
      $values = array($values);
    }

    if (!count($values))
    {
      return;
    }

    $query
      ->leftJoin($query->getRootAlias().'.LienPays LienPays')
      ->andWhereIn('LienPays.pays_id', $values)
    ;
  }

  public function getModelName()
  {
    return 'Lien';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'type_lien'      => 'Text',
      'lien_interne'   => 'Text',
      'lien_externe'   => 'Text',
      'lien_parent_id' => 'ForeignKey',
      'ordre'          => 'Number',
      'only_tablet'    => 'Boolean',
      'pays_list'      => 'ManyKey',
    );
  }
}
