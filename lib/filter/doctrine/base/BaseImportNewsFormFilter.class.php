<?php

/**
 * ImportNews filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseImportNewsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'pays_langue'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'dernier_import' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'pays_langue'    => new sfValidatorPass(array('required' => false)),
      'dernier_import' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('import_news_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ImportNews';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'pays_langue'    => 'Text',
      'dernier_import' => 'Text',
    );
  }
}
