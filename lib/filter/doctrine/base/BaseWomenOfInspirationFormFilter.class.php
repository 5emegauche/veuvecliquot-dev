<?php

/**
 * WomenOfInspiration filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWomenOfInspirationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'photo' => new sfWidgetFormFilterInput(),
      'nom'   => new sfWidgetFormFilterInput(),
      'ordre' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'photo' => new sfValidatorPass(array('required' => false)),
      'nom'   => new sfValidatorPass(array('required' => false)),
      'ordre' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('women_of_inspiration_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WomenOfInspiration';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'photo' => 'Text',
      'nom'   => 'Text',
      'ordre' => 'Number',
    );
  }
}
