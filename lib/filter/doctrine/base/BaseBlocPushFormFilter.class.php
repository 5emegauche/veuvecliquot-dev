<?php

/**
 * BlocPush filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBlocPushFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'description'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'        => new sfWidgetFormFilterInput(),
      'type_lien'    => new sfWidgetFormFilterInput(),
      'lien_interne' => new sfWidgetFormFilterInput(),
      'lien_externe' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'description'  => new sfValidatorPass(array('required' => false)),
      'image'        => new sfValidatorPass(array('required' => false)),
      'type_lien'    => new sfValidatorPass(array('required' => false)),
      'lien_interne' => new sfValidatorPass(array('required' => false)),
      'lien_externe' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_push_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocPush';
  }

  public function getFields()
  {
    return array(
      'id'           => 'Number',
      'description'  => 'Text',
      'image'        => 'Text',
      'type_lien'    => 'Text',
      'lien_interne' => 'Text',
      'lien_externe' => 'Text',
    );
  }
}
