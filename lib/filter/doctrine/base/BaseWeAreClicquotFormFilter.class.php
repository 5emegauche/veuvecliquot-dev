<?php

/**
 * WeAreClicquot filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWeAreClicquotFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'visuel'  => new sfWidgetFormFilterInput(),
      'ordre'   => new sfWidgetFormFilterInput(),
      'visible' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'visuel'  => new sfValidatorPass(array('required' => false)),
      'ordre'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'visible' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('we_are_clicquot_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WeAreClicquot';
  }

  public function getFields()
  {
    return array(
      'id'      => 'Number',
      'visuel'  => 'Text',
      'ordre'   => 'Number',
      'visible' => 'Boolean',
    );
  }
}
