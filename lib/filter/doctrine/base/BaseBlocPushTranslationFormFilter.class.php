<?php

/**
 * BlocPushTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBlocPushTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titre'      => new sfWidgetFormFilterInput(),
      'texte_lien' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titre'      => new sfValidatorPass(array('required' => false)),
      'texte_lien' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_push_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocPushTranslation';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'titre'      => 'Text',
      'texte_lien' => 'Text',
      'lang'       => 'Text',
    );
  }
}
