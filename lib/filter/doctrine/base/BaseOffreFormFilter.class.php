<?php

/**
 * Offre filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseOffreFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'idproduit' => new sfWidgetFormFilterInput(array('with_empty' => false)),
    ));

    $this->setValidators(array(
      'idproduit' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('offre_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Offre';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'idproduit' => 'Text',
    );
  }
}
