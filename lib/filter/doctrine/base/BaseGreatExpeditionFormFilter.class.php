<?php

/**
 * GreatExpedition filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseGreatExpeditionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type_bloc' => new sfWidgetFormFilterInput(),
      'image'     => new sfWidgetFormFilterInput(),
      'annee'     => new sfWidgetFormFilterInput(),
      'ordre'     => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type_bloc' => new sfValidatorPass(array('required' => false)),
      'image'     => new sfValidatorPass(array('required' => false)),
      'annee'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'ordre'     => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('great_expedition_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GreatExpedition';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'type_bloc' => 'Text',
      'image'     => 'Text',
      'annee'     => 'Number',
      'ordre'     => 'Number',
    );
  }
}
