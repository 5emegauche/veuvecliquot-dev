<?php

/**
 * Bloc filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBlocFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type_bloc'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'image'      => new sfWidgetFormFilterInput(),
      'image2'     => new sfWidgetFormFilterInput(),
      'ancre'      => new sfWidgetFormFilterInput(),
      'ordre'      => new sfWidgetFormFilterInput(),
      'type'       => new sfWidgetFormFilterInput(),
      'ecommerce'  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'offre_id'   => new sfWidgetFormFilterInput(),
      'article_id' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type_bloc'  => new sfValidatorPass(array('required' => false)),
      'image'      => new sfValidatorPass(array('required' => false)),
      'image2'     => new sfValidatorPass(array('required' => false)),
      'ancre'      => new sfValidatorPass(array('required' => false)),
      'ordre'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'type'       => new sfValidatorPass(array('required' => false)),
      'ecommerce'  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'offre_id'   => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'article_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
    ));

    $this->widgetSchema->setNameFormat('bloc_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bloc';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'type_bloc'  => 'Text',
      'image'      => 'Text',
      'image2'     => 'Text',
      'ancre'      => 'Text',
      'ordre'      => 'Number',
      'type'       => 'Text',
      'ecommerce'  => 'Boolean',
      'offre_id'   => 'Number',
      'article_id' => 'Number',
    );
  }
}
