<?php

/**
 * PageStatiqueTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePageStatiqueTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'slug'  => new sfWidgetFormFilterInput(),
      'titre' => new sfWidgetFormFilterInput(),
      'texte' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'slug'  => new sfValidatorPass(array('required' => false)),
      'titre' => new sfValidatorPass(array('required' => false)),
      'texte' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('page_statique_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PageStatiqueTranslation';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'slug'  => 'Text',
      'titre' => 'Text',
      'texte' => 'Text',
      'lang'  => 'Text',
    );
  }
}
