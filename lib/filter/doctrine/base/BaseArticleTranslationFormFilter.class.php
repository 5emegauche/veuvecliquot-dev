<?php

/**
 * ArticleTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseArticleTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titre'  => new sfWidgetFormFilterInput(),
      'slug'   => new sfWidgetFormFilterInput(),
      'ariane' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titre'  => new sfValidatorPass(array('required' => false)),
      'slug'   => new sfValidatorPass(array('required' => false)),
      'ariane' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('article_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ArticleTranslation';
  }

  public function getFields()
  {
    return array(
      'id'     => 'Number',
      'titre'  => 'Text',
      'slug'   => 'Text',
      'ariane' => 'Text',
      'lang'   => 'Text',
    );
  }
}
