<?php

/**
 * WomenPageTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWomenPageTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titre' => new sfWidgetFormFilterInput(),
      'texte' => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titre' => new sfValidatorPass(array('required' => false)),
      'texte' => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('women_page_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WomenPageTranslation';
  }

  public function getFields()
  {
    return array(
      'id'    => 'Number',
      'titre' => 'Text',
      'texte' => 'Text',
      'lang'  => 'Text',
    );
  }
}
