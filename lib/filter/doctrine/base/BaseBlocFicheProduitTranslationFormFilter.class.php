<?php

/**
 * BlocFicheProduitTranslation filter form base class.
 *
 * @package    veuveclicquot
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseBlocFicheProduitTranslationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'titre'      => new sfWidgetFormFilterInput(),
      'sous_titre' => new sfWidgetFormFilterInput(),
      'contenu'    => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'titre'      => new sfValidatorPass(array('required' => false)),
      'sous_titre' => new sfValidatorPass(array('required' => false)),
      'contenu'    => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_fiche_produit_translation_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocFicheProduitTranslation';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'titre'      => 'Text',
      'sous_titre' => 'Text',
      'contenu'    => 'Text',
      'lang'       => 'Text',
    );
  }
}
