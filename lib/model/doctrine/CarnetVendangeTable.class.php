<?php

/**
 * CarnetVendangeTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class CarnetVendangeTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object CarnetVendangeTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('CarnetVendange');
    }
}