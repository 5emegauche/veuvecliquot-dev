<?php

/**
 * StoreLocator
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    veuveclicquot
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class StoreLocator extends BaseStoreLocator {
    public function getUrlImage() {
        return sfConfig::get('app_chemin_storelocator') . $this->getImage();
    }
    
    public function getTheoricImageName() {
        $aInfos = array('store', $this->getId());
        return implode('-', $aInfos);
    }
    
    public function save(Doctrine_Connection $conn = null) {
        $svg = parent::save($conn);
        
        try {
            $bModifie = false;
            
            $sTheoricName = $this->getTheoricImageName();
            if ($this->getImage() && (substr($this->getImage(), 0, strlen($sTheoricName)) != $sTheoricName)) {
                $sPath = sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_storelocator');
                $sCurrentName = $this->getImage();
                $aInfosPath = pathinfo($sCurrentName);
                $sExtension = $aInfosPath['extension'];
                $sRetinaFileName = $sPath . $sTheoricName . '@2x' . '.' . $sExtension;
                $sFileName = $sPath . $sTheoricName . '.' . $sExtension;
                rename($sPath . $this->getImage(), $sRetinaFileName);
                $oImg = new sfImage($sRetinaFileName);
                $aDimensions = array($oImg->getWidth(), $oImg->getHeight());
                $oImg->resize($aDimensions[0] / 2, $aDimensions[1] / 2);
                $oImg->setQuality(100);
                $oImg->saveAs($sFileName);
                $this->setImage($sTheoricName . '.' . $sExtension);
                $bModifie = true;
                chmod($sRetinaFileName, 0777);
                chmod($sFileName, 0777);
            }
            
            if ($bModifie) $this->save($conn);
        } catch (Exception $e) {
            // Do nothing
        }
        
        return $svg;
    }
}
