<?php

/**
 * BlocArticleTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class BlocArticleTable extends BlocTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object BlocArticleTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('BlocArticle');
    }
}