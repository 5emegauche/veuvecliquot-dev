<?php

/**
 * BaseStoreLocator
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $nom
 * @property string $ville
 * @property string $adresse
 * @property string $code_postal
 * @property string $pays
 * @property integer $ordre
 * @property string $image
 * 
 * @method string       getNom()         Returns the current record's "nom" value
 * @method string       getVille()       Returns the current record's "ville" value
 * @method string       getAdresse()     Returns the current record's "adresse" value
 * @method string       getCodePostal()  Returns the current record's "code_postal" value
 * @method string       getPays()        Returns the current record's "pays" value
 * @method integer      getOrdre()       Returns the current record's "ordre" value
 * @method string       getImage()       Returns the current record's "image" value
 * @method StoreLocator setNom()         Sets the current record's "nom" value
 * @method StoreLocator setVille()       Sets the current record's "ville" value
 * @method StoreLocator setAdresse()     Sets the current record's "adresse" value
 * @method StoreLocator setCodePostal()  Sets the current record's "code_postal" value
 * @method StoreLocator setPays()        Sets the current record's "pays" value
 * @method StoreLocator setOrdre()       Sets the current record's "ordre" value
 * @method StoreLocator setImage()       Sets the current record's "image" value
 * 
 * @package    veuveclicquot
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseStoreLocator extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('store_locator');
        $this->hasColumn('nom', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('ville', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('adresse', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('code_postal', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
        $this->hasColumn('pays', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('ordre', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('image', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        
    }
}