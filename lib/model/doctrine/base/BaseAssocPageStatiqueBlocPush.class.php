<?php

/**
 * BaseAssocPageStatiqueBlocPush
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $page_statique_id
 * @property integer $bloc_push_id
 * @property integer $ordre
 * @property PageStatique $PageStatique
 * @property BlocPush $BlocPush
 * 
 * @method integer                   getPageStatiqueId()   Returns the current record's "page_statique_id" value
 * @method integer                   getBlocPushId()       Returns the current record's "bloc_push_id" value
 * @method integer                   getOrdre()            Returns the current record's "ordre" value
 * @method PageStatique              getPageStatique()     Returns the current record's "PageStatique" value
 * @method BlocPush                  getBlocPush()         Returns the current record's "BlocPush" value
 * @method AssocPageStatiqueBlocPush setPageStatiqueId()   Sets the current record's "page_statique_id" value
 * @method AssocPageStatiqueBlocPush setBlocPushId()       Sets the current record's "bloc_push_id" value
 * @method AssocPageStatiqueBlocPush setOrdre()            Sets the current record's "ordre" value
 * @method AssocPageStatiqueBlocPush setPageStatique()     Sets the current record's "PageStatique" value
 * @method AssocPageStatiqueBlocPush setBlocPush()         Sets the current record's "BlocPush" value
 * 
 * @package    veuveclicquot
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseAssocPageStatiqueBlocPush extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('assoc_page_statique_bloc_push');
        $this->hasColumn('page_statique_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('bloc_push_id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             ));
        $this->hasColumn('ordre', 'integer', null, array(
             'type' => 'integer',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('PageStatique', array(
             'local' => 'page_statique_id',
             'foreign' => 'id'));

        $this->hasOne('BlocPush', array(
             'local' => 'bloc_push_id',
             'foreign' => 'id'));
    }
}