<?php

/**
 * BaseBlocArticle
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property Article $Article
 * 
 * @method Article     getArticle() Returns the current record's "Article" value
 * @method BlocArticle setArticle() Sets the current record's "Article" value
 * 
 * @package    veuveclicquot
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseBlocArticle extends Bloc
{
    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Article', array(
             'local' => 'article_id',
             'foreign' => 'id'));
    }
}