<?php

/**
 * ImportNewsTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class ImportNewsTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object ImportNewsTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('ImportNews');
    }
}