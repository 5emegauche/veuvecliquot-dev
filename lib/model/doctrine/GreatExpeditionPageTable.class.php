<?php

/**
 * GreatExpeditionPageTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class GreatExpeditionPageTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object GreatExpeditionPageTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('GreatExpeditionPage');
    }
}