<?php

class VideoLister {
    private $_sPath;
    
    public function __construct($sPath) {
        $this->_sPath = $sPath;
    }
    
    private function listDirectory($sDirectory) {
        $sDirectory = (substr($sDirectory, strlen($sDirectory) - 1) == DIRECTORY_SEPARATOR)?$sDirectory:($sDirectory . DIRECTORY_SEPARATOR);
        $oDirectory = opendir($sDirectory);
        $aFichiers = array();
        while(($sIterator = readdir($oDirectory)) !== false) {
            if (!in_array($sIterator, array('.', '..'))) {
                if (is_dir($sDirectory . $sIterator)) {
                    $aSubDir = $this->listDirectory($sDirectory . $sIterator);
                    $aFichiers = array_merge($aFichiers, $aSubDir);
                }
                else {
                    $sCheminFichier = str_replace($this->_sPath, '', $sDirectory . $sIterator);
                    $aFichiers[$sCheminFichier] = $sCheminFichier;
                }
            }
        }
        return $aFichiers;
    }
    
    public function getVideoList() {
        $aRetour = array('' => '');
        $aListing = $this->listDirectory($this->_sPath);
        $aRetour = array_merge($aRetour, $aListing);
        return $aRetour;
    }
}