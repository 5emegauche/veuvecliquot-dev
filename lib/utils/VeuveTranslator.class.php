<?php

class VeuveTranslator {
    const CATEGORY_CHAMPAGNE        = 'champagne';
    const CATEGORY_HOUSE            = 'house';
    
    const ARTICLE_KEY_DATES         = 'key-dates';
    const ARTICLE_PRODUCTION_CYCLE  = 'production-cycle';
    const ARTICLE_WOMEN_INSPIRATION = 'women-of-inspiration';
    const ARTICLE_GREAT_EXPEDITIONS = 'great-expeditions';
    const ARTICLE_CARNET_VENDANGES  = 'carnet-de-vendanges';
    const ARTICLE_WE_ARE_CLICQUOT   = 'we-are-clicquot';
    
    const STATIQUE_STORE_LOCATOR    = 'store-locator';
    const STATIQUE_VISITE           = 'visit';
    const STATIQUE_PRESSE           = 'press';
    const STATIQUE_CONTACT          = 'contact';
    
    private static function _generateStatiqueTranslationTable() {
        $aTraductions = array();
        $aTraductions['en'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visiting-the-cellars',
        );
        $aTraductions['de'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'Besichtigungen',
        );
        $aTraductions['es'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visita',
        );
        $aTraductions['fr'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visite',
        );
        $aTraductions['it'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visite',
        );
        $aTraductions['ja'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visiting-the-cellars',
        );
        $aTraductions['ko'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visiting-the-cellars',
        );
        $aTraductions['pt'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visita',
        );
        $aTraductions['ch'] = array(
            self::STATIQUE_CONTACT       => 'contact',
            self::STATIQUE_PRESSE        => 'press',
            self::STATIQUE_STORE_LOCATOR => 'store-locator',
            self::STATIQUE_VISITE        => 'visit',
        );
        
        return $aTraductions;
    }
    
    private static function _generateArticleTranslationTable() {
        $aTraductions = array();
        $aTraductions['en'] = array(
            self::ARTICLE_KEY_DATES         => 'key-dates',
            self::ARTICLE_PRODUCTION_CYCLE  => 'production-cycle',
            self::ARTICLE_WOMEN_INSPIRATION => 'women-of-inspiration',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'harvest',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['de'] = array(
            self::ARTICLE_KEY_DATES         => 'kerndaten',
            self::ARTICLE_PRODUCTION_CYCLE  => 'die-weinbereitung',
            self::ARTICLE_WOMEN_INSPIRATION => 'women-of-inspiration',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'ernte',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['es'] = array(
            self::ARTICLE_KEY_DATES         => 'fechas-clave',
            self::ARTICLE_PRODUCTION_CYCLE  => 'ciclo-de-produccion',
            self::ARTICLE_WOMEN_INSPIRATION => 'mujeres-inspiradoras',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'vendimia',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['fr'] = array(
            self::ARTICLE_KEY_DATES         => 'les-dates-clefs',
            self::ARTICLE_PRODUCTION_CYCLE  => 'cycle-de-l-elaboration',
            self::ARTICLE_WOMEN_INSPIRATION => 'women-of-inspiration',
            self::ARTICLE_GREAT_EXPEDITIONS => 'les-grandes-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'vendanges',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['it'] = array(
            self::ARTICLE_KEY_DATES         => 'le-date-principali',
            self::ARTICLE_PRODUCTION_CYCLE  => 'ciclo-produttivo',
            self::ARTICLE_WOMEN_INSPIRATION => 'il-premio-veuve-clicquot',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'vendemmia',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['ja'] = array(
            self::ARTICLE_KEY_DATES         => 'key-dates',
            self::ARTICLE_PRODUCTION_CYCLE  => 'production-cycle',
            self::ARTICLE_WOMEN_INSPIRATION => 'women-of-inspiration',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'harvest',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['ko'] = array(
            self::ARTICLE_KEY_DATES         => 'key-dates',
            self::ARTICLE_PRODUCTION_CYCLE  => 'production-cycle',
            self::ARTICLE_WOMEN_INSPIRATION => 'women-of-inspiration',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'harvest',
            self::ARTICLE_WE_ARE_CLICQUOT   => 'we-are-clicquot',
        );
        $aTraductions['pt'] = array(
            self::ARTICLE_KEY_DATES         => 'datas-principais',
            self::ARTICLE_PRODUCTION_CYCLE  => 'ciclo-de-producao',
            self::ARTICLE_WOMEN_INSPIRATION => 'mulher-inspiradora',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'colheita',
        );
        $aTraductions['ch'] = array(
            self::ARTICLE_KEY_DATES         => 'key-dates',
            self::ARTICLE_PRODUCTION_CYCLE  => 'production-cycle',
            self::ARTICLE_WOMEN_INSPIRATION => 'women-of-inspiration',
            self::ARTICLE_GREAT_EXPEDITIONS => 'great-expeditions',
            self::ARTICLE_CARNET_VENDANGES  => 'harvest',
        );
        
        return $aTraductions;
    }
    
    private static function _generateCategoryTranslationTable() {
        $aTraductions = array();
        $aTraductions['en'] = array(
            self::CATEGORY_CHAMPAGNE => 'champagne',
            self::CATEGORY_HOUSE     => 'house',
        );
        $aTraductions['de'] = array(
            self::CATEGORY_CHAMPAGNE => 'unser-champagner',
            self::CATEGORY_HOUSE     => 'unser-haus',
        );
        $aTraductions['es'] = array(
            self::CATEGORY_CHAMPAGNE => 'champan',
            self::CATEGORY_HOUSE     => 'empresa',
        );
        $aTraductions['fr'] = array(
            self::CATEGORY_CHAMPAGNE => 'champagne',
            self::CATEGORY_HOUSE     => 'maison',
        );
        $aTraductions['it'] = array(
            self::CATEGORY_CHAMPAGNE => 'lo-champagne',
            self::CATEGORY_HOUSE     => 'la-casa',
        );
        $aTraductions['ja'] = array(
            self::CATEGORY_CHAMPAGNE => 'champagne',
            self::CATEGORY_HOUSE     => 'house',
        );
        $aTraductions['ko'] = array(
            self::CATEGORY_CHAMPAGNE => 'champagne',
            self::CATEGORY_HOUSE     => 'house',
        );
        $aTraductions['pt'] = array(
            self::CATEGORY_CHAMPAGNE => 'champagne',
            self::CATEGORY_HOUSE     => 'casa',
        );
        $aTraductions['ch'] = array(
            self::CATEGORY_CHAMPAGNE => 'xiang-bin',
            self::CATEGORY_HOUSE     => 'jiu-zhuang',
        );
        
        return $aTraductions;
    }
    
    public static function getCategorySlug($sNomCategorie, $sCulture) {
        $aTraductions = self::_generateCategoryTranslationTable();
        if (isset($aTraductions[$sCulture][$sNomCategorie]))
            return $aTraductions[$sCulture][$sNomCategorie];
        return false;
    }
    
    public static function getCategoryFromSlug($sSlug, $sCulture) {
        $aTraductions = self::_generateCategoryTranslationTable();
        $aTraductionLocale = array_flip($aTraductions[$sCulture]);
        if (isset($aTraductionLocale[$sSlug]))
            return $aTraductionLocale[$sSlug];
        return false;
    }
    
    public static function getArticleSlug($sNomArticle, $sCulture) {
        $aTraductions = self::_generateArticleTranslationTable();
        if (isset($aTraductions[$sCulture][$sNomArticle]))
            return $aTraductions[$sCulture][$sNomArticle];
        return false;
    }
    
    public static function getArticleFromSlug($sSlug, $sCulture) {
        $aTraductions = self::_generateArticleTranslationTable();
        $aTraductionLocale = array_flip($aTraductions[$sCulture]);
        if (isset($aTraductionLocale[$sSlug]))
            return $aTraductionLocale[$sSlug];
        return false;
    }
    
    public static function getStatiqueSlug($sNomStatique, $sCulture) {
        $aTraductions = self::_generateStatiqueTranslationTable();
        if (isset($aTraductions[$sCulture][$sNomStatique]))
            return $aTraductions[$sCulture][$sNomStatique];
        return false;
    }
    
    public static function getStatiqueFromSlug($sSlug, $sCulture) {
        $aTraductions = self::_generateStatiqueTranslationTable();
        $aTraductionsLocale = array_flip($aTraductions[$sCulture]);
        if (isset($aTraductionsLocale[$sSlug]))
            return $aTraductionsLocale[$sSlug];
        return false;
    }
}