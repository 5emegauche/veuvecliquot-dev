<?php

class FormBuilder {
    private $_sUrlStructure;
    private $_oStructure;
    
    public function __construct($sUrlStructure) {
        $this->_sUrlStructure = $sUrlStructure;
    }
    
    public function getStructure($sLang) {
        $oCURLSession = curl_init($this->_sUrlStructure . 'Form');
        curl_setopt($oCURLSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($oCURLSession, CURLOPT_POST, true);
        curl_setopt($oCURLSession, CURLOPT_POSTFIELDS, array('lang' => $sLang));
        curl_setopt($oCURLSession, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        if (sfConfig::get('app_contactform_htuser') && sfConfig::get('app_contactform_htpass'))
            curl_setopt($oCURLSession, CURLOPT_USERPWD, sfConfig::get('app_contactform_htuser') . ':' . sfConfig::get('app_contactform_htpass'));
        $sRetoutCurl = curl_exec($oCURLSession);
        curl_close($oCURLSession);
        
        // Analyse du fichier xml
        $aPartiesFormulaire = array();
        $oStructureFormulaire = simplexml_load_string($sRetoutCurl);
        foreach($oStructureFormulaire->item as $oItem) {
            $aPartiesFormulaire[(string)$oItem->form_key] = array(
                'nom'         => (string)$oItem->form_key,
                'valeur'      => (string)$oItem->value,
                'label'       => (string)$oItem->name,
                'typec'       => (string)$oItem->type,
                'items'       => $oItem->extra->items,
                'multiple'    => (string)$oItem->extra->multiple,
                'aslist'      => (string)$oItem->extra->aslist,
                'obligatoire' => (string)$oItem->mandatory,
                'othertext'   => (string)$oItem->extra->other_text,
                'otheroption' => (string)$oItem->extra->other_option,
            );
        }
        //echo '<pre>' . print_r($aPartiesFormulaire, true) . '</pre>';die;
        $this->_oStructure = $aPartiesFormulaire;
        return $aPartiesFormulaire;
    }
    
    public function sendData($sLang, sfWebRequest $request) {
        sfApplicationConfiguration::getActive()->loadHelpers('I18N');

        $bEnvoiPossible = true;
        $aDonneesEnvoyees = array('lang' => $sLang);
        $aDatas = array();
        $aErreurs = array();
        $aChampsErreur = array();
        $aRetour = array();
        foreach($this->_oStructure as $aChamp) {
            $sChamp = $aChamp['nom'];
            $bObligatoire = $aChamp['obligatoire'];
            $sValeur = $request->getPostParameter($sChamp, null);
            if ((($sValeur == null) || ($sValeur == $aChamp['label'] . '*')) && $bObligatoire == '1') {
                $bEnvoiPossible = false;
                $aErreurs[] = __('Required field') . ' : ' . $aChamp['label'];
                $aChampsErreur[] = $aChamp['nom'];
            }
            elseif ($sValeur == $aChamp['label']) {
                $aDonneesEnvoyees['datas[' . $sChamp . ']'] = '';
            }
            else {
                $aDonneesEnvoyees['datas[' . $sChamp . ']'] = $sValeur;
            }
        }
        if ($bEnvoiPossible) {
            $oCURLSession = curl_init($this->_sUrlStructure . 'Submit');
            curl_setopt($oCURLSession, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($oCURLSession, CURLOPT_POST, true);
            curl_setopt($oCURLSession, CURLOPT_POSTFIELDS, $aDonneesEnvoyees);
            curl_setopt($oCURLSession, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
            if (sfConfig::get('app_contactform_htuser') && sfConfig::get('app_contactform_htpass'))
                curl_setopt($oCURLSession, CURLOPT_USERPWD, sfConfig::get('app_contactform_htuser') . ':' . sfConfig::get('app_contactform_htpass'));
            $sRetourCurl = curl_exec($oCURLSession);
            curl_close($oCURLSession);

            $aRetour = array('retour' => 'ok', 'message' => __('Your request has been successfully sent.'), 'debug' => $sRetourCurl);
        }
        else {
            $aRetour = array('retour' => 'nok', 'message' => implode("\\n", $aErreurs), 'champs' => $aChampsErreur);
        }
        return $aRetour;
    }
}