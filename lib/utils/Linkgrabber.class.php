<?php

class Linkgrabber {
    
    const NOLINK = '';
    const INTERNALLINK = 'interne';
    const EXTERNALLINK = 'externe';
    
    public function __construct() {
    }
    
    public function getSluglikeLinks($bOnlyStatics = false) {
        $aRetour = array(
            ''                                                          => '',
            'statique;' . VeuveTranslator::STATIQUE_STORE_LOCATOR       => 'Store Locator',
            'statique;' . VeuveTranslator::STATIQUE_PRESSE              => 'Presse',
            'statique;' . VeuveTranslator::STATIQUE_CONTACT             => 'Contact',
            'article-cle;' . VeuveTranslator::ARTICLE_KEY_DATES         => 'Articles spéciaux > Dates clés',
            'article-cle;' . VeuveTranslator::ARTICLE_PRODUCTION_CYCLE  => 'Articles spéciaux > Cycle de production',
            'article-cle;' . VeuveTranslator::ARTICLE_WOMEN_INSPIRATION => 'Articles spéciaux > Women of inspiration',
            'article-cle;' . VeuveTranslator::ARTICLE_GREAT_EXPEDITIONS => 'Articles spéciaux > Grandes expéditions',
            'article-cle;' . VeuveTranslator::ARTICLE_CARNET_VENDANGES  => 'Articles spéciaux > Carnet de vendanges',
            'article-cle;' . VeuveTranslator::ARTICLE_WE_ARE_CLICQUOT   => 'Articles spéciaux > We Are Clicquot',
        );
        
        if ($bOnlyStatics) {
            asort($aRetour, SORT_STRING);
            return $aRetour;
        }
        
        // Pages statiques
        $qStatiques = Doctrine::getTable('PageStatique')->findAll();
        foreach($qStatiques as $oStatique) {
            $urlNonFormatee = 'statique-slug;' . $oStatique->getId();
            $titreStatique = $oStatique->getTitre();
            $aRetour[$urlNonFormatee] = $titreStatique;
        }
        
        // Articles
        
        // Récupération des articles ne pouvant être affichés seuls car liés à une page spéciale
        $aArticlesNonSolos = array();
        // Page WOI
        $oArticleWOI = Doctrine::getTable('WomenPage')->findOneBy('active', 1);
        if ($oArticleWOI) $aArticlesNonSolos[] = $oArticleWOI->getArticleId();
        // Page GE
        $oArticleGE = Doctrine::getTable('GreatExpeditionPage')->findOneBy('active', 1);
        if ($oArticleGE) $aArticlesNonSolos[] = $oArticleGE->getArticleId();
        
        $qArticles = Doctrine::getTable('Article')
                             ->createQuery('a')
                             ->whereNotIn('a.id', $aArticlesNonSolos)
                             ->execute();
        foreach($qArticles as $oArticle) {
            $urlNonFormatee = 'article;' . $oArticle->getId();
            $titreArticle = 'Articles > ' . $oArticle->getTitre();
            $aRetour[$urlNonFormatee] = $titreArticle;
        }
        
        // Fiches produits
        $qFichesProduits = Doctrine::getTable('FicheProduit')->findAll();
        foreach($qFichesProduits as $oFicheProduit) {
            $urlNonFormatee = 'fiche-produit;' . $oFicheProduit->getId();
            $titreFiche = 'Fiches produits > ' . $oFicheProduit->getTitre();
            $aRetour[$urlNonFormatee] = $titreFiche;
        }
        
        // Offres
        $qOffres = Doctrine::getTable('Offre')->findAll();
        foreach($qOffres as $oOffre) {
            $urlNonFormatee = 'offre;' . $oOffre->getId();
            $titreOffre = 'Offres > ' . $oOffre->getTitre();
            $aRetour[$urlNonFormatee] = $titreOffre;
        }
        
        asort($aRetour, SORT_STRING);
        
        return $aRetour;
    }
    
    private function _generateStaticLink($sLangue, $sStatique, $sSlug = null) {
        if ($sSlug == null) $sSlug = VeuveTranslator::getStatiqueSlug($sStatique, $sLangue);
        return url_for('@nocategory?sf_culture=' . $sLangue . '&pageslug=' . $sSlug);
    }
    
    private function _generateArticleLink($sLangue, $sSlug) {
        $sCategory = VeuveTranslator::getCategorySlug(VeuveTranslator::CATEGORY_HOUSE, $sLangue);
        return url_for('@category?sf_culture=' . $sLangue . '&categoryslug=' . $sCategory . '&pageslug=' . $sSlug);
    }
    
    private function _generateChampagneLink($sLangue, $sSlug) {
        $sCategory = VeuveTranslator::getCategorySlug(VeuveTranslator::CATEGORY_CHAMPAGNE, $sLangue);
        return url_for('@category?sf_culture=' . $sLangue . '&categoryslug=' . $sCategory . '&pageslug=' . $sSlug);
    }
    
    public function generateLinks($aTableauIdentifiants, $sLangue) {
        $aArticlesId = array();
        $aFichesId = array();
        $aOffresId = array();
        $aRetours = array();
        $aStatiquesId = array();
        foreach($aTableauIdentifiants as $sIdentifiant) {
            $aParties = explode(';', $sIdentifiant);
            switch($aParties[0]) {
                case 'statique':
                    $aRetours[$sIdentifiant] = $this->_generateStaticLink($sLangue, $aParties[1]);
                    break;
                case 'article-cle':
                    $aRetours[$sIdentifiant] = $this->_generateArticleLink($sLangue, VeuveTranslator::getArticleSlug($aParties[1], $sLangue));
                    break;
                case 'article':
                    $aArticlesId[] = $aParties[1];
                    break;
                case 'fiche-produit':
                    $aFichesId[] = $aParties[1];
                    break;
                case 'offre':
                    $aOffresId[] = $aParties[1];
                    break;
                case 'statique-slug':
                    $aStatiquesId[] = $aParties[1];
                    break;
            }
        }
        
        $qArticles = Doctrine::getTable('Article')
                             ->createQuery('a')
                             ->innerJoin('a.Translation t')
                             ->whereIn('t.id', $aArticlesId)
                             ->andWhere('t.lang = ?', $sLangue)
                             ->execute();
        foreach ($qArticles as $oArticle) {
            $sIdentifiant = 'article;' . $oArticle->getId();
            $aRetours[$sIdentifiant] = $this->_generateArticleLink($sLangue, $oArticle->getSlug());
        }
        
        $qFiches = Doctrine::getTable('FicheProduit')
                           ->createQuery('f')
                           ->innerJoin('f.Translation t')
                           ->whereIn('t.id', $aFichesId)
                           ->andWhere('t.lang = ?', $sLangue)
                           ->execute();
        foreach ($qFiches as $oFiche) {
            $sIdentifiant = 'fiche-produit;' . $oFiche->getId();
            $aRetours[$sIdentifiant] = $this->_generateChampagneLink($sLangue, $oFiche->getSlug());
        }
        
        $qOffres = Doctrine::getTable('Offre')
                           ->createQuery('o')
                           ->innerJoin('o.Translation t')
                           ->whereIn('t.id', $aOffresId)
                           ->andWhere('t.lang = ?', $sLangue)
                           ->execute();
        foreach ($qOffres as $oOffre) {
            $sIdentifiant = 'offre;' . $oOffre->getId();
            $aRetours[$sIdentifiant] = $this->_generateChampagneLink($sLangue, $oOffre->getSlug());
        }
        
        $qStatiques = Doctrine::getTable('PageStatique')
                              ->createQuery('s')
                              ->innerJoin('s.Translation t')
                              ->whereIn('s.id', $aStatiquesId)
                              ->andWhere('t.lang = ?', $sLangue)
                              ->execute();
        foreach ($qStatiques as $oStatique) {
            $sIdentifiant = 'statique-slug;' . $oStatique->getId();
            $aRetours[$sIdentifiant] = $this->_generateStaticLink($sLangue, null, $oStatique->getSlug());
        }
        
        return $aRetours;
    }
    
    public static function generateAgegateLinks($sLien, sfUser $oUser) {
        $aDDN = $oUser->getAttribute('ddn');
        $oPays = Doctrine::getTable('Pays')->findOneBy('slug', $oUser->getAttribute('pays'));
        $aRemplacements = array(
            '#PAYS#' => $oPays->getCodeiso(),
            '#LANGUE#' => $oUser->getCulture(),
            '#ANNEE#' => $aDDN['annee'],
            '#MOIS#' => $aDDN['mois'],
            '#JOUR#' => $aDDN['jour'],
        );
        return strtr($sLien, $aRemplacements);
    }

    public function replaceDesktopLinks($sLien, $sLangue) {
        $aTableauCorrespondances = array(
            'article-cle;carnet-de-vendanges' => array(
                'http://www.veuve-clicquot.com/en/house/art-wine#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/de/unser-haus/winzerkunst#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/es/empresa/art-wine#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/fr/maison/art-de-la-vigne#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/it/la-casa/larte-della-vigna#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/ja/art-wine#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/ko/house-ko/wainmeikingyi-jeongsu#scroll-to-vendanges01',
                'http://www.veuve-clicquot.com/pt/casa/arte-da-vinha#scroll-to-vendanges01',
                '/en/house/art-wine#scroll-to-vendanges01',
                '/de/unser-haus/winzerkunst#scroll-to-vendanges01',
                '/es/empresa/art-wine#scroll-to-vendanges01',
                '/fr/maison/art-de-la-vigne#scroll-to-vendanges01',
                '/it/la-casa/larte-della-vigna#scroll-to-vendanges01',
                '/ja/art-wine#scroll-to-vendanges01',
                '/ko/house-ko/wainmeikingyi-jeongsu#scroll-to-vendanges01',
                '/pt/casa/arte-da-vinha#scroll-to-vendanges01',
            ),
            'article-cle;women-of-inspiration' => array(
                '/fr/maison/women-of-inspiration#scroll-to-prix2014',
                'http://www.veuve-clicquot.com/fr/maison/women-of-inspiration#scroll-to-prix2014',
                '/fr/maison/women-of-inspiration',
                'http://www.veuve-clicquot.com/fr/maison/women-of-inspiration',
            ),
            'article-cle;we-are-clicquot' => array(
                '/fr/maison/art-de-la-vigne#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/fr/maison/we-are-clicquot',
                '/it/la-casa/larte-della-vigna#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/it/la-casa/we-are-clicquot',
                '/en/house/art-wine#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/en/house/we-are-clicquot',
                '/es/empresa/art-wine#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/es/empresa/we-are-clicquot',
                '/ja/art-wine#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/ja/house/we-are-clicquot',
                '/ko/house-ko/wainmeikingyi-jeongsu#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/ko/house/we-are-clicquot',
                '/pt/casa/arte-da-vinha#scroll-to-caroussel-we-are-clicquot?episode=1',
                'http://www.veuve-clicquot.com/pt/casa/we-are-clicquot',
            )
        );
        foreach($aTableauCorrespondances as $sCle => $aValeurs) {
            foreach($aValeurs as $sValeur) {
                if (strpos($sLien, $sValeur) !== FALSE) {
                    $aLiens = $this->generateLinks(array($sCle), $sLangue);
                    $sLien = str_replace($sValeur, $aLiens[$sCle], $sLien);
                }
            }
        }
        return $sLien;
    }
    
}