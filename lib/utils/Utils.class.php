<?php

class Utils {

    public static function truncate($string, $max = 60) {
        if (strlen($string) >= $max) {
            $string = substr($string, 0, $max);
            $espace = strrpos($string, " ");
            if ($espace)
                $string = substr($string, 0, $espace);
            $string .= '...';
        }
        return $string;
    }

    public static function startsWith($haystack, $needle) {
        $res = false;
        $needle = is_array($needle) ? $needle : array($needle);
        foreach($needle as $n){
            $length = strlen($n);
            if((substr($haystack, 0, $length) === $n)){
               $res = true;
               break;
            }
        }
        return $res;
    }

}