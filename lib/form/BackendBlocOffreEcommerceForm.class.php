<?php

class BackendBlocOffreEcommerceForm extends BlocOffreEcommerceForm {
    public function configure() {
        parent::configure();
        unset($this['bloc_offre_id']);
        $this->widgetSchema['pays_id']->setOption('multiple', true);
        $this->widgetSchema['langue_id']->setOption('multiple', true);

        $this->validatorSchema['pays_id']->setOption('multiple', true);
        $this->validatorSchema['langue_id']->setOption('multiple', true);
    }
}