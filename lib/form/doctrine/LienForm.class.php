<?php

/**
 * Lien form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class LienForm extends BaseLienForm {
    public function configure() {
        $aTypesLiens = Lien::getTypesLiens();
        $oLinkGrabber = new Linkgrabber();
        $aLinkGrab = $oLinkGrabber->getSluglikeLinks();
        
        $this->widgetSchema['type_lien'] = new sfWidgetFormChoice(array('choices' => $aTypesLiens));
        $this->widgetSchema['lien_interne'] = new sfWidgetFormChoice(array('choices' => $aLinkGrab));
        $this->widgetSchema['pays_list']->setOption('label', 'Pays non autorisés');
        
        $this->validatorSchema['type_lien'] = new sfValidatorChoice(array('choices' => array_keys($aTypesLiens)));
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
