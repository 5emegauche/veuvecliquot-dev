<?php

/**
 * GreatExpeditionTranslation form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class GreatExpeditionTranslationForm extends BaseGreatExpeditionTranslationForm {
    public function configure() {
        $this->widgetSchema['texte'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
    }
}
