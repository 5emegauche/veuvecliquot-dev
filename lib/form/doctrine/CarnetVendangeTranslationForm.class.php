<?php

/**
 * CarnetVendangeTranslation form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CarnetVendangeTranslationForm extends BaseCarnetVendangeTranslationForm {
    public function configure() {
        $this->widgetSchema['texte1'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
        $this->widgetSchema['texte2'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
        $this->widgetSchema['texte3'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
    }
}
