<?php

/**
 * NoteFicheProduit form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class NoteFicheProduitForm extends BaseNoteFicheProduitForm {
    public function configure() {
        $this->widgetSchema['fiche_produit_id'] = new sfWidgetFormInputHidden();
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
