<?php

/**
 * ProductionCycleTranslation form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductionCycleTranslationForm extends BaseProductionCycleTranslationForm {
    public function configure() {
        $this->widgetSchema['image'] = new sfWidgetFormInputFileEditable(array(
            'file_src' => sfConfig::get('app_chemin_production') . $this->getObject()->image,
            'is_image' => true,
            'edit_mode' => ! $this->isNew(),
            'template' => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>'
        ));
        
        $this->validatorSchema['image'] = new sfValidatorFile(array(
            'required' => false,
            'path' => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_production'),
            'mime_types' => 'web_images'
        ));
        $this->validatorSchema['image_delete'] = new sfValidatorPass();
    }
}
