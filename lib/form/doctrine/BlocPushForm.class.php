<?php

/**
 * BlocPush form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BlocPushForm extends BaseBlocPushForm {
    public function configure() {
        $aTypesLiens = Lien::getShortTypesLiens();
        $oLinkGrabber = new Linkgrabber();
        $aLinkGrab = $oLinkGrabber->getSluglikeLinks();
        
        $this->widgetSchema['type_lien'] = new sfWidgetFormChoice(array('choices' => $aTypesLiens));
        $this->widgetSchema['lien_interne'] = new sfWidgetFormChoice(array('choices' => $aLinkGrab));
        $this->widgetSchema['image'] = new sfWidgetFormInputFileEditable(array(
            'file_src' => $this->getObject()->getUrlImage(),
            'is_image' => true,
            'edit_mode' => ! $this->isNew(),
            'template' => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        $this->widgetSchema->setHelp('image', '724 &times; 500px');
        
        $this->validatorSchema['type_lien'] = new sfValidatorChoice(array('choices' => array_keys($aTypesLiens)));
        $this->validatorSchema['image'] = new sfValidatorFile(array(
            'required' => false,
            'path' => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_blocpush'),
        ));
        $this->validatorSchema['image_delete'] = new sfValidatorPass();
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
