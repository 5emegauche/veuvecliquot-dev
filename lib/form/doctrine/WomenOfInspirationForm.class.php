<?php

/**
 * WomenOfInspiration form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class WomenOfInspirationForm extends BaseWomenOfInspirationForm {
    public function configure() {
        $this->widgetSchema['photo'] = new sfWidgetFormInputFileEditable(array(
            'file_src'  => $this->getObject()->getUrlPhoto(),
            'is_image'  => true,
            'edit_mode' => ! $this->isNew(),
            'template'  => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        
        $this->validatorSchema['photo'] = new sfValidatorFile(array(
            'required' => false,
            'path'     => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_women'),
        ));
        $this->validatorSchema['photo_delete'] = new sfValidatorPass();
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
