<?php

/**
 * BlocArticle form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BlocArticleForm extends BaseBlocArticleForm {
    public function configure() {
        parent::configure();
        $this->widgetSchema['type'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['article_id'] = new sfWidgetFormInputHidden();
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
