<?php

/**
 * PageStatique form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PageStatiqueForm extends BasePageStatiqueForm {
    public function configure() {
        $aPagesStatiques = PageStatique::getListeStatiques();
        
        $this->widgetSchema['page_id'] = new sfWidgetFormChoice(array('choices' => $aPagesStatiques, 'label' => 'Page'));
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
