<?php

/**
 * FicheProduitTranslation form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FicheProduitTranslationForm extends BaseFicheProduitTranslationForm {
    public function configure() {
        $this->validatorSchema['titre'] = new sfValidatorString(array('required' => false));
    }
}
