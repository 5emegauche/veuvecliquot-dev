<?php

/**
 * Offre form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OffreForm extends BaseOffreForm {
    public function configure() {
        $aIdsProduits = FicheProduit::getListeProduits();
        
        $this->widgetSchema['idproduit'] = new sfWidgetFormChoice(array('choices' => $aIdsProduits));
        
        $this->widgetSchema['idproduit']->setOption('label', 'Produit');
        
        $this->validatorSchema['idproduit']    = new sfValidatorChoice(array('choices' => array_keys($aIdsProduits), 'required' => true));
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
