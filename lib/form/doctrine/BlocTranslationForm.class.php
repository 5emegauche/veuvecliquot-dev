<?php

/**
 * BlocTranslation form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BlocTranslationForm extends BaseBlocTranslationForm {
    public function configure() {
        $oVideo = new VideoLister(sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_videos'));
        $aVideos = $oVideo->getVideoList();
        $this->widgetSchema['texte'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
        $this->widgetSchema['video'] = new sfWidgetFormChoice(array('choices' => $aVideos));
        
        $this->validatorSchema['video'] = new sfValidatorChoice(array('choices' => array_keys($aVideos), 'required' => false));
    }
}
