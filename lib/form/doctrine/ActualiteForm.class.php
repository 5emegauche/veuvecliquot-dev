<?php

/**
 * Actualite form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ActualiteForm extends BaseActualiteForm {
    public function configure() {
        $this->widgetSchema['texte'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
        $this->widgetSchema['bloc_texte'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300));
        /*$this->widgetSchema['lang'] = new sfWidgetFormChoice(array('choices' => sfConfig::get('app_cultures')));
        
        $this->validatorSchema['lang'] = new sfValidatorChoice(array('choices' => array_keys(sfConfig::get('app_cultures')), 'required' => true));*/
    }
}
