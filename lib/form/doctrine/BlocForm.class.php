<?php

/**
 * Bloc form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BlocForm extends BaseBlocForm {
    public function configure() {
        $aTypesBlocs = Bloc::getListeTypesBloc();
        $this->widgetSchema['ordre']     = new sfWidgetFormInputHidden();
        $this->widgetSchema['type_bloc'] = new sfWidgetFormChoice(array('choices' => $aTypesBlocs));
        $this->widgetSchema['image']     = new sfWidgetFormInputFileEditable(array(
                'file_src'  => $this->getObject()->getUrlImage(),
                'is_image'  => true,
                'edit_mode' => ! $this->isNew(),
                'template'  => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        $this->widgetSchema['image2']     = new sfWidgetFormInputFileEditable(array(
                'file_src'  => $this->getObject()->getUrlImage2(),
                'is_image'  => true,
                'edit_mode' => ! $this->isNew(),
                'template'  => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        
        $this->validatorSchema['type_bloc']     = new sfValidatorChoice(array('choices' => array_keys($aTypesBlocs), 'required' => true));
        $this->validatorSchema['image']         = new sfValidatorFile(array(
                'required' => false,
                'path'     => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_offres'),
        ));
        $this->validatorSchema['image2']        = new sfValidatorFile(array(
                'required' => false,
                'path'     => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_offres'),
        ));
        $this->validatorSchema['image_delete']  = new sfValidatorPass();
        $this->validatorSchema['image2_delete'] = new sfValidatorPass();
    }
}
