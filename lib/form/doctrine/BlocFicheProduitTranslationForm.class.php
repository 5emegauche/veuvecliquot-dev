<?php

/**
 * BlocFicheProduitTranslation form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BlocFicheProduitTranslationForm extends BaseBlocFicheProduitTranslationForm {
	public function configure() {
		$this->widgetSchema['contenu'] = new sfWidgetFormTextareaTinyMCE(array('width' => 450, 'height' => 300, 'config' => sfConfig::get('app_editeurs_bloc_fiche_produit')));
	}
}
