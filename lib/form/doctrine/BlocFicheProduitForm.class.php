<?php

/**
 * BlocFicheProduit form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BlocFicheProduitForm extends BaseBlocFicheProduitForm {
    public function configure() {
        $this->widgetSchema['fiche_produit_id'] = new sfWidgetFormInputHidden();
        $this->widgetSchema['ordre']            = new sfWidgetFormInputHidden();
        
        $this->embedI18n(array('en', 'de', 'es', 'fr', 'it', 'ja', 'ko', 'pt', 'ch'));
    }
}
