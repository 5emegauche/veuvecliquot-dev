<?php

/**
 * GreatExpedition form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class GreatExpeditionForm extends BaseGreatExpeditionForm {
    public function configure() {
        $aTypes = GreatExpedition::getTableTypes();
        $this->widgetSchema['image'] = new sfWidgetFormInputFileEditable(array(
            'file_src'  => $this->getObject()->getUrlImage(),
            'is_image'  => true,
            'edit_mode' => ! $this->isNew(),
            'template'  => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        $this->widgetSchema['type_bloc'] = new sfWidgetFormChoice(array('choices' => $aTypes));
        
        $this->validatorSchema['image'] = new sfValidatorFile(array(
            'required' => false,
            'path'     => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_expedition'),
        ));
        $this->validatorSchema['image_delete'] = new sfValidatorPass();
        $this->validatorSchema['type_bloc'] = new sfValidatorChoice(array('choices' => array_keys($aTypes), 'required' => true));
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
