<?php

/**
 * FicheProduit form.
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class FicheProduitForm extends BaseFicheProduitForm {
    public function configure() {
        $aIdsProduits = FicheProduit::getListeProduits();
        
        $this->widgetSchema['idproduit'] = new sfWidgetFormChoice(array('choices' => $aIdsProduits));
        $this->widgetSchema['image']     = new sfWidgetFormInputFileEditable(array(
            'file_src' => $this->getObject()->getUrlImage(),
            'is_image' => true,
            'edit_mode' => ! $this->isNew(),
            'template' => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        $this->widgetSchema['image_note'] = new sfWidgetFormInputFileEditable(array(
            'file_src' => $this->getObject()->getUrlImageNote(),
            'is_image' => true,
            'edit_mode' => ! $this->isNew(),
            'template' => '<div>%file%<br />%input%<br />%delete% supprimer le fichier</div>',
        ));
        
        $this->widgetSchema['idproduit']->setOption('label', 'Produit');
        
        $this->widgetSchema->setHelp('image', '536px &times; 2000px');
        $this->widgetSchema->setHelp('image_note', '1476 &times; 500px');
        
        $this->validatorSchema['idproduit'] = new sfValidatorChoice(array('choices' => array_keys($aIdsProduits), 'required' => true));
        $this->validatorSchema['image'] = new sfValidatorFile(array(
            'required' => false,
            'path' => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_ficheproduit'),
        ));
        $this->validatorSchema['image_note'] = new sfValidatorFile(array(
                'required' => false,
                'path' => sfConfig::get('sf_web_dir') . sfConfig::get('app_chemin_noteproduit'),
        ));
        $this->validatorSchema['image_delete'] = new sfValidatorPass();
        $this->validatorSchema['image_note_delete'] = new sfValidatorPass();
        
        $this->embedI18n(array_keys(sfConfig::get('app_cultures')));
    }
}
