<?php

/**
 * AssocStatiqueBlocPush form base class.
 *
 * @method AssocStatiqueBlocPush getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAssocStatiqueBlocPushForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'statique_id'  => new sfWidgetFormInputHidden(),
      'bloc_push_id' => new sfWidgetFormInputHidden(),
      'ordre'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'statique_id'  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('statique_id')), 'empty_value' => $this->getObject()->get('statique_id'), 'required' => false)),
      'bloc_push_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('bloc_push_id')), 'empty_value' => $this->getObject()->get('bloc_push_id'), 'required' => false)),
      'ordre'        => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('assoc_statique_bloc_push[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AssocStatiqueBlocPush';
  }

}
