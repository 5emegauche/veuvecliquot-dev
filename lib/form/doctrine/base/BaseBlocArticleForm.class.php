<?php

/**
 * BlocArticle form base class.
 *
 * @method BlocArticle getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBlocArticleForm extends BlocForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('bloc_article[%s]');
  }

  public function getModelName()
  {
    return 'BlocArticle';
  }

}
