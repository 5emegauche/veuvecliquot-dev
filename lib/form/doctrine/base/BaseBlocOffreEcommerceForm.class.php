<?php

/**
 * BlocOffreEcommerce form base class.
 *
 * @method BlocOffreEcommerce getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBlocOffreEcommerceForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'bloc_offre_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('BlocOffre'), 'add_empty' => false)),
      'pays_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Pays'), 'add_empty' => true)),
      'langue_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Langue'), 'add_empty' => true)),
      'autorise'      => new sfWidgetFormInputCheckbox(),
      'lien'          => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'bloc_offre_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('BlocOffre'))),
      'pays_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Pays'), 'required' => false)),
      'langue_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Langue'), 'required' => false)),
      'autorise'      => new sfValidatorBoolean(array('required' => false)),
      'lien'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_offre_ecommerce[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocOffreEcommerce';
  }

}
