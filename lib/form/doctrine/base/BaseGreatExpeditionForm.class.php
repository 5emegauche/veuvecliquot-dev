<?php

/**
 * GreatExpedition form base class.
 *
 * @method GreatExpedition getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseGreatExpeditionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'type_bloc' => new sfWidgetFormInputText(),
      'image'     => new sfWidgetFormInputText(),
      'annee'     => new sfWidgetFormInputText(),
      'ordre'     => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type_bloc' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'image'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'annee'     => new sfValidatorInteger(array('required' => false)),
      'ordre'     => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('great_expedition[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'GreatExpedition';
  }

}
