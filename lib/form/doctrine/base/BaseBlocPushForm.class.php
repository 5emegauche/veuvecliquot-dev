<?php

/**
 * BlocPush form base class.
 *
 * @method BlocPush getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBlocPushForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'description'  => new sfWidgetFormInputText(),
      'image'        => new sfWidgetFormInputText(),
      'type_lien'    => new sfWidgetFormInputText(),
      'lien_interne' => new sfWidgetFormInputText(),
      'lien_externe' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'description'  => new sfValidatorString(array('max_length' => 255)),
      'image'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'type_lien'    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'lien_interne' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'lien_externe' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_push[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocPush';
  }

}
