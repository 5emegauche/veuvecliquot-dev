<?php

/**
 * PaysLangue form base class.
 *
 * @method PaysLangue getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePaysLangueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'pays_id'   => new sfWidgetFormInputHidden(),
      'langue_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'pays_id'   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('pays_id')), 'empty_value' => $this->getObject()->get('pays_id'), 'required' => false)),
      'langue_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('langue_id')), 'empty_value' => $this->getObject()->get('langue_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('pays_langue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaysLangue';
  }

}
