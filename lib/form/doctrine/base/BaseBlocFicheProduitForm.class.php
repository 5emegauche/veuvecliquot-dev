<?php

/**
 * BlocFicheProduit form base class.
 *
 * @method BlocFicheProduit getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBlocFicheProduitForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'fiche_produit_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('FicheProduit'), 'add_empty' => false)),
      'description'      => new sfWidgetFormInputText(),
      'ancre'            => new sfWidgetFormInputText(),
      'ordre'            => new sfWidgetFormInputText(),
      'ecommerce'        => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'fiche_produit_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('FicheProduit'))),
      'description'      => new sfValidatorString(array('max_length' => 255)),
      'ancre'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ordre'            => new sfValidatorInteger(array('required' => false)),
      'ecommerce'        => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc_fiche_produit[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'BlocFicheProduit';
  }

}
