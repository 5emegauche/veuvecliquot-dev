<?php

/**
 * Bloc form base class.
 *
 * @method Bloc getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBlocForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'type_bloc'  => new sfWidgetFormInputText(),
      'image'      => new sfWidgetFormInputText(),
      'image2'     => new sfWidgetFormInputText(),
      'ancre'      => new sfWidgetFormInputText(),
      'ordre'      => new sfWidgetFormInputText(),
      'type'       => new sfWidgetFormInputText(),
      'ecommerce'  => new sfWidgetFormInputCheckbox(),
      'offre_id'   => new sfWidgetFormInputText(),
      'article_id' => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type_bloc'  => new sfValidatorString(array('max_length' => 255)),
      'image'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'image2'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ancre'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ordre'      => new sfValidatorInteger(array('required' => false)),
      'type'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ecommerce'  => new sfValidatorBoolean(array('required' => false)),
      'offre_id'   => new sfValidatorInteger(array('required' => false)),
      'article_id' => new sfValidatorInteger(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('bloc[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Bloc';
  }

}
