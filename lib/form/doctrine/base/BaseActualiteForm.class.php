<?php

/**
 * Actualite form base class.
 *
 * @method Actualite getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseActualiteForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                     => new sfWidgetFormInputHidden(),
      'categorie'              => new sfWidgetFormInputText(),
      'titre'                  => new sfWidgetFormInputText(),
      'texte'                  => new sfWidgetFormTextarea(),
      'image'                  => new sfWidgetFormInputText(),
      'bloc_titre'             => new sfWidgetFormInputText(),
      'bloc_texte'             => new sfWidgetFormTextarea(),
      'bloc_image'             => new sfWidgetFormInputText(),
      'lang'                   => new sfWidgetFormInputText(),
      'video'                  => new sfWidgetFormInputText(),
      'custom_label_read_more' => new sfWidgetFormInputText(),
      'custom_read_more'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'categorie'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'titre'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'texte'                  => new sfValidatorString(array('required' => false)),
      'image'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'bloc_titre'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'bloc_texte'             => new sfValidatorString(array('required' => false)),
      'bloc_image'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'lang'                   => new sfValidatorString(array('max_length' => 5)),
      'video'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'custom_label_read_more' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'custom_read_more'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actualite[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Actualite';
  }

}
