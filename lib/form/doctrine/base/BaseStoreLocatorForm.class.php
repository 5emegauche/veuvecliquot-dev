<?php

/**
 * StoreLocator form base class.
 *
 * @method StoreLocator getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseStoreLocatorForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'nom'         => new sfWidgetFormInputText(),
      'ville'       => new sfWidgetFormInputText(),
      'adresse'     => new sfWidgetFormInputText(),
      'code_postal' => new sfWidgetFormInputText(),
      'pays'        => new sfWidgetFormInputText(),
      'ordre'       => new sfWidgetFormInputText(),
      'image'       => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nom'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ville'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'adresse'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'code_postal' => new sfValidatorString(array('max_length' => 20, 'required' => false)),
      'pays'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'ordre'       => new sfValidatorInteger(array('required' => false)),
      'image'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('store_locator[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'StoreLocator';
  }

}
