<?php

/**
 * ActualiteSlide form base class.
 *
 * @method ActualiteSlide getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseActualiteSlideForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'actualite_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Actualite'), 'add_empty' => false)),
      'ordre'        => new sfWidgetFormInputText(),
      'image'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'actualite_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Actualite'))),
      'ordre'        => new sfValidatorInteger(array('required' => false)),
      'image'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('actualite_slide[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ActualiteSlide';
  }

}
