<?php

/**
 * CarnetVendangeTranslation form base class.
 *
 * @method CarnetVendangeTranslation getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCarnetVendangeTranslationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'     => new sfWidgetFormInputHidden(),
      'titre'  => new sfWidgetFormInputText(),
      'date1'  => new sfWidgetFormInputText(),
      'texte1' => new sfWidgetFormTextarea(),
      'date2'  => new sfWidgetFormInputText(),
      'texte2' => new sfWidgetFormTextarea(),
      'date3'  => new sfWidgetFormInputText(),
      'texte3' => new sfWidgetFormTextarea(),
      'lang'   => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'id'     => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'titre'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'date1'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'texte1' => new sfValidatorString(array('required' => false)),
      'date2'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'texte2' => new sfValidatorString(array('required' => false)),
      'date3'  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'texte3' => new sfValidatorString(array('required' => false)),
      'lang'   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('lang')), 'empty_value' => $this->getObject()->get('lang'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('carnet_vendange_translation[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CarnetVendangeTranslation';
  }

}
