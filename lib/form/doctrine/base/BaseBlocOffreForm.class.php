<?php

/**
 * BlocOffre form base class.
 *
 * @method BlocOffre getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseBlocOffreForm extends BlocForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema->setNameFormat('bloc_offre[%s]');
  }

  public function getModelName()
  {
    return 'BlocOffre';
  }

}
