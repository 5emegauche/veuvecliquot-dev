<?php

/**
 * LienPays form base class.
 *
 * @method LienPays getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseLienPaysForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'pays_id' => new sfWidgetFormInputHidden(),
      'lien_id' => new sfWidgetFormInputHidden(),
    ));

    $this->setValidators(array(
      'pays_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('pays_id')), 'empty_value' => $this->getObject()->get('pays_id'), 'required' => false)),
      'lien_id' => new sfValidatorChoice(array('choices' => array($this->getObject()->get('lien_id')), 'empty_value' => $this->getObject()->get('lien_id'), 'required' => false)),
    ));

    $this->widgetSchema->setNameFormat('lien_pays[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'LienPays';
  }

}
