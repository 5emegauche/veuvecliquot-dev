<?php

/**
 * Pays form base class.
 *
 * @method Pays getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePaysForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'nom'               => new sfWidgetFormInputText(),
      'age_legal'         => new sfWidgetFormInputText(),
      'autorise'          => new sfWidgetFormInputCheckbox(),
      'affiche_facebook'  => new sfWidgetFormInputCheckbox(),
      'facebook'          => new sfWidgetFormInputText(),
      'affiche_youtube'   => new sfWidgetFormInputCheckbox(),
      'youtube'           => new sfWidgetFormInputText(),
      'affiche_twitter'   => new sfWidgetFormInputCheckbox(),
      'twitter'           => new sfWidgetFormInputText(),
      'affiche_pinterest' => new sfWidgetFormInputCheckbox(),
      'pinterest'         => new sfWidgetFormInputText(),
      'codeiso'           => new sfWidgetFormInputText(),
      'ordre'             => new sfWidgetFormInputText(),
      'ecommerce'         => new sfWidgetFormInputCheckbox(),
      'lien_ecommerce'    => new sfWidgetFormInputText(),
      'slug'              => new sfWidgetFormInputText(),
      'langue_list'       => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Langue')),
      'liens_list'        => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Lien')),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'nom'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'age_legal'         => new sfValidatorInteger(array('required' => false)),
      'autorise'          => new sfValidatorBoolean(array('required' => false)),
      'affiche_facebook'  => new sfValidatorBoolean(array('required' => false)),
      'facebook'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'affiche_youtube'   => new sfValidatorBoolean(array('required' => false)),
      'youtube'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'affiche_twitter'   => new sfValidatorBoolean(array('required' => false)),
      'twitter'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'affiche_pinterest' => new sfValidatorBoolean(array('required' => false)),
      'pinterest'         => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'codeiso'           => new sfValidatorString(array('max_length' => 5, 'required' => false)),
      'ordre'             => new sfValidatorInteger(array('required' => false)),
      'ecommerce'         => new sfValidatorBoolean(array('required' => false)),
      'lien_ecommerce'    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'slug'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'langue_list'       => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Langue', 'required' => false)),
      'liens_list'        => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Lien', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Pays', 'column' => array('slug')))
    );

    $this->widgetSchema->setNameFormat('pays[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Pays';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['langue_list']))
    {
      $this->setDefault('langue_list', $this->object->Langue->getPrimaryKeys());
    }

    if (isset($this->widgetSchema['liens_list']))
    {
      $this->setDefault('liens_list', $this->object->Liens->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->saveLangueList($con);
    $this->saveLiensList($con);

    parent::doSave($con);
  }

  public function saveLangueList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['langue_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Langue->getPrimaryKeys();
    $values = $this->getValue('langue_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Langue', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Langue', array_values($link));
    }
  }

  public function saveLiensList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['liens_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Liens->getPrimaryKeys();
    $values = $this->getValue('liens_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Liens', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Liens', array_values($link));
    }
  }

}
