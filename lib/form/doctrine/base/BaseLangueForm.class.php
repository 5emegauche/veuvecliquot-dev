<?php

/**
 * Langue form base class.
 *
 * @method Langue getObject() Returns the current form's model object
 *
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseLangueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'code'           => new sfWidgetFormInputText(),
      'nom'            => new sfWidgetFormInputText(),
      'nom_webservice' => new sfWidgetFormInputText(),
      'pays_list'      => new sfWidgetFormDoctrineChoice(array('multiple' => true, 'model' => 'Pays')),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'           => new sfValidatorString(array('max_length' => 2, 'required' => false)),
      'nom'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'nom_webservice' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'pays_list'      => new sfValidatorDoctrineChoice(array('multiple' => true, 'model' => 'Pays', 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Langue', 'column' => array('code')))
    );

    $this->widgetSchema->setNameFormat('langue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Langue';
  }

  public function updateDefaultsFromObject()
  {
    parent::updateDefaultsFromObject();

    if (isset($this->widgetSchema['pays_list']))
    {
      $this->setDefault('pays_list', $this->object->Pays->getPrimaryKeys());
    }

  }

  protected function doSave($con = null)
  {
    $this->savePaysList($con);

    parent::doSave($con);
  }

  public function savePaysList($con = null)
  {
    if (!$this->isValid())
    {
      throw $this->getErrorSchema();
    }

    if (!isset($this->widgetSchema['pays_list']))
    {
      // somebody has unset this widget
      return;
    }

    if (null === $con)
    {
      $con = $this->getConnection();
    }

    $existing = $this->object->Pays->getPrimaryKeys();
    $values = $this->getValue('pays_list');
    if (!is_array($values))
    {
      $values = array();
    }

    $unlink = array_diff($existing, $values);
    if (count($unlink))
    {
      $this->object->unlink('Pays', array_values($unlink));
    }

    $link = array_diff($values, $existing);
    if (count($link))
    {
      $this->object->link('Pays', array_values($link));
    }
  }

}
