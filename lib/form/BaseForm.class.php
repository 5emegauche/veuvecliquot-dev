<?php

/**
 * Base project form.
 * 
 * @package    veuveclicquot
 * @subpackage form
 * @author     Your name here 
 * @version    SVN: $Id: BaseForm.class.php 20147 2009-07-13 11:46:57Z FabianLange $
 */
class BaseForm extends sfFormSymfony {
    public function isSubmitted(sfWebRequest $request, $method = 'post') {
        return $request->isMethod($method) && $request->hasParameter($this->getName());
    }
    
    public function bindParameters(sfWebRequest $request) {
        $name = $this->getName();
        $this->bind($request->getParameter($name), $request->getFiles($name));
    }
}