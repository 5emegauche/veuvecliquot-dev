<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version27 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('bloc_fiche_produit_ecommerce', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'bloc_fiche_produit_id' => 
             array(
              'type' => 'integer',
              'notnull' => '1',
              'length' => '8',
             ),
             'pays_id' => 
             array(
              'type' => 'integer',
              'length' => '8',
             ),
             'langue_id' => 
             array(
              'type' => 'integer',
              'length' => '8',
             ),
             'autorise' => 
             array(
              'type' => 'boolean',
              'notnull' => '1',
              'default' => '0',
              'length' => '25',
             ),
             'lien' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->addColumn('pays', 'ecommerce', 'boolean', '25', array(
             'notnull' => '1',
             'default' => '0',
             ));
    }

    public function down()
    {
        $this->dropTable('bloc_fiche_produit_ecommerce');
        $this->removeColumn('pays', 'ecommerce');
    }
}