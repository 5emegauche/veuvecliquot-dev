<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version33 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->removeColumn('we_are_clicquot', 'text');
        $this->addColumn('we_are_clicquot_translation', 'texte', 'clob', '', array(
             ));
    }

    public function down()
    {
        $this->addColumn('we_are_clicquot', 'text', 'clob', '', array(
             ));
        $this->removeColumn('we_are_clicquot_translation', 'texte');
    }
}